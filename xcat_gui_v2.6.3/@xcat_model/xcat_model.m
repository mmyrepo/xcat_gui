classdef xcat_model < handle
    %XCAT_MODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetObservable = true)
        guiFunction = {'Construct', 'Process', 'Simulate'};
        guiSelectedFunction = [];
        
        loopIndex = 0;
        loopStart = 0;
        loopEnd   = 0;
    end
    
    properties (SetObservable = false, Access = private)
        keywordList   = {'mode';'act_phan_each';'atten_phan_each';'act_phan_ave';'atten_phan_ave';'motion_option';'out_period';'time_per_frame';'out_frames';'hrt_period';'hrt_start_ph_index';'heart_base';'heart_curve_file';'apical_thin';'uniform_heart';'hrt_v1';'hrt_v2';'hrt_v3';'hrt_v4';'hrt_v5';'hrt_t1';'hrt_t2';'hrt_t3';'hrt_t4';'resp_period';'resp_start_ph_index';'max_diaphragm_motion';'max_AP_exp';'dia_filename';'ap_filename';'hrt_motion_x';'hrt_motion_y';'hrt_motion_z';'hrt_motion_rot_xz';'hrt_motion_rot_yx';'hrt_motion_rot_zy';'vessel_flag';'coronary_art_flag';'coronary_vein_flag';'papillary_flag';'arms_flag';'gender';'organ_file';'phantom_long_axis_scale';'phantom_short_axis_scale';'phantom_height_scale';'head_cir_scale';'head_height_scale';'head_skin_cir_scale';'torso_long_axis_scale';'torso_short_axis_scale';'chest_skin_long_axis_scale';'chest_skin_short_axis_scale';'abdomen_skin_long_axis_scale';'abdomen_skin_short_axis_scale';'pelvis_skin_long_axis_scale';'pelvis_skin_short_axis_scale';'arms_cir_scale';'arms_length_scale';'arms_skin_cir_scale';'legs_cir_scale';'legs_length_scale';'legs_skin_cir_scale';'bones_scale';'head_torso_muscle_scale';'arms_muscle_cir_scale';'legs_muscle_cir_scale';'hrt_scale';'breast_type';'which_breast';'rbreast_long_axis_scale';'rbreast_short_axis_scale';'rbreast_height_scale';'vol_rbreast';'rbr_theta';'rbr_phi';'r_br_tx';['r_br_ty' char(9) ''];'r_br_tz';'lbreast_long_axis_scale';'lbreast_short_axis_scale';'lbreast_height_scale';'vol_lbreast';'lbr_theta';'lbr_phi';['l_br_tx' char(9) ''];'l_br_ty';['l_br_tz' char(9) ''];'rdiaph_liv_scale';'ldiaph_scale';'frac_H2O';'marrow_flag';'thickness_sternum';'thickness_scapula';'thickness_ribs';'thickness_backbone';'thickness_pelvis';'thickness_collar';'thickness_humerus';'thickness_radius';'thickness_ulna';'thickness_hand';'thickness_femur';'thickness_tibia';'thickness_fibula';'thickness_patella';'thickness_foot';'thickness_si';'thickness_li';'si_air_flag';'li_air_flag';'thickness_esoph';'vol_prostate';'vol_testes';'vol_liver';'vol_stomach';'vol_pancreas';'vol_spleen';'vol_gall_bladder';'vol_rkidney';'vol_lkidney';'vol_radrenal';'vol_ladrenal';'vol_small_intest';'vol_large_intest';'vol_bladder';'vol_thyroid';'vol_thymus';'vol_salivary';'vol_pituitary';'vol_eyes';'vol_rovary';'vol_lovary';'vol_ftubes';'vol_uterus';'vol_vagina';'vol_larynx';'vol_trachea';'vol_esoph';'vol_epidy';'pixel_width';'slice_width';'array_size';'subvoxel_index';'startslice';'endslice';'d_ZY_rotation';'d_XZ_rotation';'d_YX_rotation';'X_tr';'Y_tr';'Z_tr';'activity_unit';'myoLV_act';'myoRV_act';'myoLA_act';'myoRA_act';'bldplLV_act';'bldplRV_act';'bldplLA_act';'bldplRA_act';'body_activity';'muscle_activity';'brain_activity';'sinus_activity';'liver_activity';'gall_bladder_activity';'r_lung_activity';'l_lung_activity';'esophagus_activity';'laryngopharynx_activity';'st_wall_activity';'st_cnts_activity';'pancreas_activity';'r_kidney_cortex_activity';'r_kidney_medulla_activity';'l_kidney_cortex_activity';'l_kidney_medulla_activity';'adrenal_activity';'r_renal_pelvis_activity';'l_renal_pelvis_activity';'spleen_activity';'rib_activity';'cortical_bone_activity';'spine_activity';'spinal_cord_activity';'bone_marrow_activity';'art_activity';'vein_activity';'bladder_activity';'prostate_activity';'asc_li_activity';'trans_li_activity';'desc_li_activity';'sm_intest_activity';'rectum_activity';'sem_activity';'vas_def_activity';'test_activity';'epididymus_activity';'ejac_duct_activity';'pericardium_activity';'cartilage_activity';'intest_air_activity';'ureter_activity';'urethra_activity';'lymph_activity';'lymph_abnormal_activity';'airway_activity';'uterus_activity';'vagina_activity';'right_ovary_activity';'left_ovary_activity';'fallopian_tubes_activity';'parathyroid_activity';'thyroid_activity';'thymus_activity';'salivary_activity';'pituitary_activity';'eye_activity';'lens_activity';'lesn_activity';'energy';'motion_defect_flag';'ThetaCenter';'ThetaWidth';'XCenterIndex';'XWidthIndex';'Wall_fract';'motion_scale';'border_zone_long';'border_zone_radial';'x_location';'y_location';'z_location';'lesn_diameter';'tumor_motion_flag';'tumor_motion_filename';'p_center_v';'p_center_u';'p_height';'p_width';'p_length';'p_id';'vec_factor';'nurbs_save';'color_code';'ct_output';'body_flag';'organ_flag';'muscle_flag';'skeleton_flag';'bronch_flag';'bvess_flag';'heart_flag';'lung_scale';'lv_radius_scale';'lv_length_scale';'use_res';'vol_rthyroid';'vol_lthyroid'};
        muH2O         = [51.1854200000000,0.231057400000000;54.7172100000000,0.222123900000000;58.4927000000000,0.214192800000000;62.5287000000000,0.207071900000000;66.8431800000000,0.200597000000000;71.4553600000000,0.194651600000000;76.3857800000000,0.189132300000000;81.6564000000000,0.183962700000000;87.2906900000000,0.179088400000000;93.3137400000000,0.174432700000000;99.7523900000000,0.169990100000000;106.635300000000,0.165706300000000;113.993100000000,0.161574600000000;121.858700000000,0.157553900000000;130.266900000000,0.153661000000000;139.255300000000,0.149862400000000;148.864000000000,0.146160500000000;159.135600000000,0.142555200000000;170.115900000000,0.139019900000000;181.853900000000,0.135575700000000;194.401800000000,0.132212700000000];
        muAir         = [50,0.208000000000000;60,0.187500000000000;80,0.166200000000000;100,0.154100000000000;150,0.135600000000000;200,0.123300000000000];
        convertHUFlag = 0;
    end
    
    properties
        parameters = {...
            'mode'                          , '0'                 , sprintf('program mode:\n\t0 = phantom\n1 = heart lesion\n2 = spherical lesion\n3 = plaque\n4 = vectors\n5 = save anatomical variation\n\nThe phantom program can be run in different modes as follows.\n\nMode 0: standard phantom generation mode that will generate phantoms of the body.\n  Mode 1: heart lesion generator that will create phantoms of only the user\n          defined heart lesion. Subtract these phantoms from those of mode 0\n          to place the defect in the body.\n  Mode 2: spherical lesion generator that will create phantoms of only the\n          user defined lesion. Add these phantoms to those of mode 0 to place\n          the lesions in the body.\n  Mode 3: cardiac plaque generator that will create phantoms of only the\n          user defined plaque. Add these phantoms to those of mode 0 to place\n          the plaques in the body.\n  Mode 4: vector generator that will output motion vectors as determined from \n          the phantom surfaces. The vectors will be output as text files.\n  Mode 5: anatomy generator will save the phantom produced from the user-defined anatomy \n          parameters. The phantom is saved as two files, the organ file and the heart_base \n          file. The names of these files can then be specified in the parfile for later runs\n          with the program not having to take the time to generate the anatomy again. In using \n	   a saved anatomy, be sure to set all scalings back to 1; otherwise, the anatomy will be \n          scaled again.       ');...
            'act_phan_each'                 , '1'                 , sprintf('activity_phantom_each_frame:\n1=save phantom to file\n0=don''t save');...
            'atten_phan_each'               , '0'                 , sprintf('attenuation_coeff_phantom_each_frame\n1=save phantom to file\n0=don''t save');...
            'act_phan_ave'                  , '0'                 , sprintf('activity_phantom_average:\n1=save\n0=don''t save\n\nsee The average phantom is the average ONLY OF THOSE FRAMES GENERATED. That is, \nif you specify that only 2 frames be generated, then the average phantom is \njust the average of those 2 frames.\n#  ***************************************************************************\n#  ** FOR A GOOD AVERAGE, generate at least 8-16 frames per 1 complete heart\n#  ** cycle and/or per 1 complete respiratory cycle.\n#  ***************************************************************************\n');...
            'atten_phan_ave'                , '0'                 , sprintf('attenuation_coeff_phantom_average:\n1=save\n0=don''t save\n\nsee The average phantom is the average ONLY OF THOSE FRAMES GENERATED. That is, \nif you specify that only 2 frames be generated, then the average phantom is \njust the average of those 2 frames.\n#  ***************************************************************************\n#  ** FOR A GOOD AVERAGE, generate at least 8-16 frames per 1 complete heart\n#  ** cycle and/or per 1 complete respiratory cycle.\n#  ***************************************************************************\n');...
            'motion_option'                 , '2'                 , sprintf('motion_option:\n0=beating heart only\n1=respiratory motion only\n2=both motions\nHeart motion refers to heart BEATING or contraction, while resp.\nmotion refers to organ motion due to breathing. Note that the entire heart is\ntranslated or rotated due to resp. motion, even if it is not contracting.\n** IF motion_option=1 , THE HEART WILL MOVE (TRANSLATE) BUT NOT BEAT.****');...
            'out_period'                    , '5'                 , sprintf('output_period (SECS)\nif <= 0, then output_period=time_per_frame*output_frames');...
            'time_per_frame'                , '0'                 , 'time_per_frame (SECS) (**IGNORED unless output_period<=0**)';...
            'out_frames'                    , '1'                 , 'number of output time frames )';...
            'hrt_period'                    , '1'                 , sprintf('length of beating heart cycle in seconds\nnormal = 1 s\n Users sets the length and starting phase of both the heart\nand respiratory cycles. NORMAL values for length of heart beat and\nrespiratory are cycles are 1 sec. and 5 secs., respectively,\nBUT THESE CAN VARY AMONG PATIENTS and will increase if the patient\nis under stress.');...
            'hrt_start_ph_index'            , '0.0'               , sprintf('range=0 to 1 (ED=0, ES=0.4) Users sets the length and starting phase of both the heart\nand respiratory cycles. NORMAL values for length of heart beat and\nrespiratory are cycles are 1 sec. and 5 secs., respectively,\nBUT THESE CAN VARY AMONG PATIENTS and will increase if the patient\nis under stress.');...
            'heart_base'                    , 'vmale50_heart.nrb' , sprintf('basename for heart files.\nDefaults:\nmale = vmale50_heart.nrb\nfemale = vfemale50_heart.nrb');...
            'heart_curve_file'              , 'heart_curve.txt'   , sprintf('name for file containing time curve for heart\nDefault:\nheart_curve.txt');...
            'apical_thin'                   , '0.0'               , sprintf('apical_thinning (0 to 1.0 scale):\n0.0 = not present\n0.5 = halfway present\n1.0 = completely thin');...
            'uniform_heart'                 , '0'                 , sprintf('sets the thickness of the Left Ventricle:\n0 = default, nonuniform wall thickness\n1 = uniform wall thickness for Left Ventricle');...
            'hrt_v1'                        , '0.0'               , sprintf('sets the Left Ventricle end-diastolic volume (0 = do not change)\n These parameters control the Left Ventricle volume curve of the heart. The user can specify the Left Ventricle\nvolume at 5 points in the cardiac cycle. Check the logfile to see what the default volumes\nare.  The end-diastolic volume can only be reduced. The way to increase it would be to change\nthe overall heart scale.  The end-systolic volume can be increased or reduced. The other volumes\nneed to have values between the end-diastolic and end-systolic volumes.  The time durations for the\ndifferent portions of the cardiac cycle must add up to a total of 1.\nChanging these parameters will alter the heart_curve.  The altered curve and heart files can be output using\nmode = 5.');...
            'hrt_v2'                        , '0.0'               , sprintf('sets the LV end-systolic volume (0 = do not change)\nThese parameters control the Left Ventricle volume curve of the heart. The user can specify the Left Ventricle\nvolume at 5 points in the cardiac cycle. Check the logfile to see what the default volumes\nare.  The end-diastolic volume can only be reduced. The way to increase it would be to change\nthe overall heart scale.  The end-systolic volume can be increased or reduced. The other volumes\nneed to have values between the end-diastolic and end-systolic volumes.  The time durations for the\ndifferent portions of the cardiac cycle must add up to a total of 1.\nChanging these parameters will alter the heart_curve.  The altered curve and heart files can be output using\nmode = 5.');...
            'hrt_v3'                        , '0.0'               , sprintf('sets the LV volume at the beginning of the quiet phase (0 = do not change)\nThese parameters control the Left Ventricle volume curve of the heart. The user can specify the Left Ventricle\nvolume at 5 points in the cardiac cycle. Check the logfile to see what the default volumes\nare.  The end-diastolic volume can only be reduced. The way to increase it would be to change\nthe overall heart scale.  The end-systolic volume can be increased or reduced. The other volumes\nneed to have values between the end-diastolic and end-systolic volumes.  The time durations for the\ndifferent portions of the cardiac cycle must add up to a total of 1.\nChanging these parameters will alter the heart_curve.  The altered curve and heart files can be output using\nmode = 5.');...
            'hrt_v4'                        , '0.0'               , sprintf('sets the LV volume at the end of the quiet phase (0 = do not change)\n;These parameters control the Left Ventricle volume curve of the heart. The user can specify the Left Ventricle\nvolume at 5 points in the cardiac cycle. Check the logfile to see what the default volumes\nare.  The end-diastolic volume can only be reduced. The way to increase it would be to change\nthe overall heart scale.  The end-systolic volume can be increased or reduced. The other volumes\nneed to have values between the end-diastolic and end-systolic volumes.  The time durations for the\ndifferent portions of the cardiac cycle must add up to a total of 1.\nChanging these parameters will alter the heart_curve.  The altered curve and heart files can be output using\nmode = 5.');...
            'hrt_v5'                        , '0.0'               , sprintf('sets the LV volume during reduced filling, before end-diastole (0 = do not change)\nThese parameters control the Left Ventricle volume curve of the heart. The user can specify the Left Ventricle\nvolume at 5 points in the cardiac cycle. Check the logfile to see what the default volumes\nare.  The end-diastolic volume can only be reduced. The way to increase it would be to change\nthe overall heart scale.  The end-systolic volume can be increased or reduced. The other volumes\nneed to have values between the end-diastolic and end-systolic volumes.  The time durations for the\ndifferent portions of the cardiac cycle must add up to a total of 1.\nChanging these parameters will alter the heart_curve.  The altered curve and heart files can be output using\nmode = 5.');...
            'hrt_t1'                        , '0.5'               , sprintf('sets the duration from end-diastole to end-systole, hrt_v1 to hrt_v2 (default = 0.5s)\nThese parameters control the Left Ventricle volume curve of the heart. The user can specify the Left Ventricle\nvolume at 5 points in the cardiac cycle. Check the logfile to see what the default volumes\nare.  The end-diastolic volume can only be reduced. The way to increase it would be to change\nthe overall heart scale.  The end-systolic volume can be increased or reduced. The other volumes\nneed to have values between the end-diastolic and end-systolic volumes.  The time durations for the\ndifferent portions of the cardiac cycle must add up to a total of 1.\nChanging these parameters will alter the heart_curve.  The altered curve and heart files can be output using\nmode = 5.');...
            'hrt_t2'                        , '0.192'             , sprintf('sets the duration from end-systole to beginning of quiet phase, hrt_v2 to hrt_v3 (default = 0.192s)\nThese parameters control the Left Ventricle volume curve of the heart. The user can specify the Left Ventricle\nvolume at 5 points in the cardiac cycle. Check the logfile to see what the default volumes\nare.  The end-diastolic volume can only be reduced. The way to increase it would be to change\nthe overall heart scale.  The end-systolic volume can be increased or reduced. The other volumes\nneed to have values between the end-diastolic and end-systolic volumes.  The time durations for the\ndifferent portions of the cardiac cycle must add up to a total of 1.\nChanging these parameters will alter the heart_curve.  The altered curve and heart files can be output using\nmode = 5.');...
            'hrt_t3'                        , '0.115'             , sprintf('sets the duration of quiet phase, hrt_v3 to hrt_v4 (default = 0.115s)\nThese parameters control the Left Ventricle volume curve of the heart. The user can specify the Left Ventricle\nvolume at 5 points in the cardiac cycle. Check the logfile to see what the default volumes\nare.  The end-diastolic volume can only be reduced. The way to increase it would be to change\nthe overall heart scale.  The end-systolic volume can be increased or reduced. The other volumes\nneed to have values between the end-diastolic and end-systolic volumes.  The time durations for the\ndifferent portions of the cardiac cycle must add up to a total of 1.\nChanging these parameters will alter the heart_curve.  The altered curve and heart files can be output using\nmode = 5.');...
            'hrt_t4'                        , '0.193'             , sprintf('sets the duration from end of quiet phase to reduced filling, hrt_v4 to hrt_v5 (default = 0.193s)\nThese parameters control the Left Ventricle volume curve of the heart. The user can specify the Left Ventricle\nvolume at 5 points in the cardiac cycle. Check the logfile to see what the default volumes\nare.  The end-diastolic volume can only be reduced. The way to increase it would be to change\nthe overall heart scale.  The end-systolic volume can be increased or reduced. The other volumes\nneed to have values between the end-diastolic and end-systolic volumes.  The time durations for the\ndifferent portions of the cardiac cycle must add up to a total of 1.\nChanging these parameters will alter the heart_curve.  The altered curve and heart files can be output using\nmode = 5.');...
            'resp_period'                   , '5'                 , sprintf('length of respiratory cycle in seconds\nnormal breathing = 5 s\nUsers sets the length and starting phase of both the heart\nand respiratory cycles. NORMAL values for length of heart beat and\nrespiratory are cycles are 1 sec. and 5 secs., respectively,\nBUT THESE CAN VARY AMONG PATIENTS and will increase if the patient\nis under stress.\nAn index value between 0 and 1 is used the specify the starting phase\nof the heart or resp cycles. IF NO MOTION IS SPECIFIED THEN THE STARTING\nPHASE IS USED AS THE SINGLE PHASE AT WHICH THE PHANTOM IS GENERATED.\n(see documentation for more details).');...
            'resp_start_ph_index'           , '0.0'               , sprintf('range=0 to 1\nfull exhale= 0.0\nfull inhale=0.4\nUsers sets the length and starting phase of both the heart\nand respiratory cycles. NORMAL values for length of heart beat and\nrespiratory are cycles are 1 sec. and 5 secs., respectively,\nBUT THESE CAN VARY AMONG PATIENTS and will increase if the patient\nis under stress.\nAn index value between 0 and 1 is used the specify the starting phase\nof the heart or resp cycles. IF NO MOTION IS SPECIFIED THEN THE STARTING\nPHASE IS USED AS THE SINGLE PHASE AT WHICH THE PHANTOM IS GENERATED.\n(see documentation for more details).');...
            'max_diaphragm_motion'          , '2'                 , sprintf('extent in cm''s of diaphragm motion\nnormal breathing = 2 cm\nThese NORMAL values are for normal tidal breathing.\n** Modeling a deep inhale may require higher values. **\nThe AP_expansion parameter controls the anteroposterior diameter of the ribcage, body,\nand lungs. The ribs rotate upward to expand the chest cavity by the amount indicated by the \nAP_expansion parameter. The lungs and body move with the expanding ribs. There is maximum amount\nby which the AP diameter can expand, due to the size of the ribs (some expansions are impossible\ngeometrically.) If the user specifies too great an expansion, the program will terminate with an\nerror message. \nThe diaphragm motion controls the motion of the liver, the left diaphragm, stomach, spleen and\nall organs downstream from them. \nThe heart has its own parameters to control its motion. It can translate left or right (+/- values of hrt_motion_x respectively), \nto the anterior/posterior (+/- values of hrt_motion_y respectively), or up/down (+/- values of hrt_motion_z respectively) \nwith the diaphragm motion. The heart can also rotate. The x-axis runs from the right side of the body to the left.  \nChanging the x-rot will tilt the heart up(+ values)/down (- values).  The y-axis runs from the front of the body to the back.  \nChanging the y-rot will tilt the heart from side to side.  The z-axis runs from the feet to the head.  \nThe z-rot will spin the heart right or left.');...
            'max_AP_exp'                    , '1.2'               , sprintf('extent in cm''s of the AP expansion of the chest\nnormal breathing = 1.2 cm\nThese NORMAL values are for normal tidal breathing.\n** Modeling a deep inhale may require higher values. **\nThe AP_expansion parameter controls the anteroposterior diameter of the ribcage, body,\nand lungs. The ribs rotate upward to expand the chest cavity by the amount indicated by the \nAP_expansion parameter. The lungs and body move with the expanding ribs. There is maximum amount\nby which the AP diameter can expand, due to the size of the ribs (some expansions are impossible\ngeometrically.) If the user specifies too great an expansion, the program will terminate with an\nerror message. \nThe diaphragm motion controls the motion of the liver, the left diaphragm, stomach, spleen and\nall organs downstream from them. \nThe heart has its own parameters to control its motion. It can translate left or right (+/- values of hrt_motion_x respectively), \nto the anterior/posterior (+/- values of hrt_motion_y respectively), or up/down (+/- values of hrt_motion_z respectively) \nwith the diaphragm motion. The heart can also rotate. The x-axis runs from the right side of the body to the left.  \nChanging the x-rot will tilt the heart up(+ values)/down (- values).  The y-axis runs from the front of the body to the back.  \nChanging the y-rot will tilt the heart from side to side.  The z-axis runs from the feet to the head.  \nThe z-rot will spin the heart right or left.');...
            'dia_filename'                  , 'diaphragm_curve.dat', sprintf('name of curve defining diaphragm motion during respiration');...
            'ap_filename'                   , 'ap_curve.dat'      , sprintf('name of curve defining chest anterior-posterior motion during respiration');...
            'hrt_motion_x'                  , '0.0'               , sprintf('extent in cm''s of the heart''s lateral motion during breathing\ndefault = 0.0 cm)');...
            'hrt_motion_y'                  , '0.5'               , sprintf('extent in cm''s of the heart''s AP motion during breathing\ndefault = 1.2 cm)');...
            'hrt_motion_z'                  , '2.0'               , sprintf('extent in cm''s of the heart''s up/down motion during breathing\ndefault = 2.0 cm)');...
            'hrt_motion_rot_xz'             , '0.0'               , sprintf('#hrt_motion_rot_xz (extent in degrees of the heart''s xz rotation during breathing; default = 0.0 ) \nThese NORMAL values are for normal tidal breathing.\n** Modeling a deep inhale may require higher values. **\nThe AP_expansion parameter controls the anteroposterior diameter of the ribcage, body,\nand lungs. The ribs rotate upward to expand the chest cavity by the amount indicated by the \nAP_expansion parameter. The lungs and body move with the expanding ribs. There is maximum amount\nby which the AP diameter can expand, due to the size of the ribs (some expansions are impossible\ngeometrically.) If the user specifies too great an expansion, the program will terminate with an\nerror message. \nThe diaphragm motion controls the motion of the liver, the left diaphragm, stomach, spleen and\nall organs downstream from them. \nThe heart has its own parameters to control its motion. It can translate left or right (+/- values of hrt_motion_x respectively), \nto the anterior/posterior (+/- values of hrt_motion_y respectively), or up/down (+/- values of hrt_motion_z respectively) \nwith the diaphragm motion. The heart can also rotate. The x-axis runs from the right side of the body to the left.  \nChanging the x-rot will tilt the heart up(+ values)/down (- values).  The y-axis runs from the front of the body to the back.  \nChanging the y-rot will tilt the heart from side to side.  The z-axis runs from the feet to the head.  \nThe z-rot will spin the heart right or left. \nrotation parameters determine\ninitial orientation of beating (dynamic) heart LV long axis\nd_zy_rotation : +y-axis rotates toward +z-axis (about x-axis) by beta\nd_xz_rotation : +z-axis rotates toward +x-axis (about y-axis) by phi\nd_yx_rotation : +x-axis rotates toward +y-axis (about z-axis) by psi\nBased on patient data, the mean and SD heart orientations are:\nzy_rot = -110 degrees (no patient data for this rotation)\nxz_rot = 23 +- 10 deg.\nyx_rot = -52 +- 11 deg.\nPhantom will output total angles for the heart orientation in the logfile');...
            'hrt_motion_rot_yx'             , '0.0'               , sprintf('#hrt_motion_rot_yx (extent in degrees of the heart''s yx rotation during breathing; default = 0.0 ) \nThese NORMAL values are for normal tidal breathing.\n** Modeling a deep inhale may require higher values. **\nThe AP_expansion parameter controls the anteroposterior diameter of the ribcage, body,\nand lungs. The ribs rotate upward to expand the chest cavity by the amount indicated by the \nAP_expansion parameter. The lungs and body move with the expanding ribs. There is maximum amount\nby which the AP diameter can expand, due to the size of the ribs (some expansions are impossible\ngeometrically.) If the user specifies too great an expansion, the program will terminate with an\nerror message. \nThe diaphragm motion controls the motion of the liver, the left diaphragm, stomach, spleen and\nall organs downstream from them. \nThe heart has its own parameters to control its motion. It can translate left or right (+/- values of hrt_motion_x respectively), \nto the anterior/posterior (+/- values of hrt_motion_y respectively), or up/down (+/- values of hrt_motion_z respectively) \nwith the diaphragm motion. The heart can also rotate. The x-axis runs from the right side of the body to the left.  \nChanging the x-rot will tilt the heart up(+ values)/down (- values).  The y-axis runs from the front of the body to the back.  \nChanging the y-rot will tilt the heart from side to side.  The z-axis runs from the feet to the head.  \nThe z-rot will spin the heart right or left. \nrotation parameters determine\ninitial orientation of beating (dynamic) heart LV long axis\nd_zy_rotation : +y-axis rotates toward +z-axis (about x-axis) by beta\nd_xz_rotation : +z-axis rotates toward +x-axis (about y-axis) by phi\nd_yx_rotation : +x-axis rotates toward +y-axis (about z-axis) by psi\nBased on patient data, the mean and SD heart orientations are:\nzy_rot = -110 degrees (no patient data for this rotation)\nxz_rot = 23 +- 10 deg.\nyx_rot = -52 +- 11 deg.\nPhantom will output total angles for the heart orientation in the logfile');...
            'hrt_motion_rot_zy'             , '0.0'               , sprintf('#hrt_motion_rot_zy (extent in degrees of the heart''s zy rotation during breathing; default = 0.0 ) \nThese NORMAL values are for normal tidal breathing.\n** Modeling a deep inhale may require higher values. **\nThe AP_expansion parameter controls the anteroposterior diameter of the ribcage, body,\nand lungs. The ribs rotate upward to expand the chest cavity by the amount indicated by the \nAP_expansion parameter. The lungs and body move with the expanding ribs. There is maximum amount\nby which the AP diameter can expand, due to the size of the ribs (some expansions are impossible\ngeometrically.) If the user specifies too great an expansion, the program will terminate with an\nerror message. \nThe diaphragm motion controls the motion of the liver, the left diaphragm, stomach, spleen and\nall organs downstream from them. \nThe heart has its own parameters to control its motion. It can translate left or right (+/- values of hrt_motion_x respectively), \nto the anterior/posterior (+/- values of hrt_motion_y respectively), or up/down (+/- values of hrt_motion_z respectively) \nwith the diaphragm motion. The heart can also rotate. The x-axis runs from the right side of the body to the left.  \nChanging the x-rot will tilt the heart up(+ values)/down (- values).  The y-axis runs from the front of the body to the back.  \nChanging the y-rot will tilt the heart from side to side.  The z-axis runs from the feet to the head.  \nThe z-rot will spin the heart right or left. \nrotation parameters determine\ninitial orientation of beating (dynamic) heart LV long axis\nd_zy_rotation : +y-axis rotates toward +z-axis (about x-axis) by beta\nd_xz_rotation : +z-axis rotates toward +x-axis (about y-axis) by phi\nd_yx_rotation : +x-axis rotates toward +y-axis (about z-axis) by psi\nBased on patient data, the mean and SD heart orientations are:\nzy_rot = -110 degrees (no patient data for this rotation)\nxz_rot = 23 +- 10 deg.\nyx_rot = -52 +- 11 deg.\nPhantom will output total angles for the heart orientation in the logfile');...
            'vessel_flag'                   , '1'                 , sprintf('1 = include arteries and veins\n0 = do not include) ');...
            'coronary_art_flag'             , '1'                 , sprintf('coronary artery flag\n1 = include coronary arteries\n0 = do not include)');...
            'coronary_vein_flag'            , '1'                 , 'coronary vein flag (1 = include coroanry veins, 0 = do not include)';...
            'papillary_flag'                , '1'                 , 'papillary_flag (1 = include papillary muscles in heart, 0 = do not include)';...
            'arms_flag'                     , '1'                 , 'arms_flag (0 = no arms, 1 = arms at the side)';...
            'gender'                        , '0'                 , 'male or female phantom (0 = male, 1 = female), be sure to adjust below accordingly';...
            'organ_file'                    , 'vmale50.nrb'       , 'name of organ file that defines all organs (male = vmale50.nrb, female - vfemale50.nrb)';...
            'phantom_long_axis_scale'       , '1.0'               , sprintf('phantom_long_axis_scale (scales phantom laterally - scales everything but the heart)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'phantom_short_axis_scale'      , '1.0'               , sprintf('phantom_short_axis_scale (scales phantom AP - scales everything but the heart)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'phantom_height_scale'          , '1.0'               , sprintf('phantom_height_scale (scales phantom height - scales everything but the heart)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'head_cir_scale'                , '1.0'               , sprintf('head_circumference_scale (scales head radially - scales everything in head)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'head_height_scale'             , '1.0'               , sprintf('head_height_scale (scales head height - scales everything in head)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'head_skin_cir_scale'           , '1.0'               , sprintf('head_skin_circumference_scale (scales head radially - scales only outer skin)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'torso_long_axis_scale'         , '1.0'               , sprintf('torso_long_axis_scale (sets torso, chest and abdomen, transverse axis - scales everything but the heart)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'torso_short_axis_scale'        , '1.0'               , sprintf('torso_short_axis_scale (sets torso, chest and abdomen, AP axis - scales everything but the heart)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'chest_skin_long_axis_scale'    , '1.0'               , sprintf('chest_skin_long_axis_scale (sets chest transverse axis - scales only body outline) \nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'chest_skin_short_axis_scale'   , '1.0'               , sprintf('chest_skin_short_axis_scale (sets chest AP axis - scales only body outline)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'abdomen_skin_long_axis_scale'  , '1.0'               , sprintf('abdomen_skin_long_axis_scale (sets abdomen transverse axis - scales only body outline) \nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'abdomen_skin_short_axis_scale' , '1.0'               , sprintf('abdomen_skin_short_axis_scale (sets abdomen AP axis - scales only body outline)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'pelvis_skin_long_axis_scale'   , '1.0'               , sprintf('pelvis_skin_long_axis_scale (sets pelvis transverse axis - scales only body outline)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'pelvis_skin_short_axis_scale'  , '1.0'               , sprintf('pelvis_skin_short_axis_scale (sets pelvis AP axis - scales only body outline)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'arms_cir_scale'                , '1.0'               , sprintf('arms_circumference_scale (scales arms radially - scales everything in arms)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'arms_length_scale'             , '1.0'               , sprintf('arms_length_scale (scales arms length - scales everything in arms)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'arms_skin_cir_scale'           , '1.0'               , sprintf('arms_skin_circumference_scale (scales arms radially - scales only outer skin)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'legs_cir_scale'                , '1.0'               , sprintf('legs_circumference_scale (scales legs radially - scales everything in legs)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'legs_length_scale'             , '1.0'               , sprintf('legs_length_scale (scales legs length - scales everything in legs)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'legs_skin_cir_scale'           , '1.0'               , sprintf('legs_skin_circumference_scale (scales legs radially - scales only outer skin)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'bones_scale'                   , '1.0'               , sprintf('bones_scale (scales all bones in 2D about their centerlines, makes each bone thicker) \nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'head_torso_muscle_scale'       , '1.0'               , sprintf('head_torso_muscle_scale (compresses/expands the muscles radially) \nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'arms_muscle_cir_scale'         , '1.0'               , sprintf('arms_muscle_cir_scale (compresses/expands the muscles radially) \nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'legs_muscle_cir_scale'         , '1.0'               , sprintf('legs_muscle_cir_scale (compresses/expands the muscles radially) \nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'hrt_scale'                     , '1.0'               , 'hrt_scale  (scales heart in 3D) ';...
            'breast_type'                   , '1'                 , 'breast_type (0=supine, 1=prone) ';...
            'which_breast'                  , '0'                 , 'which_breast (0 = none, 1 = both, 2 = right only, 3=left only )';...
            'rbreast_long_axis_scale'       , '1.0'               , sprintf('right breast_long_axis (sets the breasts lateral dimension)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'rbreast_short_axis_scale'      , '1.0'               , sprintf('right breast_short_axis (sets the breasts antero-posterior dimension)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'rbreast_height_scale'          , '1.0'               , sprintf('right breast_height (sets the breasts height) \nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'vol_rbreast'                   , '0.0'               , 'sets rbreast volume by scaling in 3D, will over-rule above scalings';...
            'rbr_theta'                     , '10.0'              , sprintf('theta angle of the right breast (angle the breast is tilted transversely (sideways) from the center of the chest\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'rbr_phi'                       , '0.0'               , sprintf('phi angle of the right breast (angle the breast is tilted up (+)  or down (-)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'r_br_tx'                       , '0.0'               , 'x translation for right breast ';...
            'r_br_ty'                       , '0.0'               , 'y translation for right breast ';...
            'r_br_tz'                       , '0.0'               , 'z translation for right breast ';...
            'lbreast_long_axis_scale'       , '1.0'               , sprintf('left breast_long_axis (sets the breasts lateral dimension)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'lbreast_short_axis_scale'      , '1.0'               , sprintf('left breast_short_axis (sets the breasts antero-posterior dimension)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'lbreast_height_scale'          , '1.0'               , sprintf('left breast_height (sets the breasts height) \nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'vol_lbreast'                   , '0.0'               , 'sets lbreast volume by scaling in 3D, will over-rule above scalings';...
            'lbr_theta'                     , '10.0'              , sprintf('theta angle of the left breast (angle the breast is tilted transversely (sideways) from the center of the chest\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'lbr_phi'                       , '0.0'               , sprintf('phi angle of the left breast (angle the breast is tilted up (+)  or down (-)\nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'l_br_tx'                       , '0.0'               , 'x translation for left breast ';...
            'l_br_ty'                       , '0.0'               , 'y translation for left breast ';...
            'l_br_tz'                       , '0.0'               , 'z translation for left breast ';...
            'rdiaph_liv_scale'              , '1.0'               , sprintf('height of right_diaphragm/liver dome (0 = flat, 1 = original height, > 1 raises the diaphragm) \nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'ldiaph_scale'                  , '1.0'               , sprintf('height of left diaphragm dome (0 = flat, 1 = original height, > raises the diaphragm) \nThe phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is\nnamed with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \nthe phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\nbe additional to this base scaling.');...
            'frac_H2O'                      , '0.5'               , 'fraction (by weight) of water in wet bone and wet spine (used to calc. atten coeff)';...
            'marrow_flag'                   , '1'                 , 'render marrow (0 = no, 1 = yes)';...
            'thickness_sternum'             , '0.4'               , 'thickness sternum  (cm)';...
            'thickness_scapula'             , '0.35'              , 'thickness scapulas (cm)';...
            'thickness_ribs'                , '0.3'               , 'thickness ribs     (cm)';...
            'thickness_backbone'            , '0.4'               , 'thickness backbone (cm)';...
            'thickness_pelvis'              , '0.4'               , 'thickness pelvis (cm)';...
            'thickness_collar'              , '0.35'              , 'thickness collarbones (cm)';...
            'thickness_humerus'             , '0.45'              , 'thickness humerus (cm)';...
            'thickness_radius'              , '0.45'              , 'thickness radius (cm)';...
            'thickness_ulna'                , '0.45'              , 'thickness ulna (cm)';...
            'thickness_hand'                , '0.35'              , 'thickness hand bones (cm)';...
            'thickness_femur'               , '0.5'               , 'thickness femur (cm)';...
            'thickness_tibia'               , '0.6'               , 'thickness tibia (cm)';...
            'thickness_fibula'              , '0.5'               , 'thickness fibula (cm)';...
            'thickness_patella'             , '0.3'               , 'thickness patella (cm)';...
            'thickness_foot'                , '0.4'               , 'thickness foot bones (cm)';...
            'thickness_si'                  , '0.6'               , 'thickness of small intestine wall (cm)';...
            'thickness_li'                  , '0.6'               , 'thickness of large intestine wall (cm)';...
            'si_air_flag'                   , '0'                 , '0 = do not include air and 1 = include air in small intestine';...
            'li_air_flag'                   , '0'                 , sprintf('location of air in the large intestine \nLocation of air in the large intestine and rectum\n5 = air visible in the entire large intestine and rectum\n4 = air visible in ascending, transverse, descending, and sigmoid portions of the large intestine \n3 = air visible in ascending, transverse, and descending portions of the large intestine\n2 = air visible in ascending and transverse portions of the large intestine\n1 = air visible in ascending portion of the large intestine only\n0 = no air visible (entire large intestine and rectum filled with contents)');...
            'thickness_esoph'               , '0.3'               , 'thickness of the esophagus wall (cm)';...
            'vol_prostate'                  , '0.0'               , 'set the volume of the prostate; (0 = do not change)';...
            'vol_testes'                    , '0.0'               , 'set the volume of the testes; (0 = do not change)';...
            'vol_liver'                     , '0.0'               , 'set the volume of the liver; (0 = do not change)';...
            'vol_stomach'                   , '0.0'               , 'set the volume of the stomach; (0 = do not change)';...
            'vol_pancreas'                  , '0.0'               , 'set the volume of the pancreas; (0 = do not change)';...
            'vol_spleen'                    , '0.0'               , 'set the volume of the spleen; (0 = do not change)';...
            'vol_gall_bladder'              , '0.0'               , 'set the volume of the gall_bladder; (0 = do not change)';...
            'vol_rkidney'                   , '0.0'               , 'set the volume of the right kidney; (0 = do not change)';...
            'vol_lkidney'                   , '0.0'               , 'set the volume of the left kidney; (0 = do not change)';...
            'vol_radrenal'                  , '0.0'               , 'set the volume of the right adrenal; (0 = do not change)';...
            'vol_ladrenal'                  , '0.0'               , 'set the volume of the left adrenal; (0 = do not change)';...
            'vol_small_intest'              , '0.0'               , 'set the volume of the small intestine; (0 = do not change)';...
            'vol_large_intest'              , '0.0'               , 'set the volume of the large intestine; (0 = do not change)';...
            'vol_bladder'                   , '0.0'               , 'set the volume of the bladder; (0 = do not change)';...
            'vol_thyroid'                   , '0.0'               , 'set the volume of the thyroid; (0 = do not change)';...
            'vol_thymus'                    , '0.0'               , 'set the volume of the thymus; (0 = do not change)';...
            'vol_salivary'                  , '0.0'               , 'set the volume of the salivary glands; (0 = do not change)';...
            'vol_pituitary'                 , '0.0'               , 'set the volume of the pituitary gland; (0 = do not change)';...
            'vol_eyes'                      , '0.0'               , 'set the volume of the eyes; (0 = do not change)';...
            'vol_rovary'                    , '0.0'               , 'set the volume of the right ovary; (0 = do not change)';...
            'vol_lovary'                    , '0.0'               , 'set the volume of the left ovary; (0 = do not change)';...
            'vol_ftubes'                    , '0.0'               , 'set the volume of the fallopian tubes; (0 = do not change)';...
            'vol_uterus'                    , '0.0'               , 'set the volume of the uterus; (0 = do not change)';...
            'vol_vagina'                    , '0.0'               , 'set the volume of the vagina; (0 = do not change)';...
            'vol_larynx'                    , '0.0'               , 'set the volume of the larynx/pharynx; (0 = do not change)';...
            'vol_trachea'                   , '0.0'               , 'set the volume of the trachea (total); (0 = do not change)';...
            'vol_esoph'                     , '0.0'               , 'set the volume of the esophagus (total); (0 = do not change)';...
            'vol_epidy'                     , '0.0'               , 'set the volume of the epididymus; (0 = do not change)';...
            'pixel_width'                   , '0.2'               , sprintf('pixel width (cm)\nThe phantom dimensions do not necessarily have to be cubic. The array_size parameter \ndetermines the x and y dimensions of the images.  The number of slices in the z dimension \nis determined by the start_slice and end_slice parameters.  The total number of slices is\nend_slice - start_slice + 1.');...
            'slice_width'                   , '0.2'               , 'slice width (cm);   ';...
            'array_size'                    , '256'               , 'array size   ';...
            'subvoxel_index'                , '1'                 , 'subvoxel_index (=1,2,3,4 -> 1,8,27,64 subvoxels/voxel, respectively) ';...
            'startslice'                    , '10'                , 'start_slice ;  ';...
            'endslice'                      , '990'               , 'end_slice;  ';...
            'd_ZY_rotation'                 , '0'                 , sprintf('change in zy_rotation (beta) in deg. (0) \nrotation parameters determine\ninitial orientation of beating (dynamic) heart LV long axis\nd_zy_rotation : +y-axis rotates toward +z-axis (about x-axis) by beta\nd_xz_rotation : +z-axis rotates toward +x-axis (about y-axis) by phi\nd_yx_rotation : +x-axis rotates toward +y-axis (about z-axis) by psi\nBased on patient data, the mean and SD heart orientations are:\nzy_rot = -110 degrees (no patient data for this rotation)\nxz_rot = 23 +- 10 deg.\nyx_rot = -52 +- 11 deg.\nPhantom will output total angles for the heart orientation in the logfile');...
            'd_XZ_rotation'                 , '0'                 , 'change in xz_rotation ( phi) in deg. (0); ';...
            'd_YX_rotation'                 , '0'                 , 'change in yx_rotation ( psi) in deg. (0); ';...
            'X_tr'                          , '0.0'               , 'x translation in mm ; ';...
            'Y_tr'                          , '0.0'               , 'y translation in mm ;';...
            'Z_tr'                          , '0.0'               , 'z translation in mm ;';...
            'activity_unit'                 , '0'                 , sprintf('activity units (1= scale by voxel volume; 0= don''t scale)\n Creates a plaque in the coronary vessel tree that will move with the cardiac/respiratory motion\n---------------------------------------------------------------------------\nplaque_center: location of plaque along the length of the specified artery\ncenter = 0    -> base of artery\ncenter = 1.0  -> apex of artery\n-------------------------------------------\nplaque_thickness : plaque thickness in mm.\n-------------------------------------------\nplaque_width :   plaque width in mm.\n-------------------------------------------\nplaque_length :  plaque length in mm.\n------------------------------------------------------\nplaque_id  :  vessel to place the plaque in\naorta \nrca1\nrca2\nlad1\nlad2\nlad3\nlcx\n------------------------------------------------------');...
            'myoLV_act'                     , '41'                , 'hrt_myoLV_act - activity in left ventricle myocardium';...
            'myoRV_act'                     , '41'                , 'hrt_myoRV_act - activity in right ventricle myocardium';...
            'myoLA_act'                     , '41'                , 'hrt_myoLA_act - activity in left atrium myocardium';...
            'myoRA_act'                     , '41'                , 'hrt_myoRA_act - activity in right atrium myocardium';...
            'bldplLV_act'                   , '41'                , 'hrt_bldplLV_act - activity in left ventricle chamber (blood pool)';...
            'bldplRV_act'                   , '41'                , 'hrt_bldplRV_act - activity in right ventricle chamber (blood pool)';...
            'bldplLA_act'                   , '41'                , 'hrt_bldplLA_act - activity in left atria chamber (blood pool)';...
            'bldplRA_act'                   , '41'                , 'hrt_bldplRA_act - activity in right atria chamber (blood pool)';...
            'body_activity'                 , '1'                 , 'body_activity (background activity) ;';...
            'muscle_activity'               , '2'                 , 'muscle activity;';...
            'brain_activity'                , '3'                 , 'brain activity;';...
            'sinus_activity'                , '4'                 , 'sinus activity;';...
            'liver_activity'                , '5'                 , 'liver_activity;';...
            'gall_bladder_activity'         , '6'                 , 'gall_bladder_activity;';...
            'r_lung_activity'               , '7'                 , 'right_lung_activity;';...
            'l_lung_activity'               , '7'                 , 'left_lung_activity;';...
            'esophagus_activity'            , '8'                 , 'esophagus_activity;';...
            'laryngopharynx_activity'       , '9'                 , 'laryngopharynx_activity';...
            'st_wall_activity'              , '10'                , 'st_wall_activity;  (stomach wall)';...
            'st_cnts_activity'              , '10'                , 'st_cnts_activity;   (stomach contents)';...
            'pancreas_activity'             , '11'                , 'pancreas_activity;';...
            'r_kidney_cortex_activity'      , '12'                , 'right_kidney_cortex_activity;';...
            'r_kidney_medulla_activity'     , '12'                , 'right_kidney_medulla_activity;';...
            'l_kidney_cortex_activity'      , '12'                , 'left_kidney_cortex_activity;';...
            'l_kidney_medulla_activity'     , '12'                , 'left_kidney_medulla_activity;';...
            'adrenal_activity'              , '13'                , 'adrenal_activity;';...
            'r_renal_pelvis_activity'       , '14'                , 'right_renal_pelvis_activity;';...
            'l_renal_pelvis_activity'       , '14'                , 'left_renal_pelvis_activity;';...
            'spleen_activity'               , '15'                , 'spleen_activity;';...
            'rib_activity'                  , '16'                , 'rib_activity;';...
            'cortical_bone_activity'        , '16'                , 'cortical_bone_activity;';...
            'spine_activity'                , '16'                , 'spine_activity;';...
            'spinal_cord_activity'          , '16'                , 'spinal_cord_activity;';...
            'bone_marrow_activity'          , '16'                , 'bone_marrow_activity;';...
            'art_activity'                  , '17'                , 'artery_activity;';...
            'vein_activity'                 , '17'                , 'vein_activity;';...
            'bladder_activity'              , '18'                , 'bladder_activity;';...
            'prostate_activity'             , '19'                , 'prostate_activity;';...
            'asc_li_activity'               , '20'                , 'ascending_large_intest_activity;';...
            'trans_li_activity'             , '20'                , 'transcending_large_intest_activity;';...
            'desc_li_activity'              , '20'                , 'desc_large_intest_activity;';...
            'sm_intest_activity'            , '21'                , 'small_intest_activity;';...
            'rectum_activity'               , '22'                , 'rectum_activity;';...
            'sem_activity'                  , '23'                , 'sem_vess_activity;';...
            'vas_def_activity'              , '24'                , 'vas_def_activity;';...
            'test_activity'                 , '25'                , 'testicular_activity;';...
            'epididymus_activity'           , '26'                , 'epididymus_activity;';...
            'ejac_duct_activity'            , '27'                , 'ejaculatory_duct_activity;';...
            'pericardium_activity'          , '41'                , 'pericardium activity;';...
            'cartilage_activity'            , '16'                , 'cartilage activity;';...
            'intest_air_activity'           , '20'                , 'activity of intestine contents (air); ';...
            'ureter_activity'               , '28'                , 'ureter activity; ';...
            'urethra_activity'              , '28'                , 'urethra activity; ';...
            'lymph_activity'                , '29'                , 'lymph normal activity; ';...
            'lymph_abnormal_activity'       , '29'                , 'lymph abnormal activity; ';...
            'airway_activity'               , '7'                 , 'airway tree activity';...
            'uterus_activity'               , '30'                , 'uterus_activity;';...
            'vagina_activity'               , '31'                , 'vagina_activity;';...
            'right_ovary_activity'          , '32'                , 'right_ovary_activity;';...
            'left_ovary_activity'           , '32'                , 'left_ovary_activity;';...
            'fallopian_tubes_activity'      , '32'                , 'fallopian tubes_activity;';...
            'parathyroid_activity'          , '34'                , 'parathyroid_activity;';...
            'thyroid_activity'              , '34'                , 'thyroid_activity;';...
            'thymus_activity'               , '35'                , 'thymus_activity;';...
            'salivary_activity'             , '36'                , 'salivary_activity;';...
            'pituitary_activity'            , '37'                , 'pituitary_activity;';...
            'eye_activity'                  , '38'                , 'eye_activity;';...
            'lens_activity'                 , '39'                , 'eye_lens_activity;';...
            'lesn_activity'                 , '40'                , 'activity for heart lesion, plaque, or spherical lesion';...
            'energy'                        , '80'                , 'radionuclide energy in keV (range 1 - 40MeV, increments of 0.5 keV) ; for attn. map only';...
            'motion_defect_flag'            , '0'                 , sprintf('(0 = do not include, 1 = include) regional motion abnormality in the LV as defined by heart lesion parameters\n Creates lesion (defect) for the LEFT VENTRICLE ONLY.\n--------------------------------\ntheta_center: location of lesion center in circumferential dimension\ntheta center =    0.0  => anterior wall\ntheta center =  +90.0  => lateral   "\ntheta center = +180.0  => inferior  "\ntheta center = +270.0  => septal    "\n--------------------------------\ntheta_width : lesion width in circumferential dimension\nTOTAL width of defect in degrees. So for example a width of 90 deg.\nmeans that the width is 45 deg. on either side of theta center.\n--------------------------------\nx center :   lesion center in long-axis dimension\nx center = 0    -> base of LV\nx center = 1.0  -> apex of LV\n--------------------------------\nx width:  lesion width in long-axis dimension\ntotal width. Defect extend half the total width on either side of the\nx_center.\nNOTE: if the specified width extends beyond the boundaries of the LV\nthen the defect is cut off and the effective width is less than the\nspecified width. So for example...\n--------------------------------\nWall_fract : fraction of the LV wall that the lesion transgresses\nWall_fract = 0.0 => transgresses none of the wall\nWall_fract = 0.5 => transgresses the inner half of the wall\nWall_fract = 1.0 => trangresses the entire wall\n--------------------------------');...
            'ThetaCenter'                   , '0.0'               , 'theta center in deg. (between 0 and 360) ';...
            'ThetaWidth'                    , '100.0'             , 'theta width in deg., total width (between 0 and 360 deg.)';...
            'XCenterIndex'                  , '.5'                , 'x center (0.0=base, 1.0=apex, other fractions=distances in between)';...
            'XWidthIndex'                   , '60'                , 'x width, total in mm''s';...
            'Wall_fract'                    , '1.0'               , 'wall_fract, fraction of the outer wall transgressed by the lesion';...
            'motion_scale'                  , '0.0'               , 'scales the motion of the defect region (1 = normal motion, < 1 = reduced motion), altered motion blends with normal ';...
            'border_zone_long'              , '10'                , 'longitudinal width (in terms of number of control points) of transition between abnormal and normal motion';...
            'border_zone_radial'            , '5'                 , 'radial width (in terms of number of control points) of transition between abnormal and normal motion';...
            'x_location'                    , '0'                 , 'x coordinate (pixels) to place lesion';...
            'y_location'                    , '0'                 , 'y coordinate (pixels) to place lesion ';...
            'z_location'                    , '0'                 , 'z coordinate (pixels) to place lesion ';...
            'lesn_diameter'                 , '0.0'               , 'Diameter of lesion (mm)';...
            'tumor_motion_flag'             , '0'                 , 'Sets tumor motion (0 = default motion based on lungs, 1 = motion defined by user curve below)';...
            'tumor_motion_filename'         , 'tumor_curve.dat'   , 'Name of user defined motion curve for tumor';...
            'p_center_v'                    , '0.2'               , 'plaque center along the length of the artery (between 0 and 1)';...
            'p_center_u'                    , '0.5'               , 'plaque center along the circumference of the artery (between 0 and 1)';...
            'p_height'                      , '1.0'               , 'plaque thickness in mm.';...
            'p_width'                       , '2.0'               , 'plaque width in mm.';...
            'p_length'                      , '5.0'               , 'plaque length in mm.';...
            'p_id'                          , 'aorta'             , 'vessel ID to place the plaque in ';...
            'vec_factor'                    , '2'                 , 'higher number will increase the precision of the vector output';...
            'nurbs_save'                    , '0'                 , '';...
            'color_code'                    , '0'                 , '';...
            'ct_output'                     , '0'                 , '';...
            'body_flag'                     , '0'                 , '';...
            'organ_flag'                    , '0'                 , '';...
            'muscle_flag'                   , '0'                 , '';...
            'skeleton_flag'                 , '0'                 , '';...
            'bronch_flag'                   , '0'                 , '';...
            'bvess_flag'                    , '0'                 , '';...
            'heart_flag'                    , '0'                 , '';...
            'lung_scale'                    , '1.0'               , '';...
            'lv_radius_scale'               , '1.0'               , '';...
            'lv_length_scale'               , '1.0'               , '';...
            'use_res'                       , '0'                 , '';...
            'vol_rthyroid'                  , '0.0'               , '';...
            'vol_lthyroid'                  , '0.0'               , ''};
    end
    
    properties  (SetObservable = false)
        phantomDir = '';
        tumorDir   = '';
        outputDir  = '';
        
        phantomFileName = '';
        
        phantomFiles = '';
        tumorFiles   = '';
        
        combinedFiles = '';
        
        combinedDICOMFiles = '';
        combinedVOXFiles   = '';
        
        numberOfFrames = []; %123
        numberOfSlices = []; %140
        
        imgArraySize      = []; %256
        
        voxelDims          = [];
        voxelArray         = [];
        voxelDimsOriginal  = [];
        voxelArrayOriginal = [];
        
        currentSliceIndex   = [];
        
        img         = [];
        imgOriginal = [];
        
        imgScale = 'square';
        
        imgLength = 200; %cm
        imgWidth  = 51.2; %cm
        
        imageView         = 'Transverse';
        imageViewOriginal = 'Transverse';
        
        nrmse = [];
        
        pixelWidth   = [];
        sliceWidth   = [];
        startSlice   = [];
        endSlice     = [];
        subvoxelIndx = [];
        
        includeHeartPlaque = 0;
        includeHeartDefect = 0;
        includeTumor       = 0;
        
        energy = [];
        
        useStructureSetTumor = false;
        gtvVoxelized = [];
        
    end
    
    properties
        organId = {'myoLV',41, 'myocardium Left Ventricle';'myoRV',41, 'myocardium Right Ventricle';'myoLA',41, 'myocardium Left Atrium';'myoRA',41, 'myocardium Right Atrium';'bldplLV',41, 'blood pool Left Ventricle';'bldplRV',41, 'blood pool Right Ventricle';'bldplLA',41, 'blood pool Left Atrium';'bldplRA',41, 'blood pool Right Atrium';'body',1, 'body';'muscle',2, 'muscle';'brain',3, 'brain';'sinus',4, 'sinus';'liver',5, 'liver';'gall_bladder',6, 'gall bladder';'r_lung',7, 'right lung';'l_lung',7, 'left lung';'esophagus',8, 'esophagus';'laryngopharynx',9, 'laryngopharynx';'st_wall',10, 'stomach wall';'st_cnts',10, 'stomach contents';'pancreas',11, 'pancreas';'r_kidney_cortex',12, 'right kidney cortex';'r_kidney_medulla',12, 'right kidney medulla';'l_kidney_cortex',12, 'left kidney cortex';'l_kidney_medulla',12, 'left kideny medulla';'adrenal',13, 'adrenal gland';'r_renal_pelvis',14, 'right renal pelvis';'l_renal_pelvis',14, 'left renal pelvis';'spleen',15, 'spleen';'rib',16, 'rib';'cortical_bone',16, 'cortical bone';'spine',16, 'spine';'spinal_cord',16, 'spinal cord';'bone_marrow',16, 'bone marrow';'art',17, 'artery';'vein',17, 'vein';'bladder',18, 'bladder';'prostate',19, 'prostate';'asc_li',20, 'ascending colon';'trans_li',20, 'transverse colon';'desc_li',20, 'descending colon';'sm_intest',21, 'small intestine';'rectum',22, 'rectum';'sem',23, 'seminal vesicles';'vas_def',24, 'vas deferens';'test',25, 'testicles';'epididymus',26, 'epididymis';'ejac_duct',27, 'ejaculatory ducts';'pericardium',41, 'pericardium';'cartilage',16, 'cartilage';'intest_air',20, 'intestinal air';'ureter',28, 'ureter';'urethra',28, 'urethra';'lymph',29, 'lymph nodes';'lymph_abnormal',29, 'lymph nodes abnormal';'airway',7, 'airway';'uterus',30, 'uterus';'vagina',31, 'vagina';'right_ovary',32, 'right ovary';'left_ovary',32, 'left ovary';'fallopian_tubes',32, 'fallopian tubes';'parathyroid',34, 'parathyroid';'thyroid',34, 'thyroid';'thymus',35, 'thymus';'salivary',36, 'salivary gland';'pituitary',37, 'pituitary gland';'eye',38, 'eye';'lens',39, 'lens';'lesn',40, 'lesion'};
        %organId = {'body',1;'muscle',2;'brain',3;'sinus',4;'liver',5;'gall_bladder',6;'r_lung',7;'l_lung',7;'esophagus',8;'laryngopharynx',9;'st_wall',10;'st_cnts',10;'pancreas',11;'r_kidney_cortex',12;'r_kidney_medulla',12;'l_kidney_cortex',12;'l_kidney_medulla',12;'adrenal',13;'r_renal_pelvis',14;'l_renal_pelvis',14;'spleen',15;'rib',16;'cortical_bone',17;'spine',18;'spinal_cord',18;'bone_marrow',19;'art',20;'vein',21;'bladder',22;'prostate',23;'asc_li',24;'trans_li',24;'desc_li',24;'sm_intest',25;'rectum',26;'sem',27;'vas_def',28;'test',29;'epididymus',30;'ejac_duct',31;'pericardium',32;'cartilage',33;'intest_air',24;'ureter',34;'urethra',34;'myoLV',35;'myoRV',35;'myoLA',35;'myoRA',35;'bldplLV',35;'bldplRV',35;'bldplLA',35;'bldplRA',35;'lymph',35;'lymph_abnormal',36;'airway',37;'uterus',38;'vagina',39;'right_ovary',40;'left_ovary',40;'fallopian_tubes',41;'parathyroid',42;'thyroid',42;'thymus',43;'salivary',44;'pituitary',45;'eye',2;'lens',2;'lesn',2}
    end
    
    properties
        activities = {'myoLV',41;'myoRV',41;'myoLA',41;'myoRA',41;'bldplLV',41;'bldplRV',41;'bldplLA',41;'bldplRA',41;'body',1;'muscle',2;'brain',3;'sinus',4;'liver',5;'gall_bladder',6;'r_lung',7;'l_lung',7;'esophagus',8;'laryngopharynx',9;'st_wall',10;'st_cnts',10;'pancreas',11;'r_kidney_cortex',12;'r_kidney_medulla',12;'l_kidney_cortex',12;'l_kidney_medulla',12;'adrenal',13;'r_renal_pelvis',14;'l_renal_pelvis',14;'spleen',15;'rib',16;'cortical_bone',16;'spine',16;'spinal_cord',16;'bone_marrow',16;'art',17;'vein',17;'bladder',18;'prostate',19;'asc_li',20;'trans_li',20;'desc_li',20;'sm_intest',21;'rectum',22;'sem',23;'vas_def',24;'test',25;'epididymus',26;'ejac_duct',27;'pericardium',41;'cartilage',16;'intest_air',20;'ureter',28;'urethra',28;'lymph',29;'lymph_abnormal',29;'airway',7;'uterus',30;'vagina',31;'right_ovary',32;'left_ovary',32;'fallopian_tubes',32;'parathyroid',34;'thyroid',34;'thymus',35;'salivary',36;'pituitary',37;'eye',38;'lens',39;'lesn',40};
        volumes    = [];
        thickness  = [];
    end
    
    methods
        % Constructor
        function obj = xcat_model()
        end
        
        % Get methods
        function funcType  = getFunction(obj)
            funcType = obj.guiSelectedFunction;
        end
        
        function funcName  = getFunctionName(obj)
            if isempty(obj.getFunction)
                funcName = '';
            else
                funcName = obj.guiFunction{obj.getFunction};
            end
        end
        
        function phnDir    = getPhantomDir(obj)
            phnDir = obj.phantomDir;
        end
        
        function tumDir    = getTumorDir(obj)
            tumDir = obj.tumorDir;
        end
        
        function outDir    = getOutputDir(obj)
            outDir = obj.outputDir;
        end
        
        function phnFiles  = getPhantomFiles(obj)
            phnFiles = obj.phantomFiles;
        end
        
        function phnName = getPhantomFileName(obj)
            phnFiles = obj.getPhantomFiles;
            if ~iscell(phnFiles)
                phnFiles = {phnFiles};
            end
            
            [~, phnName,~] = fileparts(phnFiles{1});
        end
        
        function tumFiles  = getTumorFiles(obj)
            tumFiles = obj.tumorFiles;
        end
        
        function cbnFiles  = getCombinedFiles(obj)
            cbnFiles = obj.combinedFiles;
        end
        
        function anmFiles  = getAnimationFiles(obj)
            anmFiles = obj.animationFiles;
        end
        
        function numFrames = getNumberOfFrames(obj)
            numFrames = obj.numberOfFrames;
        end
        
        function numSlices = getNumberOfSlices(obj)
            numSlices = obj.numberOfSlices;
        end
        
        function arrSize   = getImageArraySize(obj)
            arrSize = obj.imgArraySize;
        end
        
        function sliceIndx = getSliceIndex(obj)
            sliceIndx = obj.currentSliceIndex;
        end
        
        function img       = getImage(obj)
            img = obj.img;
        end
        
        function img       = getImageOriginal(obj)
            img = obj.imgOriginal;
        end
        
        function img       = getImageSlice(obj, sliceIndex)
            if size(obj.img, 3) == 1
                img = obj.img;
                return;
            end
            img = obj.img(:,:,sliceIndex);
        end
        
        function imgScale  = getImageScale(obj)
            imgScale = obj.imgScale;
        end
        
        function imgView   = getImageView(obj)
            imgView = obj.imageView;
        end
        
        function imgView   = getImageViewOriginal(obj)
            imgView = obj.imageViewOriginal;
        end
        
        function imgMax    = getImageMax(obj, imgSlice)
            if isempty(obj.getImage)
                imgMax = [];
                return;
            end
            
            if any(imgSlice) < 0
                imgMax = [];
                return;
            end
            
            if nargin < 1
                imgSlice = 1:size(obj.getImage,3);
            end
            
            if numel(imgSlice) > 1
                imgMax = max(max(max(obj.getImageSlice(imgSlice))));
            else
                imgMax = max(max(obj.getImageSlice(imgSlice)));
            end
        end
        
        function imgMin    = getImageMin(obj, imgSlice)
            if isempty(obj.getImage)
                imgMin = [];
                return;
            end
            
            if any(imgSlice) < 0
                imgMin = [];
                return;
            end
            
            if nargin < 1
                imgSlice = 1:size(obj.getImage,3);
            end
            if numel(imgSlice) > 1
                imgMin = min(min(min(obj.getImageSlice(imgSlice))));
            else
                imgMin = min(min(obj.getImageSlice(imgSlice)));
            end
        end
        
        function imgLim    = getImageLimits(obj, sliceIndx)
            if nargin < 1
                sliceIndx = 1:size(obj.getImage, 3);
            end
            imgLim = [obj.getImageMin(sliceIndx) obj.getImageMax(sliceIndx)];
        end
        
        function dims      = getVoxelDimensions(obj)
            dims = obj.voxelDims;
        end
        
        function organ     = getOrgan(obj, organId)
            indx = ([obj.organId{:,2}] == organId)';
            if isempty(indx)
                organ = [];
            else
                organ = obj.organId(indx,3);
            end
        end
        
        
        
        %#=============#
        %# Set methods #
        %#=============#
        function setFunction(obj, functionIndex)
            obj.guiSelectedFunction = functionIndex;
        end
        
        function setImageArraySize(obj, arrSize)
            obj.imgArraySize = arrSize;
        end
        
        function setNumberOfSlices(obj, numofslices)
            obj.numberOfSlices = numofslices;
        end
        
        function setCurrentView(obj, view)
            obj.imageView = view;
        end
        
        function setOriginalView(obj, view)
            obj.imageViewOriginal = view;
        end
        
        function setSliceIndex(obj, indx)
            obj.currentSliceIndex = indx;
        end
        
        function setImageScale(obj, scale)
            obj.imgScale = scale;
        end
        
        function setImage(obj, img)
            obj.img = img;
        end
        
        function setImageOriginal(obj, img)
            obj.imgOriginal = img;
        end
        
        function setVoxelDimensions(obj, dims)
            obj.voxelDims = dims;
        end
        
        function setVoxelDimensionsOriginal(obj, dims)
            obj.voxelDimsOriginal = dims;
        end
        
        function setActivities(obj, values)
            % values is a cell array
            for i=1:length(values);
                if isfield(obj.activities, values{i,1})
                    if ischar(values{i,2})
                        values{i,2} = str2num(values{i,2});
                    end
                    obj.activities.(values{i,1}) = values{i,2};
                end
            end
        end
        
        function setActivity(obj, name, value)
            if isfield(obj.activities, name)
                if ischar(value)
                    value = str2num(value);
                end
                obj.activities.(name) = value;
            end
        end
        
        function setStartSlice(obj, val)
            obj.setParameterValue('startslice', value);
        end
        
        function setEndSlice(obj, val)
            obj.setParameterValue('endslice', value);
        end
        
        %function setParameterValue(obj, paramName, paramValue)
        %    indx = cellfun(@(x) ~isempty(strcmpi(paramName, x)), obj.parameters{:,1});
        %end
        
        %#===========#
        %# Utilities #
        %#===========#
        function parameters = parseParameterFile(obj, fname)
            i = 1;
            
            fid = fopen(fname, 'r');
            fline = fgetl(fid);
            while ischar(fline)
                if ~strcmpi(fline(1), '#') || ~isempty(fline)
                    newend = strfind(fline, '#') - 1;
                    if ~isempty(newend)
                        fline = fline(1:newend(1));
                    end
                    fline(fline==' ') = [];
                    fline = strtrim(fline);
                    
                    valIndx = strfind(fline, '=');
                    parameters{i,1} = fline(1:valIndx-1);
                    parameters{i,2} = fline(valIndx+1:end);
                    
                end
                fline = fgets(fid);
                i = i+1;
            end
            
            parameters(cellfun(@(x) isempty(x), parameters(:,1)), :) = [];
            
            fclose(fid);
        end
        
        function assignParameters(obj, fname)
            
            if nargin > 1
                fprintf('Reading from file %s\n', fname);
                obj.parameters = obj.parseParameterFile(fname);
            end
            
            % Obtain number of frames
            indxArraySize      = cellfun(@(x) ~isempty(strfind(x, 'out_frames')), obj.parameters(:,1));
            obj.numberOfFrames = str2num(obj.parameters{indxArraySize, 2});
            
            % Obtain array size from parameter file
            indxArraySize    = cellfun(@(x) ~isempty(strfind(x, 'array_size')), obj.parameters(:,1));
            obj.imgArraySize = str2num(obj.parameters{indxArraySize, 2});
            
            % Obtain pixel width
            indxArraySize    = cellfun(@(x) ~isempty(strfind(x, 'pixel_width')), obj.parameters(:,1));
            obj.pixelWidth = str2num(obj.parameters{indxArraySize, 2});
            
            % Obtain Slice width
            indxArraySize    = cellfun(@(x) ~isempty(strfind(x, 'slice_width')), obj.parameters(:,1));
            obj.sliceWidth = str2num(obj.parameters{indxArraySize, 2});
            
            % Obtain Subvoxel index
            indxArraySize    = cellfun(@(x) ~isempty(strfind(x, 'subvoxel_index')), obj.parameters(:,1));
            obj.subvoxelIndx = str2num(obj.parameters{indxArraySize, 2});
            
            % Obtain Start slice
            indxArraySize  = cellfun(@(x) ~isempty(strfind(x, 'startslice')), obj.parameters(:,1));
            obj.startSlice = str2num(obj.parameters{indxArraySize, 2});
            
            % Obtain End slice
            indxArraySize  = cellfun(@(x) ~isempty(strfind(x, 'endslice')), obj.parameters(:,1));
            obj.endSlice   = str2num(obj.parameters{indxArraySize, 2});
            
            % Obtain Energy
            indxArraySize  = cellfun(@(x) ~isempty(strfind(x, 'energy')), obj.parameters(:,1));
            obj.energy   = str2num(obj.parameters{indxArraySize, 2});
            
            % Set number of slices
            obj.numberOfSlices = obj.endSlice - obj.startSlice + 1;
            
            
            % Set Voxel array and dimensions
            obj.voxelDimsOriginal  = [obj.pixelWidth obj.pixelWidth obj.sliceWidth];
            obj.voxelArrayOriginal = [obj.imgArraySize obj.imgArraySize (obj.endSlice - obj.startSlice + 1) ];
            obj.voxelArray         = obj.voxelArrayOriginal;
            obj.voxelDims          = obj.voxelDimsOriginal;
            
            
            % Set activities
            nonemptyIndx    = cellfun(@(x) ~isempty(strfind(x, '_activity')), obj.parameters(:,1));
            activities      = obj.parameters(nonemptyIndx, 1:2);
            activities(:,3) = cellfun(@(x) strfind(x, '_activity')-1, activities(:,1), 'UniformOutput', false);
            organNames      = cellfun(@(x,y) x(1:y), activities(:,1), activities(:,3), 'UniformOutput', false);
            obj.setActivities([organNames, activities(:,2)]);
            
        end
        
        function readParametersFromFile(obj, fname)
            parameters = obj.parseParameterFile(fname);
            
            % Remove spaces from the parameters
            parameters(:,1) = cellfun(@(x) regexprep(x,'\s',''), parameters(:,1), 'UniformOutput', false);
            
            % Find the indices from parameters in the files corresponding to parameters
            % in the gui_model class parameters
            cellindx = [];
            for i=1:length(parameters)
                cellindx{i,1} = cellfun(@(x) strcmpi(x, parameters{i,1}), strtrim(obj.parameters(:,1))); %, 'UniformOutput', false);
            end
            
            indx = [];
            for i=1:length(cellindx)
                try
                    indx = cellfun(@(x) find(x==1), cellindx, 'UniformOutput', false);
                catch err
                    i
                    keyboard
                end
            end
            
            indx = cell2mat(indx);
                        
            % Set parameters to the model
            obj.parameters(indx,2) = parameters(:,2);
            
            % Assign other parameters
            obj.assignParameters;
        end
        
        function inputParameters(obj)
            prompt = {'Array size:', 'Pixel width (cm):', 'Slice width (cm):', 'Subvoxel pixel', 'Start slice:', 'End Slice:', 'Energy (keV):'};
            dlg_title = 'Input';
            num_lines = 1;
            def = {num2str(obj.imgArraySize), num2str(obj.pixelWidth), num2str(obj.sliceWidth), '1', '1', num2str(200./obj.sliceWidth), '80' };
            answer = inputdlg(prompt,dlg_title,num_lines,def);
            
            
            if isempty(answer)
                return;
            end
            
            if isempty(str2num(answer{1})) | str2num(answer{1}) <= 0 | isempty(str2num(answer{2})) | str2num(answer{2}) <= 0 |isempty(str2num(answer{3})) | str2num(answer{3}) <= 0
                return;
            end
            
            obj.imgArraySize = str2num(answer{1});
            obj.pixelWidth   = str2num(answer{2});
            obj.sliceWidth   = str2num(answer{3});
            obj.subvoxelIndx = str2num(answer{4});
            obj.startSlice   = str2num(answer{5});
            obj.endSlice     = str2num(answer{6});
            obj.energy       = str2num(answer{7});
            
            obj.numberOfSlices = obj.endSlice - obj.startSlice + 1;
            
            % Set Voxel array and dimensions
            obj.voxelDimsOriginal  = [obj.pixelWidth obj.pixelWidth obj.sliceWidth];
            obj.voxelArrayOriginal = [obj.imgArraySize obj.imgArraySize (obj.endSlice - obj.startSlice + 1) ];
            obj.voxelArray         = obj.voxelArrayOriginal;
            obj.voxelDims          = obj.voxelDimsOriginal;
        end
        
        
        
        function contdata = getContour(obj, ROIseq, contourName)
            cont = [];
            contRef = cellfun(@(x) strcmpi(x, contourName), ROIseq(:,2));
            cont    = ROIseq(contRef,4);
            
            items   = fieldnames(cont{1,1});
            for k=1:length(items)
                contdata{k,1} = cont{1,1}.(items{k}).ContourData;
                contdata{k,1} = reshape(contdata{k,1}, 3, [])';
                contdata{k,2} = [range(contdata{k,1}(:,1)) range(contdata{k,1}(:,2)) range(contdata{k,1}(:,3))];
                contdata{k,3} = [max(contdata{k,1}(:,1)) max(contdata{k,1}(:,2)) max(contdata{k,1}(:,3))];
                contdata{k,4} = [min(contdata{k,1}(:,1)) min(contdata{k,1}(:,2)) min(contdata{k,1}(:,3))];
            end
            
        end
        
        
        function [mu_, mup_] = attCoeffWater(obj, energy, voxelDimensions)
            muWater = [1,4078;1.50000000000000,1376;2,617.300000000000;3,192.900000000000;4,82.7800000000000;5,42.5800000000000;6,24.6400000000000;8,10.3700000000000;10,5.32900000000000;15,1.67300000000000;20,0.809600000000000;30,0.375600000000000;40,0.268300000000000;50,0.226900000000000;60,0.205900000000000;80,0.183700000000000;100,0.170700000000000;150,0.150500000000000;200,0.137000000000000;300,0.118600000000000;400,0.106100000000000;500,0.0968700000000000;600,0.0895600000000000;800,0.0786500000000000];
            mu_ = interp1(muWater(:,1), muWater(:,2), energy);
            mu_ = mu_*1.05; % Converts from cm2/g to 1/cm  assuming tumor density is 1.05 g/cm3
            % mu_ per pixel
            mup_ = mu_*voxelDimensions(1);
        end
        
        
        function [roiVox, roiVoxOutline, limits] = voxelize(obj, roiCont, voxelResolution )
            %VOXELIZE Summary of this function goes here
            %   Detailed explanation goes here
            
            
            if nargin == 1
                voxelResolution = [1 1 (roiCont{2,1}(1,3) - roiCont{1,1}(1,3))];
            end
            
            for k=1:length(roiCont)
                maxlimits(k,:) = roiCont{k,3};
                minlimits(k,:) = roiCont{k,4};
            end
            
            xyz_max = max(maxlimits);
            
            %s = sign(max(maxlimits));
            xyz_min = min(minlimits);
            
            roiVox        = [];
            roiVoxFilled  = [];
            roiVoxOutline = [];
            
            dx = voxelResolution(1);
            dy = voxelResolution(2);
            dz = voxelResolution(3);
            
            [xx, yy, zz] = ndgrid(xyz_min(1,1):dx:xyz_max(1,1), xyz_min(1,2):dy:xyz_max(1,2), xyz_min(1,3):dz:xyz_max(1,3));
            roiVox = zeros(size(xx,1), size(yy,2), size(zz,3));
            roiVoxFilled = roiVox;
            for i=1:length(roiCont)
                
                vec = [];
                
                xyz = roiCont{i,1};
                
                iz = floor(size(zz,3).*(xyz(1,3) - xyz_min(1,3))./(xyz_max(1,3) - xyz_min(1,3))); iz(iz<=0) = 1; iz(iz>size(zz,3)) = size(zz,3);
                for k=1:length(xyz)
                    iy = floor(size(yy,2).*(xyz(k,2) - xyz_min(1,2))./(xyz_max(1,2) - xyz_min(1,2))); iy(iy<=0) = 1; iy(iy>size(yy,2)) = size(yy,2);
                    ix = floor(size(xx,1).*(xyz(k,1) - xyz_min(1,1))./(xyz_max(1,1) - xyz_min(1,1))); ix(ix<=0) = 1; ix(ix>size(xx,1)) = size(xx,1);
                    roiVoxOutline(ix, iy, iz) = 1;
                    vec(k,1:2) = [ix, iy];
                end
                roiVox(:,:,iz) = poly2mask(vec(:,2), vec(:,1), size(roiVox(:,:,1),1), size(roiVox(:,:,1),2));
                roiVoxFilled(:,:,iz) = roipoly(roiVoxFilled(:,:,iz), vec(:,2), vec(:,1));
            end
            
            limits = [xyz_min(1,1) xyz_max(1,1) min(diff(xyz_min(1,1):xyz_max(1,1))); xyz_min(1,2) xyz_max(1,2) min(diff(xyz_min(1,2):xyz_max(1,2))); xyz_min(1,3) xyz_max(1,3) min(diff(xyz_min(1,3):xyz_max(1,3)))];
            
            % Find gaps in tumor
            gapIndx = squeeze(all(all(roiVox(:,:,:)==0)));
            
            if ~isempty(find(gapIndx == 1))
                zi = 1:size(roiVox,3);
                zq = zi(gapIndx);
                zi = zi(~gapIndx);
                [xxk,yyk,zzk] = ndgrid(1:size(roiVox,1), 1:size(roiVox,2), zi);
                F = griddedInterpolant(xxk, yyk, zzk, roiVox(:,:,~gapIndx));
                [xxq, yyq, zzq] = ndgrid(1:size(roiVox,1), 1:size(roiVox,2), zq);
                roiVox(:,:,gapIndx) = F(xxq, yyq, zzq);
                
                % Convert values less than on to one
                roiVox = logical(roiVox);
                roiVox = double(roiVox);
                
            end
            
            
            
            
            
        end
        
        
        function fnames  = sortFilenames(obj, fnames)
            if ~iscell(fnames)
                return;
            end
            
            %keyboard
            fnameInfo = cellfun(@(x) dir(x), fnames, 'UniformOutput', false);
            fileOrder = cellfun(@(x) x.datenum, fnameInfo, 'UniformOutput', false);
            fileOrder = cell2mat(fileOrder)';
            [~,indx] = sort(fileOrder);
            
            fnames = fnames(indx);
            
            
            % Double check based on filenames, i.e. look for numbers in
            % files like file_1.bin, file_2.bin etc. Not reliable as well.
            try
                if ismac || isunix
                    [numIndx1, numIndx2] = cellfun(@(x) regexp(x, '_\d{1,3}.bin'), fnames);
                    numIndx1 = numIndx1';
                    numIndx2 = numIndx2';
                    nums = cellfun(@(x) x(numIndx1+1:(numIndx2)), fnames,  'UniformOutput', false)';
                    
                    try
                        nums = cellfun(@(x)  cell2mat(textscan(x, '%d%*s')), nums);
                    catch err
                        errStr = sprintf('An error has occured when reading the phantom files.\nThe most probable reason is that there are other files in the same directory that confuse the algorithm.\nPlease try to separate the phantom files in a different directory and rerun combine');
                        uiwait(errordlg(errStr, 'Error'));
                        return;
                    end
                    [~, indx] = sort(nums);
                    
                    fnames = fnames(indx);
                end
            catch err
            end
            
        end
        
        function adjustView(obj, view)
            
            obj.img        = obj.imgOriginal;
            obj.voxelDims  = obj.voxelDimsOriginal;
            obj.voxelArray = obj.voxelArrayOriginal;
            
            if obj.convertHUFlag
                obj.convertHU;
            end
            
            if strcmpi(view, 'Transverse')
                for i=1:size(obj.img,3)
                    obj.img(:,:,i) = fliplr(rot90(obj.img(:,:,i), -1));
                end
                obj.voxelDims  = obj.voxelDimsOriginal;
                obj.voxelArray = obj.voxelArrayOriginal;
            elseif strcmpi(view, 'Sagittal')
                
                obj.img = permute(obj.img, [3 2 1]);
                obj.voxelDims  = obj.voxelDims([2 3 1]);
                obj.voxelArray = obj.voxelArray([3 2 1]);
                for i=1:size(obj.img,3)
                    obj.img(:,:,i) = rot90(obj.img(:,:,i),-2);
                    obj.img(:,:,i) = fliplr(obj.img(:,:,i));
                end
            elseif strcmpi(view, 'Coronal')
                obj.img = permute(obj.img, [3 1 2]);
                obj.voxelDims  = obj.voxelDims([1 3 2]);
                obj.voxelArray = obj.voxelArray([3 1 2]);
                for i=1:size(obj.img,3)
                    obj.img(:,:,i) = fliplr(rot90(obj.img(:,:,i),-2));
                end
                
            elseif strcmpi(view, 'Original')
                
            end
            
        end
        
        
        
        %#====================#
        %# Processing methods #
        %#====================#
        function createAnimation(obj, sliceIndex, currentView)
            if isempty(obj.phantomFiles) | numel(obj.phantomFiles) == 1
                obj.importPhantom();
            end
            
            if isempty(obj.img)
                return;
            end
            
            animg = [];
            
            hf   = figure('Name', 'Live View', 'NumberTitle', 'off', 'MenuBar', 'none', 'Toolbar', 'none');
            hfax = axes('Parent', hf);
            
            obj.loopStart = 1;
            obj.loopEnd   = numel(obj.phantomFiles);
            for f=1:numel(obj.phantomFiles)
                obj.loopIndex = f;
                
                obj.imgOriginal = obj.readBinaryPhantom(obj.phantomFiles{f});
                obj.imgOriginal = reshape(obj.imgOriginal, obj.voxelArrayOriginal(1), obj.voxelArrayOriginal(2), []);
                obj.img         = obj.imgOriginal;
                obj.adjustView(currentView);
                
                if isempty(animg)
                    animg = zeros(obj.voxelArray(1), obj.voxelArray(2), obj.numberOfFrames);
                end
                
                imshow(obj.img(:,:,sliceIndex), 'DisplayRange', [], 'Parent', hfax);
                drawnow
                
                animg(:,:, f) = obj.img(:,:,sliceIndex);
            end
            close(hf);
            
            obj.img = animg;
            
            
        end
        
        
        function phantom_comb = combineStructureSetTumor(obj, phantom, tumor, location, energy)
            
            locx = location(1) - floor(0.5*size(tumor,1)):location(1) + floor(0.5*size(tumor,1)) - 1;
            locy = location(2) - floor(0.5*size(tumor,2)):location(2) + floor(0.5*size(tumor,2)) - 1;
            locz = location(3) - floor(0.5*size(tumor,3)):location(3) + floor(0.5*size(tumor,3)) - 1;
            
            % [xxtum, yytum, zztum] = ndgrid(locx, locy, locz);
            
            phantom_comb = phantom;
            
            maxVal = max(max(max(phantom_comb)));
            
            [~, tumorVal] = attCoeffWater(energy, obj.voxelDimsOriginal);
            for k=1:numel(locz)
                for j=1:numel(locy)
                    for i=1:numel(locx)
                        if tumor(i,j,k) == 1
                            phantom_comb(locx(i), locy(j), locz(k)) = tumorVal; %1.2*maxVal
                        end
                    end
                end
            end
        end
        
        
        
        function combineTumor(obj)
            
            % Get phantom files
            [phantomFiles, phantomDir] = uigetfile(fullfile(obj.phantomDir, '*.bin'), 'Select phantom files', 'Multiselect', 'on');
            if ~iscell(phantomFiles)
                if ~phantomFiles
                    return;
                end
                phantomFiles = {phantomFiles};
            end
            
            if isempty(phantomFiles)
                return;
            end
            
            
            phantomFiles = fullfile(phantomDir, phantomFiles);
            phantomFiles = obj.sortFilenames(phantomFiles);
            
            obj.phantomDir     = phantomDir;
            obj.phantomFiles   = phantomFiles';
            obj.numberOfFrames = length(phantomFiles);
            
            if obj.useStructureSetTumor
                
                fprintf('Tumor anatomy will be imported from structure set\n');
                
                
                % if structure set tumor is used
                energy = obj.getParameterValue('energy');
                
                loc_x    = obj.getParameterValue('x_location');
                loc_y    = obj.getParameterValue('y_location');
                loc_z    = obj.getParameterValue('z_location');
                location = [loc_x loc_y loc_z];
                
                % Construct output filename
                [~, combinedFiles, ~] = cellfun(@(x) fileparts(x), obj.phantomFiles, 'UniformOutput', false);
                indx = cellfun(@(x) strfind(x, '_'), combinedFiles, 'UniformOutput', false);
                obj.combinedFiles = arrayfun(@(x) fullfile(obj.outputDir, sprintf('%sC%s.bin', x{:}(1:indx{1}-1), x{:}(indx{2}:end))), combinedFiles, 'UniformOutput', false);
                
                % The number of combined files will depend on the number of
                % phantom files available at each directory.
                numberOfCombinedFiles = length(obj.phantomFiles);
                
                fprintf('Number of combined files: %d\n', numberOfCombinedFiles);
                
                
                % Use loop to add tumor at specific location
                obj.loopStart = 1;
                obj.loopEnd   = numberOfCombinedFiles;
                for f=1:numberOfCombinedFiles;
                    % Update loop index
                    obj.loopIndex = f;
                    
                    phImg = obj.readBinaryPhantom(obj.phantomFiles{f});
                    
                    phantom_comb = combineStructureSetTumor(obj, phImg, obj.gtvVoxelized, location, energy);
                    
                    % Write combined phantom to file
                    fileout = fopen(obj.combinedFiles{f}, 'w');
                    fwrite(fileout, phantom_comb, 'float32');
                    fclose(fileout);
                    
                end
                
            else
                
                fprintf('Spherical tumor anatomy will be used\n');
                
                
                % Get tumor files
                [tumorFiles, tumorDir] = uigetfile(fullfile('..', obj.phantomDir, '*.bin'), 'Select tumor files', 'Multiselect', 'on');
                if ~iscell(tumorFiles)
                    if ~tumorFiles
                        return;
                    end
                    tumorFiles = {tumorFiles};
                end
                
                if isempty(tumorFiles)
                    return;
                end
                
                
                % Set output dir
                outputDir = uigetdir(fullfile('..', obj.phantomDir), 'Select output directory');
                if ~exist(fullfile(outputDir, 'Combined'), 'dir')
                    mkdir(outputDir, 'Combined');
                end
                
                tumorFiles = fullfile(tumorDir, tumorFiles);
                tumorFiles = obj.sortFilenames(tumorFiles);
                
                obj.tumorDir   = tumorDir;
                obj.tumorFiles = tumorFiles';
                
                
                % Output Directory
                if ~exist(fullfile(outputDir, 'Combined'), 'dir')
                    mkdir(outputDir, 'Combined');
                end
                obj.outputDir = fullfile(outputDir, 'Combined');
                
                % Read or input parameters
                [parfname, parpname] = uigetfile('*.par', 'Select parameter file to read parameters', 'Multiselect', 'off', obj.phantomDir);
                if ~parfname
                    fprintf('Parameter file not selected\n');
                    return;
                else
                    obj.assignParameters(fullfile(parpname,parfname));
                end
                
                % Construct output filename
                [~, combinedFiles, ~] = cellfun(@(x) fileparts(x), obj.phantomFiles, 'UniformOutput', false);
                indx = cellfun(@(x) strfind(x, '_'), combinedFiles, 'UniformOutput', false);
                if size(indx,1)  == 1
                    obj.combinedFiles{1} = fullfile(obj.outputDir, sprintf('%sC%s.bin', combinedFiles{:}(1:indx{1}(1)-1), combinedFiles{:}(indx{1}(2):end)));
                else
                    obj.combinedFiles = arrayfun(@(x) fullfile(obj.outputDir, sprintf('%sC%s.bin', x{:}(1:indx{1}-1), x{:}(indx{2}:end))), combinedFiles, 'UniformOutput', false);
                end
                
                % The number of combined files will depend on the number of
                % phantom or tumor files available at each directory.
                % The algorithm decides which of those has the lower number of
                % files and use that number as the end point of combination.
                numberOfCombinedFiles = min([length(obj.tumorFiles) length(obj.phantomFiles)]);
                
                fprintf('Number of combined files: %d\n', numberOfCombinedFiles);
                
                
                % Use loop to add tumor at specific location
                obj.loopStart = 1;
                obj.loopEnd   = numberOfCombinedFiles;
                for f=1:numberOfCombinedFiles;
                    % Update loop index
                    obj.loopIndex = f;
                    
                    phImg = obj.readBinaryPhantom(obj.phantomFiles{f});
                    tmImg = obj.readBinaryPhantom(obj.tumorFiles{f}  );
                    
                    % logical indexing of tumor locations in 3D Array
                    l = tmImg > 0;
                    
                    % Merge tumor data with the phantom image
                    phImg(l) = tmImg(l);
                    
                    % Write combined phantom to file
                    fileout = fopen(obj.combinedFiles{f}, 'w');
                    fwrite(fileout, phImg, 'float32');
                    fclose(fileout);
                    
                end
            end
        end
        
        
        function combineTumorAutomated(obj, phantomDir, tumorDir, outputDir, parNam)
                       
            phantomFiles = dir(fullfile(phantomDir, '*.bin'));
            phantomFiles = {phantomFiles.name}';
            
            % Sort phantom files
            atnIndx = strfind(phantomFiles{1}, 'atn');   % Find the index of word 'atn'
            for k=1:length(phantomFiles)
                [startIndx endIndx] = regexp(phantomFiles{k}(atnIndx:end), '\d{1,4}'); % Find the index of 1 up four consecutrive numbers
                startIndx = atnIndx + startIndx-1;
                endIndx   = atnIndx + endIndx-1;
                numFile(k,1) = str2num(phantomFiles{k}(startIndx:endIndx));  % Store the number found
            end
            % Sort the numbers
            [~, sortedIndx] = sort(numFile);
            
            % Sort the files
            phantomFiles = phantomFiles(sortedIndx);
            
            % Get Tumor files
            tumorFiles = dir(fullfile(tumorDir, '*.bin'));
            tumorFiles = {tumorFiles.name}';
            tumorFiles = tumorFiles(sortedIndx);     % Sort the tumor files
            
            %obj.tumorDir   = tumorDir;
            %obj.tumorFiles = tumorFiles';
            
            
            % Read parameter file
            parameterDir = sprintf('%s%s..%s', phantomDir, filesep, filesep);
            parFile = fullfile(parameterDir, parNam);
            
            
            
            % Construct output filename
            [~, combinedFiles, ~] = cellfun(@(x) fileparts(x), phantomFiles, 'UniformOutput', false);
            %indx = cellfun(@(x) strfind(x, '_'), combinedFiles, 'UniformOutput', false);
            indx = cellfun(@(x) strfind(x, 'atn'), combinedFiles, 'UniformOutput', false);
            if size(indx,1)  == 1
                combinedFiles{1} = fullfile(outputDir, sprintf('%sc_%s.bin', combinedFiles{:}(1:indx{1}(1)-1), combinedFiles{:}(indx{1}(2):end)));
            else
                combinedFiles = arrayfun(@(x) fullfile(outputDir, sprintf('%sc_%s.bin', x{:}(1:indx{1}-1), x{:}(indx{2}:end))), combinedFiles, 'UniformOutput', false);
            end
            
            % The number of combined files will depend on the number of
            % phantom or tumor files available at each directory.
            % The algorithm decides which of those has the lower number of
            % files and use that number as the end point of combination.
            numberOfCombinedFiles = min([length(tumorFiles) length(phantomFiles)]);
            
            fprintf('Number of combined files: %d\n', numberOfCombinedFiles);
           
            % Use loop to add tumor at specific location
            obj.loopStart = 1;
            obj.loopEnd   = numberOfCombinedFiles;
            for f=1:numberOfCombinedFiles;
                % Update loop index
                obj.loopIndex = f;
                
                try 
                    phImg = obj.readBinaryPhantom(fullfile(phantomDir, phantomFiles{f}));
                catch err
                    errStr = sprintf('There was an error reading the file:\n%s\n\nOperation will abort.', phantomFiles{f});
                    herr = errordlg(errStr, 'ERROR');
                    waitfor(herr);
                    return;
                end
                tmImg = obj.readBinaryPhantom(fullfile(tumorDir, tumorFiles{f}));
                
                % logical indexing of tumor locations in 3D Array
                l = tmImg > 0;
                
                % Merge tumor data with the phantom image
                phImg(l) = tmImg(l);
                
                % Write combined phantom to file
                fileout = fopen(combinedFiles{f}, 'w');
                fwrite(fileout, phImg, 'float32');
                fclose(fileout);
                
            end
        end
        
        
        
        function compareImages(obj)
            
            [fname1, pname1] = uigetfile('*.bin', 'Select first set of images', 'Multiselect', 'on');
            if ~iscell(fname1)
                if ~fname1
                    return;
                end
            end
            
            if ~iscell(fname1)
                fname1 = {fname1};
            end
            
            [fname2, pname2] = uigetfile(fullfile(pname1, '*.bin'), 'Select second set of images', 'Multiselect', 'on');
            if ~iscell(fname2)
                if ~fname2
                    return;
                end
            end
            
            if ~iscell(fname2)
                fname2 = {fname2};
            end
            
            outDir = uigetdir(pname2, 'Select Output Directory');
            if ~outDir
                return;
            end
            
            [parfname, parpname] = uigetfile('*.par', 'Select parameter file to read parameters', 'Multiselect', 'off', obj.phantomDir);
            if ~parfname
                % Enter voxel array size
                prompt    = {'Enter image matrix size:'};
                dlg_title = 'Input Matrix Dimensions';
                num_lines = 1;
                defVals   = {'256 256 140'};
                answer    = inputdlg(prompt,dlg_title,num_lines,defVals);
                
                if isempty(answer)
                    return;
                end
                
                obj.reset();
                
                voxelArray = str2num(answer{1});
                
                obj.voxelArrayOriginal = voxelArray;
                
            else
                obj.reset();
                obj.assignParameters(fullfile(parpname,parfname));
            end
            
            
            
            % Variable to store the difference image
            imgDiff = [];
            
            % Variable to store the Normalized Mean Squared Error
            nrmse = zeros(min([numel(fname1) numel(fname2)]), 1);
            
            
            % Loop through images
            obj.loopStart = 1;
            obj.loopEnd   = min([numel(fname1) numel(fname2)]);
            for k=1:min([numel(fname1) numel(fname2)])
                obj.loopIndex = k;
                img1 = obj.readBinaryPhantom(fullfile(pname1, fname1{k}));
                img2 = obj.readBinaryPhantom(fullfile(pname2, fname2{k}));
                
                % Check if images have the same size
                sizeFlag = obj.compareImageSize(img1, img2);
                if ~sizeFlag
                    uiwait(errordlg('Images do not have the same size'));
                    return;
                end
                
                % Set the imgdiff size
                if all(size(imgDiff)) == 0
                    imgDiff = zeros(size(img1,1), size(img1,2), size(img1,3), min([numel(fname1) numel(fname2)]));
                end
                
                % Create difference image
                %imgDiff(:, :, :, k) = img1 - img2;
                imgDiff    = img1 - img2;
                imgRelDiff = imgDiff./img1;
                imgRelDiff(isnan(imgRelDiff)) = 0;
                imgRelDiff(isinf(imgRelDiff)) = imgDiff(isinf(imgRelDiff));
                
                % Calculate Normalized Mean Squared Error
                nrmse(k) = obj.calculateNRMSE(img1, img2);
                
            end
            
            obj.imgOriginal = reshape(imgDiff, obj.voxelArrayOriginal(1), obj.voxelArrayOriginal(2), []);
            obj.img         = obj.imgOriginal;
            
            obj.imageViewOriginal = 'Transverse';
            obj.imageView         = 'Transverse';
            
            obj.nrmse = nrmse;
            
            obj.numberOfSlices = size(obj.imgOriginal, 3);
            obj.setSliceIndex(1);
            
            
            obj.phantomDir = outDir;
            
            % Store images and nrms to specified output directory
            for k=1:size(imgDiff,4)
                [~,fname] = fileparts(fname1{k});
                outfname = fullfile(outDir, sprintf('diffImg_%s.bin', fname));
                obj.writeBinaryImage(outfname, imgDiff(:,:,:, k));
            end
            
            % Store relative difference image
            [~,outfname1] = fileparts(fname1{1});
            outfname = fullfile(outDir, sprintf('relDiffImg_%s.bin', outfname1));
            obj.writeBinaryImage(outfname, imgRelDiff);
            
            save(fullfile(outDir, sprintf('NRMSE.mat')), 'nrmse');
            
        end
        
        
        function flag = compareImageSize(obj, img1, img2)
            size1 = size(img1);
            size2 = size(img2);
            
            flag = all(size1 == size2);
        end
        
        
        function nrmse = calculateNRMSE(obj, img1, img2)
            flag = obj.compareImageSize(img1, img2);
            
            if ~flag
                nrmse = [];
                return;
            end
            
            img1R  = reshape(img1,1,[]);
            img2R  = reshape(img2,1,[]);
            totDim = length(img1R);
            
            totnum = sum((img1R(2:totDim-1)-img2R(2:totDim-1)).^2);
            totden = sum(img1R(2:totDim-1).^2);
            nrmse  = sqrt(totnum / totden);
        end
        
        
        function centerTumor(obj)
            
            % Get phantom files
            [phantomFiles, phantomDir] = uigetfile(fullfile(obj.phantomDir, '*.bin'), 'Select combined phantom files', 'Multiselect', 'on');
            if ~iscell(phantomFiles)
                if ~phantomFiles
                    return;
                end
                phantomFiles = {phantomFiles};
            end
            
            if isempty(phantomFiles)
                return;
            end
            
            
            phantomFiles = fullfile(phantomDir, phantomFiles);
            phantomFiles = obj.sortFilenames(phantomFiles);
            
            obj.phantomDir     = phantomDir;
            obj.phantomFiles   = phantomFiles';
            obj.numberOfFrames = length(phantomFiles);
            
            
            % Get tumor files
            [tumorFiles, tumorDir] = uigetfile(fullfile('..', obj.phantomDir, '*.bin'), 'Select tumor files', 'Multiselect', 'on');
            if ~iscell(tumorFiles)
                if ~tumorFiles
                    return;
                end
                tumorFiles = {tumorFiles};
            end
            
            if isempty(tumorFiles)
                return;
            end
            
            % Select output directory
            [outputDir] = uigetdir(obj.phantomDir, 'Select output directory');
            if ~outputDir
                return;
            else
                outputDir = fullfile(outputDir, 'Centered');
                if ~exist(outputDir, 'dir')
                    mkdir(outputDir);
                end
            end
            
            % Read or input parameters
            [parfname, parpname] = uigetfile('*.par', 'Select parameter file to read parameters', 'Multiselect', 'off', obj.phantomDir);
            if ~parfname
                %obj.inputParameters;
                return;
            else
                obj.assignParameters(fullfile(parpname,parfname));
            end
            
            fprintf('Loading file: ...\n');
            obj.loopStart = 1;
            obj.loopEnd   = numel(obj.tumorFiles);
            for k=1:length(obj.tumorFiles)
                
                obj.loopIndex = k;
                
                % Load phantom data
                dataph = obj.readBinaryPhantom(obj.phantomFiles{k});
                
                % Load Tumor data
                datatum = obj.readBinaryPhantom(obj.tumorFiles{k}  );
                
                
                % Reshape tumor and phantom data to 3D
                datatum = reshape(datatum, obj.voxelArrayOriginal(1), obj.voxelArrayOriginal(2), []);
                dataph  = reshape(dataph , obj.voxelArrayOriginal(1), obj.voxelArrayOriginal(2), []);
                
                % Find the pixels with value greater than 0, i.e. the tumor
                % pixels
                l = datatum>0;
                
                % Locate centre of tumor at each slice of current frame
                tum_z = ceil(mean(find(mean(max(l)))));
                tum_y = ceil(mean(find(mean(max(permute(l,[3 1 2]))))));
                tum_x = ceil(mean(find(mean(max(permute(l,[3 2 1]))))));
                
                % Isocenter
                isocenter(k,:) = [tum_x, tum_y, tum_z];
                
                % Shift between current isocenter and isocenter at first
                % slice
                isoDiff(k,:)   = isocenter(k,:) - isocenter(1,:);
                
                % Array to store the centered data for the current frame
                data_cntr = zeros(obj.voxelArrayOriginal(1), obj.voxelArrayOriginal(2), obj.voxelArrayOriginal(3));
                
                
                % Translate phantom image according to isocenter difference
                i_cmb = [1:256] + isoDiff(k, 1);
                j_cmb = [1:256] + isoDiff(k, 2);
                l_cmb = [1:140] + isoDiff(k, 3);
                
                
                if k==10;keyboard;end
                %                   for j=1:size(data_cntr,3)
                %                       data_cntr(:,:,j) = imtranslate(dataph(:,:,j+isoDiff(3)),[isoDiff(1), isoDiff(2)]);
                %                   end
                
                %                 for n = 1:obj.voxelArrayOriginal(3)
                %                     for i=1:obj.voxelArrayOriginal(1);
                %                         for j=1:obj.voxelArrayOriginal(2);
                %
                %                             if n-isoDiff(k,3)>0 && i-isoDiff(k,1)>0 && j-isoDiff(k,2)>0 && n-isoDiff(k,3)<(obj.voxelArrayOriginal(3)+1) && i-isoDiff(k,1)<(obj.voxelArrayOriginal(1)+1) && j-isoDiff(k,2)<(obj.voxelArrayOriginal(2)+1);
                %                                 data_cntr(i-isoDiff(k,1), j-isoDiff(k,2), n-isoDiff(k,3)) = dataph(i,j,n);
                %                             end
                %
                %                         end
                %                     end
                %                 end
                
                % Write file
                
                [~, fname, ext] = fileparts(obj.phantomFiles{k});
                
                prefix  = fname(1:strfind(fname, sprintf('%s_', 'atn'))-1);
                outname = sprintf('%sCNTRatn_%d%s', prefix, k, ext);
                
                fidout = fopen(fullfile(outputDir, outname),'w');
                fwrite(fidout,data_cntr,'float32');
                fclose(fidout);
                
            end
            
            
            
        end
        
        function convertHU(obj)
            muAir = 0.001225*interp1(obj.muAir(:,1), obj.muAir(:,2), obj.energy);
            muH2O = 0.99997*interp1(obj.muH2O(:,1), obj.muH2O(:,2), obj.energy);
            obj.img = obj.img./obj.pixelWidth;
            obj.img = 1e3.*(obj.img - muH2O)./(muH2O - muAir);
            obj.convertHUFlag = 1;
        end
        
        function convertDICOM(obj)
            if isempty(obj.img)
                return;
            end
            
            odir = uigetdir(obj.phantomDir, 'Select output directory for DICOM files');
            if ~odir
                return;
            end
            
            obj.outputDir = odir;
            
            % Get some extra information from parameter file
            indxArraySize    = cellfun(@(x) ~isempty(strfind(x, 'gender')), obj.parameters(:,1));
            gender           = str2num(obj.parameters{indxArraySize, 2});
            
            % Convert image to HU if not already converted
            if ~obj.convertHUFlag
                obj.convertHU;
                obj.convertHUFlag = 1;
            end
            
            XCTmin = -obj.voxelDims(1) * obj.voxelArray(1)/2;
            YCTmin = -obj.voxelDims(2) * obj.voxelArray(2)/2;
            ZCTmin = -obj.voxelDims(3) * obj.voxelArray(3)/2;
            
            % Convert to uint16
            info.RescaleSlope     = 1;
            info.RescaleIntercept = min(min(min(obj.img)));
            img                   = uint16(obj.img + abs(min(min(min(obj.img)))));
            
            % Create the XCAT patient ID and gender info
            info.PatientID  = strcat('XCAT',datestr(clock, 'yyyymmddHHMMSS'));
            info.PatientSex = 'M';
            if gender
                info.PatientSex = 'F';
            end
            
            % Create Study and Series Information
            info.StudyDate         = datestr(clock, 'yyyymmdd');
            info.SeriesDate        = datestr(clock, 'yyyymmdd');
            info.StudyTime         = datestr(clock, 'hhmmss');
            info.SeriesTime        = datestr(clock, 'hhmmss');
            info.StudyID           = 'XCATStudy';
            info.StudyDescription  = 'XCAT Phantom Study';
            info.SeriesDescription = 'XCAT CT Series' ;
            info.SeriesNumber      = 1;
            info.AcquisitionNumber = 1;
            
            % Create Modality and Image type information
            info.Modality  = 'CT';
            info.ImageType = 'ORIGINAL\PRIMARY\AXIAL';
            
            % Create image realted information
            info.PixelSpacing   = 10.*[obj.voxelDims(1) obj.voxelDims(2)]';
            info.SliceThickness = 10.*obj.voxelDims(3);
            info.KVP            = obj.energy;
            
            % Create Patient Information
            info.PatientPosition    = 'HFS';
            info.ImageOrientationPatient = [1;0;0;0;1;0];
            %info.PatientOrientation = 'RLPA';
            
            % Generate SOP UIDs beforehand - to avoid errors - this is
            % MAtlab bug
            for k=1:obj.numberOfSlices
                try
                    SOPUID{k} = dicomuid;
                catch err
                    keyboard;
                end
            end
            
            % Start loop
            obj.loopStart = 1;
            obj.loopEnd   = obj.numberOfSlices;
            for slicenum = 1:obj.numberOfSlices
                
                if slicenum == 1
                    info.SOPInstanceUID       = dicomuid;
                    info.InstanceCreationDate = datestr(clock, 'yyyymmdd');
                    info.ContentDate          = datestr(clock, 'yyyymmdd');
                    info.InstanceCreationTime = datestr(clock, 'hhmmss');
                    info.ContentTime          = datestr(clock, 'hhmmss');
                    
                    info.ImagePositionPatient = [XCTmin YCTmin ZCTmin+(slicenum-1)*obj.voxelDims(3)]';
                    info.InstanceNumber       = slicenum;
                else
                    info = dicominfo(dcmfname);
                    info.SOPInstanceUID = SOPUID{slicenum};
                    info.ImagePositionPatient = [XCTmin YCTmin ZCTmin+(slicenum-1)*obj.voxelDims(3)]';
                    info.InstanceNumber       = slicenum;
                end
                
                
                %dcmfname = fullfile(obj.outputDir, strcat(info.Modality, '.', info.SOPInstanceUID, '.dcm'));
                dcmfname = fullfile(obj.outputDir, sprintf('CT.%s.dcm', info.SOPInstanceUID));
                if slicenum == 1
                    dicomwrite(img(:,:,slicenum), dcmfname, info);
                else
                    %fprintf('Deviate %d\n', slicenum);
                    try
                        dicomwrite(img(:,:,slicenum), dcmfname, info, 'CreateMode', 'Copy');
                    catch err
                        keyboard
                    end
                end
                %dicomwrite(imageHU(:,:,slicenum)', dcmfname, info);
                obj.loopIndex = slicenum;
                
                %info.SOPInstanceUID = [];
                %dcmfname = [];
                
            end
            
        end
        
        
        % #*********#
        % #   AIP   #
        % #*********#
        function createAIP(obj)
            [fname, pname] = uigetfile('*.bin', 'Select phantom file(s)', 'MultiSelect', 'on');
            if ~iscell(fname)
                if ~fname
                    return;
                end
            else
                if isempty(fname)
                    return;
                end
            end
            
            [parfname, parpname] = uigetfile('*.par', 'Select parameter file to obtain parameters', 'Multiselect', 'off', pname);
            if ~parfname
                obj.inputParameters;
            else
                obj.assignParameters(fullfile(parpname,parfname));
                %obj.readParametersFromFile(fullfile(parpname,parfname));
            end
            
            odir = uigetdir(pname, 'Select output directory for AIP file');
            if ~odir
                return;
            end
            
            filenames = fullfile(pname, fname);
            
            if ~iscell(filenames)
                filenames = {filenames};
                fname = {fname};
            end
            
            aipimg = zeros(obj.imgArraySize, obj.imgArraySize, (obj.endSlice-obj.startSlice+1));
            
            obj.loopStart = 1;
            obj.loopEnd   = numel(filenames);
            for f=1:numel(filenames);
                obj.loopIndex = f;
                
                img = obj.readBinaryPhantom(filenames{f});
                img = reshape(img,obj.imgArraySize, obj.imgArraySize,[]);
                
                aipimg = aipimg + img/numel(filenames);
            end
            
            
            
            if ~exist(fullfile(odir, 'AIP'), 'dir')
                mkdir(odir, 'AIP');
            end
            
            indx = strfind(fname{1}, '_');
            aipFile = fullfile(odir, 'AIP', strcat(fname{1}(1:indx(end)), 'aip.bin'));
            fileid = fopen(aipFile,'w');
            fwrite(fileid, aipimg, 'float32');
            fclose(fileid);
            
            fprintf('AIP image succesfully created\n');
            
        end
        
        % #*********#
        % #   MIP   #
        % #*********#
        function createMIP(obj)
            
            [fname, pname] = uigetfile(fullfile(obj.phantomDir, '*.bin'), 'Select phantom file(s)', 'MultiSelect', 'on');
            if ~iscell(fname)
                if ~fname
                    return;
                end
            else
                if isempty(fname)
                    return;
                end
            end
            
            [parfname, parpname] = uigetfile(fullfile(obj.phantomDir, '*.par'), 'Select parameter file to obtain parameters', 'Multiselect', 'off', pname);
            if ~parfname
                obj.inputParameters;
            else
                obj.assignParameters(fullfile(parpname,parfname));
            end
            
            odir = uigetdir(pname, 'Select output directory for MIP file');
            if ~odir
                return;
            end
            
            filename = fullfile(pname, fname);
            if ~iscell(filename)
                filename = {filename};
                fname = {fname};
            end
            
            mipimg = zeros(obj.voxelArrayOriginal);
            
            obj.loopStart = 1;
            obj.loopEnd   = numel(filename);
            for f=1:numel(filename);
                obj.loopIndex = f;
                
                img = obj.readBinaryPhantom(filename{f});
                
                if f == 1
                    mipimg = img;
                else
                    indx = img > mipimg;
                    mipimg(indx) = img(indx);
                end
            end
            
            
            if ~exist(fullfile(odir, 'MIP'), 'dir')
                mkdir(odir, 'MIP');
            end
            
            indx = strfind(fname{1}, '_');
            mipFile = fullfile(odir, 'MIP', strcat(fname{1}(1:indx(end)), 'mip.bin'));
            fileid = fopen(mipFile,'w');
            fwrite(fileid, mipimg, 'float32');
            fclose(fileid);
            
        end
        
        %#================#
        %# Import methods #
        %#================#
        function importPhantom(obj)
            [fname, pname] = uigetfile('*.bin', 'Select phantom .bin file(s)', 'Multiselect', 'on');
            if ~iscell(fname)
                if ~fname
                    return;
                end
            end
            
            obj.reset;
            
            
            [parfname, parpname] = uigetfile('*.par', 'Select parameter file to obtain parameters', 'Multiselect', 'off', pname);
            if ~parfname
                obj.inputParameters;
            else
                obj.assignParameters(fullfile(parpname,parfname));
            end
            
            if isempty(obj.voxelArrayOriginal) || obj.voxelArrayOriginal(1) <= 0 || obj.voxelArrayOriginal(2) <= 0 || obj.voxelArrayOriginal(3) <= 0
                uiwait(errordlg('Voxel Array Size is not set or canot be read from parameter file'));
                return;
            end
            
            obj.phantomFiles = fullfile(pname, fname);
            
            if iscell(fname)
                obj.imgOriginal     = obj.readBinaryPhantom(fullfile(pname, fname{1}));
                
                obj.phantomFiles    = obj.sortFilenames(obj.phantomFiles);
                obj.phantomFileName = fname{1};
                
                obj.numberOfFrames  = length(obj.phantomFiles);
                
            else
                obj.imgOriginal = obj.readBinaryPhantom(fullfile(pname, fname));
                
                obj.phantomFileName = fname;
                obj.numberOfFrames  = 1;
            end
            
            
            
            obj.imgOriginal = reshape(obj.imgOriginal, obj.voxelArrayOriginal(1), obj.voxelArrayOriginal(2), []);
            obj.img         = obj.imgOriginal;
            
            obj.imageViewOriginal = 'Transverse';
            %obj.imageView         = 'Transverse';
            
            obj.phantomDir = pname;
            
        end
        
        
        function importMatlabPhantom(obj)
            [fname, pname] = uigetfile('*.mat', 'Select phantom .mat file(s)', 'Multiselect', 'on');
            if ~iscell(fname)
                if ~fname
                    return;
                end
            end
            
            obj.reset;
            
            [parfname, parpname] = uigetfile('*.par', 'Select parameter file to obtain parameters', 'Multiselect', 'off', pname);
            if ~parfname
                obj.inputParameters;
            else
                obj.assignParameters(fullfile(parpname,parfname));
            end
            
            if iscell(fname)
                load(fullfile(pname, fname{1}));
                
                data = reshape(data, obj.voxelArrayOriginal(1), obj.voxelArrayOriginal(2), []);
                
                obj.numberOfSlices = size(data, 3);
                obj.setSliceIndex(1);
                
                obj.imgOriginal = data;
                obj.img         = obj.imgOriginal;
                
                obj.phantomFiles   = fullfile(pname, fname);
                obj.phantomFiles   = obj.sortFilenames(obj.phantomFiles);
                
                
                obj.numberOfFrames = length(obj.animationFiles);
                
            else
                load(fullfile(pname, fname));
                data = reshape(data, obj.voxelArrayOriginal(1), obj.voxelArrayOriginal(2), []);
                
                obj.numberOfSlices = size(data, 3);
                obj.setSliceIndex(1);
                
                obj.imgOriginal = data;
                obj.img         = obj.imgOriginal;
            end
            
            obj.imageViewOriginal = 'Transverse';
            obj.imageView         = 'Transverse';
            
            
            obj.phantomDir = pname;
        end
        
        %         function importTumor(obj)
        %             [fname, pname] = uigetfile(fullfile(obj.phantomDir, '*.bin'), 'Select tumor-only .bin file(s)', 'Multiselect', 'on');
        %             if ~iscell(fname)
        %                 if ~fname
        %                     return;
        %                 end
        %             end
        %
        %             obj.tumorFiles = fullfile(pname, fname);
        %             if numel(obj.tumorFiles)>1
        %                 obj.tumorFiles = obj.sortFilenames(obj.tumorFiles);
        %             end
        %             obj.tumorDir   = pname;
        %
        %         end
        
        
        function img = readBinaryPhantom(obj, fname)
            if isempty(fname)
                return;
            end
            
            fid = fopen(fname);
            img = fread(fid, 'float32');
            fclose(fid);
            
        end
        
        
        function importStructureSet(obj, fname)
            
            if ~fname
                [sname, dname] = uigetfile('*.dcm', 'Select Structure Set DICOM file', 'Multiselect', 'Off');
                if ~sname
                    return;
                end
                
                fname = fullfile(dname, sname);
            end
            
            dcminfo = dicominfo(fname);
            
            % Find ROI sequence in Structure Set
            contourItems = fieldnames(dcminfo.StructureSetROISequence);
            ROIseq(:,1)  = cellfun(@(x) dcminfo.StructureSetROISequence.(x).ROINumber, contourItems, 'UniformOutput', false);
            ROIseq(:,2)  = cellfun(@(x) dcminfo.StructureSetROISequence.(x).ROIName  , contourItems, 'UniformOutput', false);
            
            
            % Find ROI Contours
            contourItems = fieldnames(dcminfo.ROIContourSequence);
            
            % Get ROI Contour Ref number
            refNum = cellfun(@(x) dcminfo.ROIContourSequence.(x).ReferencedROINumber, contourItems); %, 'UniformOutput', false);
            
            % Get ROI Contour Color
            ROIcolor = cellfun(@(x) dcminfo.ROIContourSequence.(x).ROIDisplayColor, contourItems, 'UniformOutput', false);
            ROIseq(:,3) = ROIcolor([ROIseq{:,1}]);
            
            % Find Contoured ROI
            ROIwithContour = cellfun(@(x) isfield(dcminfo.ROIContourSequence.(x), 'ContourSequence'), contourItems);
            ROIcontItem         = cellfun(@(x) dcminfo.ROIContourSequence.(x).ContourSequence, contourItems(ROIwithContour), 'UniformOutput', false);
            ROIcont  = cellfun(@(x) dcminfo.ROIContourSequence.(x).ContourSequence, contourItems(ROIwithContour), 'UniformOutput', false);
            
            ROIseq(refNum(ROIwithContour),4) = ROIcont;
            
            
            % Get the contour for the gtv
            gtvCont = getContour(ROIseq, 'GTV');
            
            if isemtpy(gtvCont)
                uiwait(errodlg('GTV not found in structure set'));
                return;
            end
            
            % Voxelize the tumor
            [obj.gtvVoxelized, ~, ~] = voxelize(obj, gtvCont, obj.voxelDimsOriginal);
            
            % Set the flag to true
            obj.useStructureSetTumor = true;
            
        end
        
        
        
        function importWorkspacePhantom(obj)
            v = evalin('base', 'who');
            
            if isempty(v)
                img = [];
                uiwait(errordlg('The base workspace seems empty'));
                return;
            end
            
            [Selection] = listdlg('ListString',v);
            
            if ~Selection
                img = [];
                return;
            end
            if isempty(Selection)
                img = [];
                return;
            end
            
            obj.imgOriginal = evalin('base', v{Selection});
            obj.img         = obj.imgOriginal;
            
            viewStr = {'Transverse', 'Sagittal', 'Coronal', 'Original'};
            [Selection,ok] = listdlg('ListString', viewStr, 'Name', 'Select Image View');
            if ~ok
                uiwait(errordlg('A view must be selected'));
                return;
            end
            
            obj.setOriginalView(viewStr{Selection});
            obj.setCurrentView(viewStr{Selection});
            
            obj.voxelArrayOriginal = size(obj.imgOriginal);
            obj.voxelArray = obj.voxelArrayOriginal;
            
            x = inputdlg('Enter voxel dimensions:', 'Voxel Dimensions', [1 50], {'0.2 0.2 0.25'} );
            obj.voxelDimsOriginal = str2num(x{:});
            obj.voxelDims = obj.voxelDimsOriginal;
            
        end
        
        
        %#================#
        %# Export methods #
        %#================#
        function exportAnimation(obj, folder)
            
            if nargin < 1
                if obj.outputDir =='';
                    folder = obj.phantomDir;
                else
                    folder = obj.outputDir;
                end
            end
            
            if isempty(obj.img)
                return;
            end
            
            prompt = {'Framerate (frames/sec):', 'Quality:'};
            dlg_title = 'Input';
            num_lines = 1;
            def = {'30', '100'};
            answer = inputdlg(prompt,dlg_title,num_lines,def);
            
            if isempty(answer)
                return;
            end
            
            [fname, pname] = uiputfile(fullfile('.','animation.avi'), 'Export Anbimation File');
            if isempty(fname) | ~fname
                return;
            end
            
            videoparam.frameRate = str2num(answer{1});
            videoparam.quality   = str2num(answer{2});
            
            
            videoFile            = VideoWriter(fullfile(pname, fname));
            videoFile.Quality    = videoparam.quality;
            videoFile.FrameRate  = videoparam.frameRate;
            
            open(videoFile);
            
            for frame = 1:size(obj.img, 3)
                writeVideo(videoFile, mat2gray(obj.img(:,:,frame)));
            end
            
            fprintf('Frames written: %d\nDuration:%.2f\n', videoFile.FrameCount, videoFile.Duration);
            
            close(videoFile);
            
        end
        
        function exportWorkspace(obj)
            if ~isempty(obj.img)
                assignin('base', 'img', obj.img);
            end
        end
        
        function exportASCII(obj)
            
            if obj.outputDir =='';
                folder = obj.phantomDir;
            else
                folder = obj.outputDir;
            end
            
            if isempty(obj.img)
                return;
            end
            
            [fname, pname] = uiputfile({'*.dat';'*.txt';'*.asc'}, 'Save ASCII Phantom', fullfile(folder, 'phantom.dat'));
            if ~fname
                return;
            end
            
            %dlmwrite(fullfile(pname, fname), [size(obj.img,1) size(obj.img,2) size(obj.img,3)], 'delimiter', ' ');
            %dlmwrite(fullfile(pname, fname), obj.voxelDims, 'delimiter', ' ', '-append');
            %for i=1:size(obj.img, 3)
            %    fprintf('Writing slice %d out of %d\n', i, size(obj.img, 3));
            %    dlmwrite(fullfile(pname, fname), obj.img(:,:,i), 'delimiter', '', '-append');
            %end
            
            fid = fopen(fullfile(pname, fname), 'w');
            fprintf(fid, '%d %d %d\n', size(obj.img,1), size(obj.img,2), size(obj.img,3));
            fprintf(fid, '%.3f %3f %3f\n', obj.voxelDims(1), obj.voxelDims(2), obj.voxelDims(3));
            
            obj.loopStart = 1;
            obj.loopEnd = size(obj.img, 3);
            
            for i=1:size(obj.img, 3)
                obj.loopIndex = i;
                %fprintf('Writing Slice %d/%d\n', i, size(obj.img, 3));
                for j=1:size(obj.img, 2)
                    %for k=1:size(obj.img,1)
                    %    fprintf(fid, '%f ', obj.img(k,j,i));
                    %end
                    fprintf(fid, '%f ', obj.img(:,j,i));
                    fprintf(fid, '\n'); 
                end
                fprintf(fid, '\n'); 
            end
            
            fclose(fid);
            
            % Check if activity phantom is checked
            val = obj.getParameterValue('act_phan_each');
            
            if val
                obj.activities = obj.parameters(169:237,:);
                obj.activities(:,1) = cellfun(@(x) regexprep(x,'_act', ''), obj.activities(:,1), 'UniformOutput', false);
                
                % unique activity values
                uac = unique(obj.activities(:,2));
                uac = sort(arrayfun(@(x) str2num(x{1}), uac));
                
                fid = fopen(fullfile(pname,'AttenuationRange.dat'), 'w');
                
                fprintf(fid, '%d', length(uac)+1);
                fprintf(fid, '0 0 Air false 0.0 0.0 0.0 0.2\n');
                
                keyboard
                for k=1:length(unique(obj.activities(:,2)))
                    
                end
                
            end
            
            
            
           
            
        end
        
        
        function indx = findParameterIndex(obj, parametername)
            indx = cellfun(@(x) ~isempty(strfind(x, parametername)), [obj.parameters(:,1)]);
            indx = find(indx>0);
        end
        
        
        function setParameterValue(obj, parametername, value)
            indx = cellfun(@(x) strcmpi(x,parametername), [obj.parameters(:,1)]);
            indx = find(indx>0);
            if strcmpi(parametername, 'bladder_activity')
                indx = indx(2);
            end
            
            if ~ischar(value)
                obj.parameters{indx,2} = num2str(value);
            else
                obj.parameters{indx,2} = value;
            end
        end
        
        
        function val = getParameterValue(obj, parametername)
            indx = cellfun(@(x) strcmpi(x,parametername), [obj.parameters(:,1)]);
            indx = find(indx>0);
            val = obj.parameters{indx,2};
        end
        
        %#=======#
        %# Reset #
        %#=======#
        function reset(obj)
            obj.phantomDir = '';
            obj.tumorDir   = '';
            obj.outputDir  = '';
            
            obj.phantomFileName = '';
            
            obj.phantomFiles = '';
            obj.tumorFiles   = '';
            
            obj.combinedFiles = '';
            
            obj.combinedDICOMFiles = '';
            obj.combinedVOXFiles   = '';
            
            obj.numberOfFrames = []; %123
            obj.numberOfSlices = []; %140
            
            obj.imgArraySize      = []; %256
            
            obj.voxelDims          = [];
            obj.voxelArray         = [];
            obj.voxelDimsOriginal  = [];
            obj.voxelArrayOriginal = [];
            
            obj.currentSliceIndex   = [];
            
            
            obj.img         = [];
            obj.imgOriginal = [];
            
            obj.imgScale = 'square';
            
            obj.imgLength = 200; %cm
            obj.imgWidth  = 51.2; %cm
            
            obj.imageView         = 'Transverse';
            obj.imageViewOriginal = 'Transverse';
            
            
            obj.pixelWidth   = [];
            obj.sliceWidth   = [];
            obj.startSlice   = [];
            obj.endSlice     = [];
            obj.subvoxelIndx = [];
            
            obj.includeHeartPlaque = 0;
            obj.includeHeartDefect = 0;
            obj.includeTumor       = 0;
            
            obj.energy = [];
            
            obj.useStructureSetTumor = 0;
            obj.gtvVoxelized = [];
            
            obj.nrmse = [];
        end
        
        
        %#================#
        %# Activity Image #
        %#================#
        function img = activityImage(obj, scale)
            if nargin < 2
                scale = [256 256 1000];
            end
            img = [];
            try
                load('gui_figs/imgActivity.mat');
                img = obj.scaleImage(img, scale);
                img = permute(img, [3 2 1]);
            catch err
                if strfind(err.identifier, 'nomem')
                    load('gui_figs/imgActivity_lowRes.mat');
                    %img = obj.scaleImage(img, scale);
                    img = permute(img, [3 2 1]);
                end
                uiwait(warndlg(sprintf('Due to memory issues a lower resolution phantom is loaded in the first screen.\nChanging the resolution of the phantom may crash the software.\nThe phantom resolution at the first screen is a good indication of what your system can cope with in terms of XCAT phantom generation.')));
            end
            img1 = [];
            for i=1:size(img,3)
                img1(:,:,i) = rot90(img(:,:,i),1);
            end
            img = img1;
            clear img1;
        end
        
        %         function img = adjustActivityImage(obj)
        %             img = [];
        %             imgZ = round(obj.imgLength./obj.voxelDimsOriginal(3));
        %             imgX = round(obj.imgWidth./obj.voxelDimsOriginal(2));
        %             imgY = imgX;
        %
        %             for k=1:imgY;
        %                 img(:,:,k) = imresize(obj.img(:,:,k),[imgX imgZ]);
        %             end
        %
        %             obj.img = [];
        %             obj.img = img;
        %
        %             if obj.endSlice > size(obj.img,2)
        %                 obj.endSlice = size(obj.img,2);
        %             end
        %
        %         end
        %
        function img_scaled = scaleImage(obj, img, dims, view)
            
            if nargin < 4
                % Assumes img input in trasverse view,
                % e.g. size(img) = 256 x 256 x 800
                view = 'transverse';
            end
            
            %imgLength = 200; %cm
            %imgWidth  = 51.2; %cm
            
            if strcmpi(view, 'transverse')
                % Original and new resolution
                curres = [obj.imgWidth obj.imgWidth obj.imgLength]./size(img);
                newres = [obj.imgWidth obj.imgWidth obj.imgLength]./dims;
                
                % Original grid and grid for interpolation
                [xx, yy, zz]    = ndgrid(0:curres(1):(obj.imgWidth-curres(1)), 0:curres(2):(obj.imgWidth-curres(2)), 0:curres(3):(obj.imgLength)-curres(3));
                [xxi, yyi, zzi] = ndgrid(0:newres(1):(obj.imgWidth-newres(1)), 0:newres(2):(obj.imgWidth-newres(2)), 0:newres(3):(obj.imgLength-newres(3)));
                
                % Interpolant function
                F = griddedInterpolant(xx, yy, zz, img);
                
                % Interpolate
                img_scaled = F(xxi, yyi, zzi);
                
            elseif strcmpi(view, 'sagittal') | strcmpi(view, 'coronal')
                % e.g. size(img) = 800 x 256 x 256
                
            elseif strcmpi(view, 'sagittal_rotated') | strcmpi(view, 'coronal_rotated')
                % e.g. size(img) = 256 x 800 x 256
                
                % Original and new resolution
                curres = [obj.imgWidth obj.imgLength obj.imgWidth]./size(img);
                newres = [obj.imgWidth obj.imgLength obj.imgWidth]./dims;
                
                % Construct position vectors
                curx = 0:curres(1):(obj.imgWidth  - curres(1));
                cury = 0:curres(3):(obj.imgWidth  - curres(3));
                curz = 0:curres(2):(obj.imgLength - curres(2));
                
                newx = 0:newres(1):(obj.imgWidth  - newres(1));
                newy = 0:newres(3):(obj.imgWidth  - newres(3));
                newz = 0:newres(2):(obj.imgLength - newres(2));
                
                % Original grid and grid for interpolation
                [xx , zz , yy ] = ndgrid(curx, curz, cury);
                [xxi, zzi, yyi] = ndgrid(newx, newz, newy);
                
                % Interpolant function
                F = griddedInterpolant(xx, zz, yy, img);
                
                % Interpolate
                img_scaled = F(xxi, zzi, yyi);
                
            else
                img_scaled = [];
                
            end
            
        end
        
        function img_cropped = cropImage(obj, img, limits)
            % Crop always along the third dimension
            
            img_cropped = img(:,:,limits(1):limits(2));
            
        end
        
        
        function writeAP(obj, apdata, apfilename)
            fidAP = fopen(apfilename, 'w');
            
            fprintf(fidAP, '%d :N\n\n', size(apdata,1));
            fprintf(fidAP, 'Control Points:\n');
            
            for k=1:size(apdata,1)
                fprintf(fidAP,'%.6f %.6f %.6f\n', apdata(k,1), apdata(k,2), 0.0);
            end
            
            fclose(fidAP);
            
        end
        
        
        function writeDC(obj, dcdata, dcfilename)
            fidDC = fopen(dcfilename, 'w');
            
            fprintf(fidDC, '%d :N\n\n', size(dcdata,1));
            fprintf(fidDC, 'Control Points:\n');
            
            for k=1:size(dcdata,1)
                fprintf(fidDC, '%.6f %.6f %.6f\n', dcdata(k,1), dcdata(k,2), 0.0);
            end
            
            fclose(fidDC);
            
        end
        
        
        function writeBinaryImage(obj, filename, imgdata)
            
            fileid = fopen(filename,'wb');
            fwrite(fileid, imgdata, 'float32');
            fclose(fileid);
            
        end
        
        function writeParameterFile(obj, filename, phmode, motion)
            fid = fopen(filename, 'w');
            
            fprintf(fid, '#-----------------------------------------------------------------------------------------------#\n');
            fprintf(fid, '# This parameter file was created by XCAT_GUI Matlab application developed by Marios Myronakis  #\n');
            fprintf(fid, '# 2015 - Brigham and Women''s Hospital, Dana Farber Cancer Institute & Harvard Medical School    #\n');
            fprintf(fid, '# Parameter file generated on %s                                              #\n', datestr(date, 21));
            fprintf(fid, '#-----------------------------------------------------------------------------------------------#\n');
            fprintf(fid, '\n');
            if phmode < 0 || phmode > 5
                fprintf(fid, 'mode = %d			        # program mode (0 = phantom, 1 = heart lesion, 2 = spherical lesion, 3 = plaque, 4 = vectors, 5 = save anatomical variation) SEE NOTE 0\n', str2num(obj.getParameterValue('mode')));
            else
                fprintf(fid, 'mode = %d			        # program mode (0 = phantom, 1 = heart lesion, 2 = spherical lesion, 3 = plaque, 4 = vectors, 5 = save anatomical variation) SEE NOTE 0\n', phmode);
            end
            fprintf(fid, '\n');
            fprintf(fid, 'act_phan_each = %d      # activity_phantom_each_frame (1=save phantom to file, 0=don''t save\n'         , str2num(obj.getParameterValue('act_phan_each'  )));
            fprintf(fid, 'atten_phan_each = %d    # attenuation_coeff_phantom_each_frame (1=save phantom to file, 0=don''t save\n', str2num(obj.getParameterValue('atten_phan_each')));
            fprintf(fid, 'act_phan_ave = %d       # activity_phantom_average    (1=save , 0=don''t save) see NOTE 1\n'            , str2num(obj.getParameterValue('act_phan_ave'   )));
            fprintf(fid, 'atten_phan_ave = %d     # attenuation_coeff_phantom_average  (1=save, 0=don''t save) see NOTE 1\n'      , str2num(obj.getParameterValue('act_phan_ave'   )));
            fprintf(fid, '\n');
            if motion < 0 || motion > 2
                fprintf(fid, 'motion_option = %d	    # motion_option  (0=beating heart only, 1=respiratory motion only, 2=both motions) see NOTE 2\n', str2num(obj.getParameterValue('motion_option')));
            else
                fprintf(fid, 'motion_option = %d	    # motion_option  (0=beating heart only, 1=respiratory motion only, 2=both motions) see NOTE 2\n', motion);
            end
            fprintf(fid, '\n');
            fprintf(fid, 'out_period = %.3f 		# output_period (SECS) (if <= 0, then output_period=time_per_frame*output_frames)\n', str2num(obj.getParameterValue('out_period'    )));
            fprintf(fid, 'time_per_frame = %.3f     # time_per_frame (SECS) (**IGNORED unless output_period<=0**)\n'                    , str2num(obj.getParameterValue('time_per_frame')));
            fprintf(fid, 'out_frames = %d 	        # output_frames (# of output time frames )\n'                                       , str2num(obj.getParameterValue('out_frames'    )));
            fprintf(fid, '\n');
            
            obj.writeParameters(fid);
            
            fclose(fid);
        end
        
        function writeParameters(obj, fid)
            fprintf(fid, '#-------------------------------------------- Heart Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'hrt_period = %.3f                     # hrt_period (SECS) (length of beating heart cycle; normal = 1s) see NOTE 3\n'        , str2num(obj.getParameterValue('hrt_period'        )));
            fprintf(fid, 'hrt_start_ph_index = %.3f             # hrt_start_phase_index (range=0 to 1; ED=0, ES=0.4) see NOTE 3 \n'                   , str2num(obj.getParameterValue('hrt_start_ph_index')));
            fprintf(fid, 'heart_base = %s		                # basename for heart files (male = vmale50_heart.nrb; female = vfemale50_heart.nrb)\n', obj.getParameterValue('heart_base'        ));
            fprintf(fid, 'heart_curve_file = %s	                # name for file containing time curve for heart\n'                                    , obj.getParameterValue('heart_curve_file'  ));
            fprintf(fid,'\n');
            fprintf(fid, 'apical_thin = %.3f                    # apical_thinning (0 to 1.0 scale, 0.0 = not present, 0.5 = halfway present, 1.0 = completely thin)\n'       , str2num(obj.getParameterValue('apical_thin'  )));
            fprintf(fid, 'uniform_heart = %d                    # sets the thickness of the LV (0 = default, nonuniform wall thickness; 1 = uniform wall thickness for LV)\n', str2num(obj.getParameterValue('uniform_heart')));
            fprintf(fid, '\n');
            fprintf(fid, 'hrt_scale = %.3f                      # hrt_scale  (scales heart in 3D)\n', str2num(obj.getParameterValue('hrt_scale')));
            fprintf(fid, '\n');
            fprintf(fid, 'hrt_v1 = %.3f                         # sets the LV end-diastolic volume (0 = do not change); see NOTE 3A\n'                              , str2num(obj.getParameterValue('hrt_v1')));
            fprintf(fid, 'hrt_v2 = %.3f                         # sets the LV end-systolic volume (0 = do not change); see NOTE 3A\n'                               , str2num(obj.getParameterValue('hrt_v2')));
            fprintf(fid, 'hrt_v3 = %.3f                         # sets the LV volume at the beginning of the quiet phase (0 = do not change); see NOTE 3A\n'        , str2num(obj.getParameterValue('hrt_v3')));
            fprintf(fid, 'hrt_v4 = %.3f                         # sets the LV volume at the end of the quiet phase (0 = do not change); see NOTE 3A\n'              , str2num(obj.getParameterValue('hrt_v4')));
            fprintf(fid, 'hrt_v5 = %.3f                         # sets the LV volume during reduced filling, before end-diastole (0 = do not change); see NOTE 3A\n', str2num(obj.getParameterValue('hrt_v5')));
            fprintf(fid, '\n');
            fprintf(fid, 'hrt_t1 = %.3f                         # sets the duration from end-diastole to end-systole, hrt_v1 to hrt_v2 (default = 0.5s); see NOTE 3A\n'              , str2num(obj.getParameterValue('hrt_t1')));
            fprintf(fid, 'hrt_t2 = %.3f                         # sets the duration from end-systole to beginning of quiet phase, hrt_v2 to hrt_v3 (default = 0.192s); see NOTE 3A\n', str2num(obj.getParameterValue('hrt_t2')));
            fprintf(fid, 'hrt_t3 = %.3f                         # sets the duration of quiet phase, hrt_v3 to hrt_v4 (default = 0.115s); see NOTE 3A\n'                              , str2num(obj.getParameterValue('hrt_t3')));
            fprintf(fid, 'hrt_t4 = %.3f                         # sets the duration from end of quiet phase to reduced filling, hrt_v4 to hrt_v5 (default = 0.193s); see NOTE 3A\n'  , str2num(obj.getParameterValue('hrt_t4')));
            fprintf(fid, '\n');
            fprintf(fid, 'hrt_motion_x = %.3f                   # hrt_motion_x (extent in cm''s of the heart''s lateral motion during breathing; default = 0.0 cm)\n', str2num(obj.getParameterValue('hrt_motion_x')));
            fprintf(fid, 'hrt_motion_y = %.3f                   # hrt_motion_y (extent in cm''s of the heart''s AP motion during breathing; default = 1.2 cm)\n'     , str2num(obj.getParameterValue('hrt_motion_y')));
            fprintf(fid, 'hrt_motion_z = %.3f                   # hrt_motion_z (extent in cm''s of the heart''s up/down motion during breathing; default = 2.0 cm)\n', str2num(obj.getParameterValue('hrt_motion_z')));
            fprintf(fid, '\n');
            fprintf(fid, 'hrt_motion_rot_xz = %.3f              # hrt_motion_rot_xz (extent in degrees of the heart''s xz rotation during breathing; default = 0.0 ) SEE NOTE 4 and NOTE 8 \n', str2num(obj.getParameterValue('hrt_motion_rot_xz')));
            fprintf(fid, 'hrt_motion_rot_yx = %.3f              # hrt_motion_rot_yx (extent in degrees of the heart''s yx rotation during breathing; default = 0.0 ) SEE NOTE 4 and NOTE 8\n' , str2num(obj.getParameterValue('hrt_motion_rot_yx')));
            fprintf(fid, 'hrt_motion_rot_zy = %.3f              # hrt_motion_rot_zy (extent in degrees of the heart''s zy rotation during breathing; default = 0.0 ) SEE NOTE 4 and NOTE 8\n' , str2num(obj.getParameterValue('hrt_motion_rot_zy')));
            fprintf(fid, '\n');
            fprintf(fid, 'd_ZY_rotation = %.3f	                    # change in zy_rotation (beta) in deg. (0); see NOTE 8\n', str2num(obj.getParameterValue('d_ZY_rotation')));
            fprintf(fid, 'd_XZ_rotation = %.3f	                    # change in xz_rotation ( phi) in deg. (0);\n'           , str2num(obj.getParameterValue('d_XZ_rotation')));
            fprintf(fid, 'd_YX_rotation = %.3f	                    # change in yx_rotation ( psi) in deg. (0);\n'           , str2num(obj.getParameterValue('d_YX_rotation')));
            fprintf(fid, 'X_tr = %.3f                            # x translation in mm ;\n'                                  , str2num(obj.getParameterValue('X_tr'         )));
            fprintf(fid, 'Y_tr = %.3f                            # y translation in mm ;\n'                                  , str2num(obj.getParameterValue('Y_tr'         )));
            fprintf(fid, 'Z_tr = %.3f                            # z translation in mm ;\n'                                  , str2num(obj.getParameterValue('Z_tr'         )));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#---------------------Heart lesion parameters------------------------------SEE NOTE 9\n');
            fprintf(fid, 'motion_defect_flag = %d   # (0 = do not include, 1 = include) regional motion abnormality in the LV as defined by heart lesion parameters see NOTE 9\n', str2num(obj.getParameterValue('motion_defect_flag')));
            fprintf(fid, 'ThetaCenter = %.3f        # theta center in deg. (between 0 and 360) \n'                                                                               , str2num(obj.getParameterValue('ThetaCenter'       )));
            fprintf(fid, 'ThetaWidth = %.3f         # theta width in deg., total width (between 0 and 360 deg.)\n'                                                               , str2num(obj.getParameterValue('ThetaWidth'        )));
            fprintf(fid, 'XCenterIndex = %.3f       # x center (0.0=base, 1.0=apex, other fractions=distances in between)\n'                                                     , str2num(obj.getParameterValue('XCenterIndex'      )));
            fprintf(fid, 'XWidthIndex = %.3f        # x width, total in mm''s\n'                                                                                                 , str2num(obj.getParameterValue('XWidthIndex'       )));
            fprintf(fid, 'Wall_fract = %.3f         # wall_fract, fraction of the outer wall transgressed by the lesion\n'                                                       , str2num(obj.getParameterValue('Wall_fract'        )));
            fprintf(fid, 'motion_scale = %.3f       # scales the motion of the defect region (1 = normal motion, < 1 = reduced motion), altered motion blends with normal \n'    , str2num(obj.getParameterValue('motion_scale'      )));
            fprintf(fid, 'border_zone_long = %d     # longitudinal width (in terms of number of control points) of transition between abnormal and normal motion\n'              , str2num(obj.getParameterValue('border_zone_long'  )));
            fprintf(fid, 'border_zone_radial = %d   # radial width (in terms of number of control points) of transition between abnormal and normal motion\n'                    , str2num(obj.getParameterValue('border_zone_radial')));
            fprintf(fid, '#--------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#---------------------Heart plaque parameters------------------------------SEE NOTE 11\n');
            fprintf(fid, 'p_center_v = %.3f      # plaque center along the length of the artery (between 0 and 1)\n'       , str2num(obj.getParameterValue('p_center_v')));
            fprintf(fid, 'p_center_u = %.3f      # plaque center along the circumference of the artery (between 0 and 1)\n', str2num(obj.getParameterValue('p_center_u')));
            fprintf(fid, 'p_height = %.3f        # plaque thickness in mm.\n'                                              , str2num(obj.getParameterValue('p_height'  )));
            fprintf(fid, 'p_width = %.3f         # plaque width in mm.\n'                                                  , str2num(obj.getParameterValue('p_width'   )));
            fprintf(fid, 'p_length = %.3f        # plaque length in mm.\n'                                                 , str2num(obj.getParameterValue('p_length'  )));
            fprintf(fid, 'p_id = %s              # vessel ID to place the plaque in \n'                                    , obj.getParameterValue('p_id'      ));
            fprintf(fid, '#--------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#---------------------Spherical lesion parameters--------------------------SEE NOTE 10\n');
            fprintf(fid, 'x_location = %d			        # x coordinate (pixels) to place lesion\n'                                                        , str2num(obj.getParameterValue('x_location'           )));
            fprintf(fid, 'y_location = %d 			        # y coordinate (pixels) to place lesion \n'                                                       , str2num(obj.getParameterValue('y_location'           )));
            fprintf(fid, 'z_location = %d                   # z coordinate (pixels) to place lesion \n'                                                       , str2num(obj.getParameterValue('z_location'           )));
            fprintf(fid, 'lesn_diameter = %.3f              # Diameter of lesion (mm)\n'                                                                      , str2num(obj.getParameterValue('lesn_diameter'        )));
            fprintf(fid, 'tumor_motion_flag = %d            # Sets tumor motion (0 = default motion based on lungs, 1 = motion defined by user curve below)\n', str2num(obj.getParameterValue('tumor_motion_flag'    )));
            fprintf(fid, 'tumor_motion_filename = %s		# Name of user defined motion curve for tumor\n'                                                  , obj.getParameterValue('tumor_motion_filename'));
            fprintf(fid, '#--------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Respiration Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'resp_period = %.3f            # resp_period (SECS) (length of respiratory cycle; normal breathing = 5s) see NOTE 3\n'                        , str2num(obj.getParameterValue('resp_period'                 )));
            fprintf(fid, 'resp_start_ph_index = %.3f    # resp_start_phase_index (range=0 to 1, full exhale= 0.0, full inhale=0.4) see NOTE 3\n'                       , str2num(obj.getParameterValue('resp_start_ph_index'         )));
            fprintf(fid, 'max_diaphragm_motion = %.3f   # max_diaphragm_motion  (extent in cm''s of diaphragm motion; normal breathing = 2 cm) see NOTE 4\n'           , str2num(obj.getParameterValue('max_diaphragm_motion'        )));
            fprintf(fid, 'max_AP_exp =  %.3f            # max_AP_expansion  (extent in cm''s of the AP expansion of the chest; normal breathing = 1.2 cm) see NOTE 4\n', str2num(obj.getParameterValue('max_AP_exp'                  )));
            fprintf(fid, 'dia_filename =  %s	        # name of curve defining diaphragm motion during respiration\n'                                                , obj.getParameterValue('dia_filename'                ));
            fprintf(fid, 'ap_filename =   %s 			# name of curve defining chest anterior-posterior motion during respiration\n'                                 , obj.getParameterValue('ap_filename'                 ));
            fprintf(fid, 'rdiaph_liv_scale = %.3f	    # height of right_diaphragm/liver dome (0 = flat, 1 = original height, > 1 raises the diaphragm)  SEE NOTE 5\n', str2num(obj.getParameterValue('rdiaph_liv_scale'            )));
            fprintf(fid, 'ldiaph_scale = %.3f           # height of left diaphragm dome (0 = flat, 1 = original height, > raises the diaphragm)  SEE NOTE 5\n'         , str2num(obj.getParameterValue('ldiaph_scale'                )));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Vessel Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'vessel_flag = %d\t\t\t\t# vessel_flag (1 = include arteries and veins, 0 = do not include)\n'                 , str2num(obj.getParameterValue('vessel_flag'       )));
            fprintf(fid, 'coronary_art_flag = %d\t\t# coronary artery flag (1 = include coronary arteries, 0 = do not include)\n'       , str2num(obj.getParameterValue('coronary_art_flag' )));
            fprintf(fid, 'coronary_vein_flag = %d      # coronary vein flag (1 = include coroanry veins, 0 = do not include)\n'         , str2num(obj.getParameterValue('coronary_vein_flag')));
            fprintf(fid, 'papillary_flag = %d			# papillary_flag (1 = include papillary muscles in heart, 0 = do not include)\n', str2num(obj.getParameterValue('papillary_flag'    )));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Gender Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'gender = %d				# male or female phantom (0 = male, 1 = female), be sure to adjust below accordingly\n', str2num(obj.getParameterValue('gender'    )));
            fprintf(fid, 'organ_file = %s	# name of organ file that defines all organs (male = vmale50.nrb, female - vfemale50.nrb)\n'   , obj.getParameterValue('organ_file'));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Breast Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'breast_type = %d		# breast_type (0=supine, 1=prone) \n'                           , str2num(obj.getParameterValue('breast_type' )));
            fprintf(fid, 'which_breast = %d	# which_breast (0 = none, 1 = both, 2 = right only, 3=left only )\n', str2num(obj.getParameterValue('which_breast')));
            fprintf(fid, '\n');
            fprintf(fid, 'rbreast_long_axis_scale = %.3f	# right breast_long_axis (sets the breasts lateral dimension) SEE NOTE 5\n'          , str2num(obj.getParameterValue('rbreast_long_axis_scale' )));
            fprintf(fid, 'rbreast_short_axis_scale = %.3f	# right breast_short_axis (sets the breasts antero-posterior dimension) SEE NOTE 5\n', str2num(obj.getParameterValue('rbreast_short_axis_scale')));
            fprintf(fid, 'rbreast_height_scale = %.3f		# right breast_height (sets the breasts height)  SEE NOTE 5\n'                       , str2num(obj.getParameterValue('rbreast_height_scale'    )));
            fprintf(fid, '\n');
            fprintf(fid, 'vol_rbreast = %.3f               # sets rbreast volume by scaling in 3D, will over-rule above scalings\n', str2num(obj.getParameterValue('vol_rbreast')));
            fprintf(fid, '\n');
            fprintf(fid, 'rbr_theta = %.3f	# theta angle of the right breast (angle the breast is tilted transversely (sideways) from the center of the chest SEE NOTE 5\n', str2num(obj.getParameterValue('rbr_theta')));
            fprintf(fid, 'rbr_phi = %.3f		# phi angle of the right breast (angle the breast is tilted up (+)  or down (-) SEE NOTE 5\n'                               , str2num(obj.getParameterValue('rbr_phi'  )));
            fprintf(fid, 'r_br_tx = %.3f		# x translation for right breast \n', str2num(obj.getParameterValue('r_br_tx')));
            fprintf(fid, 'r_br_ty = %.3f		# y translation for right breast \n', str2num(obj.getParameterValue('r_br_ty')));
            fprintf(fid, 'r_br_tz = %.3f		# z translation for right breast \n', str2num(obj.getParameterValue('r_br_tz')));
            fprintf(fid, '\n');
            fprintf(fid, 'lbreast_long_axis_scale = %.3f   # left breast_long_axis (sets the breasts lateral dimension) SEE NOTE 5\n'          , str2num(obj.getParameterValue('lbreast_long_axis_scale' )));
            fprintf(fid, 'lbreast_short_axis_scale = %.3f  # left breast_short_axis (sets the breasts antero-posterior dimension) SEE NOTE 5\n', str2num(obj.getParameterValue('lbreast_short_axis_scale')));
            fprintf(fid, 'lbreast_height_scale = %.3f      # left breast_height (sets the breasts height)  SEE NOTE 5\n'                       , str2num(obj.getParameterValue('lbreast_height_scale'    )));
            fprintf(fid, '\n');
            fprintf(fid, 'vol_lbreast = %.3f				# sets lbreast volume by scaling in 3D, will over-rule above scalings\n', str2num(obj.getParameterValue('vol_lbreast')));
            fprintf(fid, '\n');
            fprintf(fid, 'lbr_theta = %.3f    # theta angle of the left breast (angle the breast is tilted transversely (sideways) from the center of the chest SEE NOTE 5\n', str2num(obj.getParameterValue('lbr_theta')));
            fprintf(fid, 'lbr_phi = %.3f       # phi angle of the left breast (angle the breast is tilted up (+)  or down (-) SEE NOTE 5\n'                                  , str2num(obj.getParameterValue('lbr_phi'  )));
            fprintf(fid, 'l_br_tx	= %.3f		# x translation for left breast \n', str2num(obj.getParameterValue('l_br_tx')));
            fprintf(fid, 'l_br_ty = %.3f		# y translation for left breast \n', str2num(obj.getParameterValue('l_br_ty')));
            fprintf(fid, 'l_br_tz	= %.3f		# z translation for left breast \n', str2num(obj.getParameterValue('l_br_tz')));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Phantom Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'phantom_long_axis_scale = %.3f		# phantom_long_axis_scale (scales phantom laterally - scales everything but the heart) SEE NOTE 5\n', str2num(obj.getParameterValue('phantom_long_axis_scale' )));
            fprintf(fid, 'phantom_short_axis_scale = %.3f		# phantom_short_axis_scale (scales phantom AP - scales everything but the heart) SEE NOTE 5 \n'     , str2num(obj.getParameterValue('phantom_short_axis_scale')));
            fprintf(fid, 'phantom_height_scale = %.3f			# phantom_height_scale (scales phantom height - scales everything but the heart) SEE NOTE 5\n'      , str2num(obj.getParameterValue('phantom_height_scale'    )));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Head Parameters ------------------------------------------------------------\n' );
            fprintf(fid, 'head_cir_scale = %.3f			# head_circumference_scale (scales head radially - scales everything in head) SEE NOTE 5\n'      , str2num(obj.getParameterValue('head_cir_scale'         )));
            fprintf(fid, 'head_height_scale = %.3f         # head_height_scale (scales head height - scales everything in head) SEE NOTE 5\n'            , str2num(obj.getParameterValue('head_height_scale'      )));
            fprintf(fid, 'head_skin_cir_scale = %.3f		# head_skin_circumference_scale (scales head radially - scales only outer skin) SEE NOTE 5\n', str2num(obj.getParameterValue('head_skin_cir_scale'    )));
            fprintf(fid, 'head_torso_muscle_scale = %.3f	# head_torso_muscle_scale (compresses/expands the muscles radially)  SEE NOTE 5\n'           , str2num(obj.getParameterValue('head_torso_muscle_scale')));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Torso Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'torso_long_axis_scale = %.3f		# torso_long_axis_scale (sets torso, chest and abdomen, transverse axis - scales everything but the heart) SEE NOTE 5\n', str2num(obj.getParameterValue('torso_long_axis_scale' )));
            fprintf(fid, 'torso_short_axis_scale = %.3f	# torso_short_axis_scale (sets torso, chest and abdomen, AP axis - scales everything but the heart) SEE NOTE 5\n'           , str2num(obj.getParameterValue('torso_short_axis_scale')));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Chest Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'chest_skin_long_axis_scale = %.3f		# chest_skin_long_axis_scale (sets chest transverse axis - scales only body outline) SEE NOTE 5  \n', str2num(obj.getParameterValue('chest_skin_long_axis_scale' )));
            fprintf(fid, 'chest_skin_short_axis_scale = %.3f		# chest_skin_short_axis_scale (sets chest AP axis - scales only body outline) SEE NOTE 5\n'     , str2num(obj.getParameterValue('chest_skin_short_axis_scale')));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Abdomen Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'abdomen_skin_long_axis_scale = %.3f		# abdomen_skin_long_axis_scale (sets abdomen transverse axis - scales only body outline) SEE NOTE 5  \n', str2num(obj.getParameterValue('abdomen_skin_long_axis_scale' )));
            fprintf(fid, 'abdomen_skin_short_axis_scale = %.3f		# abdomen_skin_short_axis_scale (sets abdomen AP axis - scales only body outline) SEE NOTE 5\n'         , str2num(obj.getParameterValue('abdomen_skin_short_axis_scale')));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Pelvis Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'pelvis_skin_long_axis_scale = %.3f		# pelvis_skin_long_axis_scale (sets pelvis transverse axis - scales only body outline) SEE NOTE 5\n', str2num(obj.getParameterValue('pelvis_skin_long_axis_scale' )));
            fprintf(fid, 'pelvis_skin_short_axis_scale = %.3f		# pelvis_skin_short_axis_scale (sets pelvis AP axis - scales only body outline) SEE NOTE 5\n'       , str2num(obj.getParameterValue('pelvis_skin_short_axis_scale')));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Arms Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'arms_flag = %d				    # arms_flag (0 = no arms, 1 = arms at the side)\n'                                           , str2num(obj.getParameterValue('arms_flag'            )));
            fprintf(fid, 'arms_cir_scale = %.3f 			# arms_circumference_scale (scales arms radially - scales everything in arms) SEE NOTE 5\n'  , str2num(obj.getParameterValue('arms_cir_scale'       )));
            fprintf(fid, 'arms_length_scale = %.3f			# arms_length_scale (scales arms length - scales everything in arms) SEE NOTE 5\n'           , str2num(obj.getParameterValue('arms_length_scale'    )));
            fprintf(fid, 'arms_skin_cir_scale = %.3f		# arms_skin_circumference_scale (scales arms radially - scales only outer skin) SEE NOTE 5\n', str2num(obj.getParameterValue('arms_skin_cir_scale'  )));
            fprintf(fid, 'arms_muscle_cir_scale = %.3f		# arms_muscle_cir_scale (compresses/expands the muscles radially)  SEE NOTE 5\n'             , str2num(obj.getParameterValue('arms_muscle_cir_scale')));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Legs Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'legs_cir_scale = %.3f			# legs_circumference_scale (scales legs radially - scales everything in legs) SEE NOTE 5\n'      , str2num(obj.getParameterValue('legs_cir_scale'       )));
            fprintf(fid, 'legs_length_scale = %.3f         # legs_length_scale (scales legs length - scales everything in legs) SEE NOTE 5\n'            , str2num(obj.getParameterValue('legs_length_scale'    )));
            fprintf(fid, 'legs_skin_cir_scale = %.3f		# legs_skin_circumference_scale (scales legs radially - scales only outer skin) SEE NOTE 5\n', str2num(obj.getParameterValue('legs_skin_cir_scale'  )));
            fprintf(fid, 'legs_muscle_cir_scale = %.3f		# legs_muscle_cir_scale (compresses/expands the muscles radially)  SEE NOTE 5\n'             , str2num(obj.getParameterValue('legs_muscle_cir_scale')));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Bones Parameters ------------------------------------------------------------\n');
            fprintf(fid, 'bones_scale = %.3f				# bones_scale (scales all bones in 2D about their centerlines, makes each bone thicker)  SEE NOTE 5\n', str2num(obj.getParameterValue('bones_scale')));
            fprintf(fid, 'marrow_flag = %d				# render marrow (0 = no, 1 = yes)\n'                                                                      , str2num(obj.getParameterValue('marrow_flag')));
            fprintf(fid, 'frac_H2O = %.3f			# fraction (by weight) of water in wet bone and wet spine (used to calc. atten coeff)\n'                      , str2num(obj.getParameterValue('frac_H2O'   )));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Intestinal Air -------------------------------------------------------------------\n');
            fprintf(fid, 'si_air_flag = %d				# 0 = do not include air and 1 = include air in small intestine\n', str2num(obj.getParameterValue('si_air_flag')));
            fprintf(fid, 'li_air_flag = %d				# location of air in the large intestine see NOTE 6\n'            , str2num(obj.getParameterValue('li_air_flag')));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Organ Thickness ------------------------------------------------------------\n');
            fprintf(fid, 'thickness_sternum = %.3f   # thickness sternum  (cm)\n'                  , str2num(obj.getParameterValue('thickness_sternum' )));
            fprintf(fid, 'thickness_scapula = %.3f	# thickness scapulas (cm)\n'                   , str2num(obj.getParameterValue('thickness_scapula' )));
            fprintf(fid, 'thickness_ribs = %.3f		# thickness ribs     (cm)\n'                   , str2num(obj.getParameterValue('thickness_ribs'    )));
            fprintf(fid, 'thickness_backbone = %.3f	# thickness backbone (cm)\n'                   , str2num(obj.getParameterValue('thickness_backbone')));
            fprintf(fid, 'thickness_pelvis = %.3f	# thickness pelvis (cm)\n'                     , str2num(obj.getParameterValue('thickness_pelvis'  )));
            fprintf(fid, 'thickness_collar = %.3f	# thickness collarbones (cm)\n'                , str2num(obj.getParameterValue('thickness_collar'  )));
            fprintf(fid, 'thickness_humerus = %.3f	# thickness humerus (cm)\n'                    , str2num(obj.getParameterValue('thickness_humerus' )));
            fprintf(fid, 'thickness_radius = %.3f	# thickness radius (cm)\n'                     , str2num(obj.getParameterValue('thickness_radius'  )));
            fprintf(fid, 'thickness_ulna = %.3f		# thickness ulna (cm)\n'                       , str2num(obj.getParameterValue('thickness_ulna'    )));
            fprintf(fid, 'thickness_hand = %.3f		# thickness hand bones (cm)\n'                 , str2num(obj.getParameterValue('thickness_hand'    )));
            fprintf(fid, 'thickness_femur = %.3f	    # thickness femur (cm)\n'                  , str2num(obj.getParameterValue('thickness_femur'   )));
            fprintf(fid, 'thickness_tibia = %.3f	    # thickness tibia (cm)\n'                  , str2num(obj.getParameterValue('thickness_tibia'   )));
            fprintf(fid, 'thickness_fibula = %.3f	# thickness fibula (cm)\n'                     , str2num(obj.getParameterValue('thickness_fibula'  )));
            fprintf(fid, 'thickness_patella = %.3f	# thickness patella (cm)\n'                    , str2num(obj.getParameterValue('thickness_patella' )));
            fprintf(fid, 'thickness_foot = %.3f		# thickness foot bones (cm)\n'                 , str2num(obj.getParameterValue('thickness_foot'    )));
            fprintf(fid, 'thickness_si = %.3f		# thickness of small intestine wall (cm)\n'    , str2num(obj.getParameterValue('thickness_si'      )));
            fprintf(fid, 'thickness_li = %.3f		# thickness of large intestine wall (cm)\n'    , str2num(obj.getParameterValue('thickness_li'      )));
            fprintf(fid, 'thickness_esoph = %.3f	    # thickness of the esophagus wall (cm)\n'  , str2num(obj.getParameterValue('thickness_esoph'   )));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Organ Volume ------------------------------------------------------------\n');
            fprintf(fid, 'vol_prostate = %.3f           # set the volume of the prostate; (0 = do not change)\n'         , str2num(obj.getParameterValue('vol_prostate'    )));
            fprintf(fid, 'vol_testes = %.3f             # set the volume of the testes; (0 = do not change)\n'           , str2num(obj.getParameterValue('vol_testes'      )));
            fprintf(fid, 'vol_liver = %.3f				# set the volume of the liver; (0 = do not change)\n'            , str2num(obj.getParameterValue('vol_liver'       )));
            fprintf(fid, 'vol_stomach = %.3f            # set the volume of the stomach; (0 = do not change)\n'          , str2num(obj.getParameterValue('vol_stomach'     )));
            fprintf(fid, 'vol_pancreas = %.3f			# set the volume of the pancreas; (0 = do not change)\n'         , str2num(obj.getParameterValue('vol_pancreas'    )));
            fprintf(fid, 'vol_spleen = %.3f             # set the volume of the spleen; (0 = do not change)\n'           , str2num(obj.getParameterValue('vol_spleen'      )));
            fprintf(fid, 'vol_gall_bladder = %.3f		# set the volume of the gall_bladder; (0 = do not change)\n'     , str2num(obj.getParameterValue('vol_gall_bladder')));
            fprintf(fid, 'vol_rkidney = %.3f            # set the volume of the right kidney; (0 = do not change)\n'     , str2num(obj.getParameterValue('vol_rkidney'     )));
            fprintf(fid, 'vol_lkidney = %.3f            # set the volume of the left kidney; (0 = do not change)\n'      , str2num(obj.getParameterValue('vol_lkidney'     )));
            fprintf(fid, 'vol_radrenal = %.3f			# set the volume of the right adrenal; (0 = do not change)\n'    , str2num(obj.getParameterValue('vol_radrenal'    )));
            fprintf(fid, 'vol_ladrenal = %.3f			# set the volume of the left adrenal; (0 = do not change)\n'     , str2num(obj.getParameterValue('vol_ladrenal'    )));
            fprintf(fid, 'vol_small_intest = %.3f		# set the volume of the small intestine; (0 = do not change)\n'  , str2num(obj.getParameterValue('vol_small_intest')));
            fprintf(fid, 'vol_large_intest = %.3f		# set the volume of the large intestine; (0 = do not change)\n'  , str2num(obj.getParameterValue('vol_large_intest')));
            fprintf(fid, 'vol_bladder = %.3f            # set the volume of the bladder; (0 = do not change)\n'          , str2num(obj.getParameterValue('vol_bladder'     )));
            fprintf(fid, 'vol_thyroid = %.3f            # set the volume of the thyroid; (0 = do not change)\n'          , str2num(obj.getParameterValue('vol_thyroid'     )));
            fprintf(fid, 'vol_thymus = %.3f             # set the volume of the thymus; (0 = do not change)\n'           , str2num(obj.getParameterValue('vol_thymus'      )));
            fprintf(fid, 'vol_salivary = %.3f			# set the volume of the salivary glands; (0 = do not change)\n'  , str2num(obj.getParameterValue('vol_salivary'    )));
            fprintf(fid, 'vol_pituitary = %.3f			# set the volume of the pituitary gland; (0 = do not change)\n'  , str2num(obj.getParameterValue('vol_pituitary'   )));
            fprintf(fid, 'vol_eyes = %.3f				# set the volume of the eyes; (0 = do not change)\n'             , str2num(obj.getParameterValue('vol_eyes'        )));
            fprintf(fid, 'vol_rovary = %.3f             # set the volume of the right ovary; (0 = do not change)\n'      , str2num(obj.getParameterValue('vol_rovary'      )));
            fprintf(fid, 'vol_lovary = %.3f             # set the volume of the left ovary; (0 = do not change)\n'       , str2num(obj.getParameterValue('vol_lovary'      )));
            fprintf(fid, 'vol_ftubes = %.3f             # set the volume of the fallopian tubes; (0 = do not change)\n'  , str2num(obj.getParameterValue('vol_ftubes'      )));
            fprintf(fid, 'vol_uterus = %.3f             # set the volume of the uterus; (0 = do not change)\n'           , str2num(obj.getParameterValue('vol_uterus'      )));
            fprintf(fid, 'vol_vagina = %.3f             # set the volume of the vagina; (0 = do not change)\n'           , str2num(obj.getParameterValue('vol_vagina'      )));
            fprintf(fid, 'vol_larynx = %.3f             # set the volume of the larynx/pharynx; (0 = do not change)\n'   , str2num(obj.getParameterValue('vol_larynx'      )));
            fprintf(fid, 'vol_trachea = %.3f			# set the volume of the trachea (total); (0 = do not change)\n'  , str2num(obj.getParameterValue('vol_trachea'     )));
            fprintf(fid, 'vol_esoph = %.3f				# set the volume of the esophagus (total); (0 = do not change)\n', str2num(obj.getParameterValue('vol_esoph'       )));
            fprintf(fid, 'vol_epidy = %.3f				# set the volume of the epididymus; (0 = do not change)\n'       , str2num(obj.getParameterValue('vol_epidy'       )));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Activities ------------------------------------------------------------\n');
            fprintf(fid, 'activity_unit = 0	# activity units (1= scale by voxel volume; 0= don''t scale) NOTE 11\n');
            fprintf(fid, '\n');
            fprintf(fid, 'myoLV_act = %.3f		# hrt_myoLV_act - activity in left ventricle myocardium\n'                 , str2num(obj.getParameterValue('myoLV_act'                )));
            fprintf(fid, 'myoRV_act = %.3f		# hrt_myoRV_act - activity in right ventricle myocardium\n'                , str2num(obj.getParameterValue('myoRV_act'                )));
            fprintf(fid, 'myoLA_act = %.3f		# hrt_myoLA_act - activity in left atrium myocardium\n'                    , str2num(obj.getParameterValue('myoLA_act'                )));
            fprintf(fid, 'myoRA_act = %.3f		# hrt_myoRA_act - activity in right atrium myocardium\n'                   , str2num(obj.getParameterValue('myoRA_act'                )));
            fprintf(fid, 'bldplLV_act = %.3f		# hrt_bldplLV_act - activity in left ventricle chamber (blood pool)\n' , str2num(obj.getParameterValue('bldplLV_act'              )));
            fprintf(fid, 'bldplRV_act = %.3f		# hrt_bldplRV_act - activity in right ventricle chamber (blood pool)\n', str2num(obj.getParameterValue('bldplRV_act'              )));
            fprintf(fid, 'bldplLA_act = %.3f		# hrt_bldplLA_act - activity in left atria chamber (blood pool)\n'     , str2num(obj.getParameterValue('bldplLA_act'              )));
            fprintf(fid, 'bldplRA_act = %.3f		# hrt_bldplRA_act - activity in right atria chamber (blood pool)\n'    , str2num(obj.getParameterValue('bldplRA_act'              )));
            fprintf(fid, 'body_activity = %.3f		# body_activity (background activity) ;\n'                             , str2num(obj.getParameterValue('body_activity'            )));
            fprintf(fid, 'muscle_activity = %.3f		# muscle activity;\n'                                              , str2num(obj.getParameterValue('muscle_activity'          )));
            fprintf(fid, 'brain_activity = %.3f		# brain activity;\n'                                                   , str2num(obj.getParameterValue('brain_activity'           )));
            fprintf(fid, 'sinus_activity = %.3f		# sinus activity;\n'                                                   , str2num(obj.getParameterValue('sinus_activity'           )));
            fprintf(fid, 'liver_activity = %.3f		# liver_activity;\n'                                                   , str2num(obj.getParameterValue('liver_activity'           )));
            fprintf(fid, 'gall_bladder_activity = %.3f	# gall_bladder_activity;\n'                                        , str2num(obj.getParameterValue('gall_bladder_activity'    )));
            fprintf(fid, 'r_lung_activity = %.3f			# right_lung_activity;\n'                                      , str2num(obj.getParameterValue('r_lung_activity'          )));
            fprintf(fid, 'l_lung_activity = %.3f         # left_lung_activity;\n'                                          , str2num(obj.getParameterValue('l_lung_activity'          )));
            fprintf(fid, 'esophagus_activity = %.3f		# esophagus_activity;\n'                                           , str2num(obj.getParameterValue('esophagus_activity'       )));
            fprintf(fid, 'laryngopharynx_activity = %.3f	# laryngopharynx_activity\n'                                   , str2num(obj.getParameterValue('laryngopharynx_activity'  )));
            fprintf(fid, 'st_wall_activity = %.3f		# st_wall_activity;  (stomach wall)\n'                             , str2num(obj.getParameterValue('st_wall_activity'         )));
            fprintf(fid, 'st_cnts_activity = %.3f		# st_cnts_activity;   (stomach contents)\n'                        , str2num(obj.getParameterValue('st_cnts_activity'         )));
            fprintf(fid, 'pancreas_activity = %.3f		# pancreas_activity;\n'                                            , str2num(obj.getParameterValue('pancreas_activity'        )));
            fprintf(fid, 'r_kidney_cortex_activity = %.3f	# right_kidney_cortex_activity;\n'                             , str2num(obj.getParameterValue('r_kidney_cortex_activity' )));
            fprintf(fid, 'r_kidney_medulla_activity = %.3f	# right_kidney_medulla_activity;\n'                            , str2num(obj.getParameterValue('r_kidney_medulla_activity')));
            fprintf(fid, 'l_kidney_cortex_activity = %.3f	# left_kidney_cortex_activity;\n'                              , str2num(obj.getParameterValue('l_kidney_cortex_activity' )));
            fprintf(fid, 'l_kidney_medulla_activity = %.3f	# left_kidney_medulla_activity;\n'                             , str2num(obj.getParameterValue('l_kidney_medulla_activity')));
            fprintf(fid, 'adrenal_activity = %.3f		# adrenal_activity;\n'                                             , str2num(obj.getParameterValue('adrenal_activity'         )));
            fprintf(fid, 'r_renal_pelvis_activity = %.3f	# right_renal_pelvis_activity;\n'                              , str2num(obj.getParameterValue('r_renal_pelvis_activity'  )));
            fprintf(fid, 'l_renal_pelvis_activity = %.3f # left_renal_pelvis_activity;\n'                                  , str2num(obj.getParameterValue('l_renal_pelvis_activity'  )));
            fprintf(fid, 'spleen_activity = %.3f		# spleen_activity;\n'                                              , str2num(obj.getParameterValue('spleen_activity'          )));
            fprintf(fid, 'rib_activity = %.3f			# rib_activity;\n'                                                 , str2num(obj.getParameterValue('rib_activity'             )));
            fprintf(fid, 'cortical_bone_activity = %.3f	# cortical_bone_activity;\n'                                       , str2num(obj.getParameterValue('cortical_bone_activity'   )));
            fprintf(fid, 'spine_activity = %.3f			# spine_activity;\n'                                               , str2num(obj.getParameterValue('spine_activity'           )));
            fprintf(fid, 'spinal_cord_activity = %.3f	# spinal_cord_activity;\n'                                         , str2num(obj.getParameterValue('spinal_cord_activity'     )));
            fprintf(fid, 'bone_marrow_activity = %.3f	# bone_marrow_activity;\n'                                         , str2num(obj.getParameterValue('bone_marrow_activity'     )));
            fprintf(fid, 'art_activity = %.3f		# artery_activity;\n'                                                  , str2num(obj.getParameterValue('art_activity'             )));
            fprintf(fid, 'vein_activity = %.3f		# vein_activity;\n'                                                    , str2num(obj.getParameterValue('vein_activity'            )));
            fprintf(fid, 'bladder_activity = %.3f		# bladder_activity;\n'                                             , str2num(obj.getParameterValue('bladder_activity'         )));
            fprintf(fid, 'prostate_activity = %.3f		# prostate_activity;\n'                                            , str2num(obj.getParameterValue('prostate_activity'        )));
            fprintf(fid, 'asc_li_activity = %.3f		# ascending_large_intest_activity;\n'                              , str2num(obj.getParameterValue('asc_li_activity'          )));
            fprintf(fid, 'trans_li_activity = %.3f		# transcending_large_intest_activity;\n'                           , str2num(obj.getParameterValue('trans_li_activity'        )));
            fprintf(fid, 'desc_li_activity = %.3f		# desc_large_intest_activity;\n'                                   , str2num(obj.getParameterValue('desc_li_activity'         )));
            fprintf(fid, 'sm_intest_activity = %.3f		# small_intest_activity;\n'                                        , str2num(obj.getParameterValue('sm_intest_activity'       )));
            fprintf(fid, 'rectum_activity = %.3f		# rectum_activity;\n'                                              , str2num(obj.getParameterValue('rectum_activity'          )));
            fprintf(fid, 'sem_activity = %.3f		# sem_vess_activity;\n'                                                , str2num(obj.getParameterValue('sem_activity'             )));
            fprintf(fid, 'vas_def_activity = %.3f		# vas_def_activity;\n'                                             , str2num(obj.getParameterValue('vas_def_activity'         )));
            fprintf(fid, 'test_activity = %.3f		# testicular_activity;\n'                                              , str2num(obj.getParameterValue('test_activity'            )));
            fprintf(fid, 'epididymus_activity = %.3f		# epididymus_activity;\n'                                      , str2num(obj.getParameterValue('epididymus_activity'      )));
            fprintf(fid, 'ejac_duct_activity = %.3f		# ejaculatory_duct_activity;\n'                                    , str2num(obj.getParameterValue('ejac_duct_activity'       )));
            fprintf(fid, 'pericardium_activity = %.3f     	# pericardium activity;\n'                                     , str2num(obj.getParameterValue('pericardium_activity'     )));
            fprintf(fid, 'cartilage_activity = %.3f		# cartilage activity;\n'                                           , str2num(obj.getParameterValue('cartilage_activity'       )));
            fprintf(fid, 'intest_air_activity = %.3f	# activity of intestine contents (air); \n'                        , str2num(obj.getParameterValue('intest_air_activity'      )));
            fprintf(fid, 'ureter_activity = %.3f		# ureter activity;\n'                                              , str2num(obj.getParameterValue('ureter_activity'          )));
            fprintf(fid, 'urethra_activity = %.3f		# urethra activity;\n'                                             , str2num(obj.getParameterValue('urethra_activity'         )));
            fprintf(fid, 'lymph_activity = %.3f		# lymph normal activity;\n'                                            , str2num(obj.getParameterValue('lymph_activity'           )));
            fprintf(fid, 'lymph_abnormal_activity = %.3f	# lymph abnormal activity;\n'                                  , str2num(obj.getParameterValue('lymph_abnormal_activity'  )));
            fprintf(fid, 'airway_activity = %.3f	# airway tree activity\n'                                              , str2num(obj.getParameterValue('airway_activity'          )));
            fprintf(fid, 'uterus_activity = %.3f		# uterus_activity;\n'                                              , str2num(obj.getParameterValue('uterus_activity'          )));
            fprintf(fid, 'vagina_activity = %.3f		# vagina_activity;\n'                                              , str2num(obj.getParameterValue('vagina_activity'          )));
            fprintf(fid, 'right_ovary_activity = %.3f	# right_ovary_activity;\n'                                         , str2num(obj.getParameterValue('right_ovary_activity'     )));
            fprintf(fid, 'left_ovary_activity = %.3f	# left_ovary_activity;\n'                                          , str2num(obj.getParameterValue('left_ovary_activity'      )));
            fprintf(fid, 'fallopian_tubes_activity = %.3f	# fallopian tubes_activity;\n'                                 , str2num(obj.getParameterValue('fallopian_tubes_activity' )));
            fprintf(fid, 'parathyroid_activity = %.3f	# parathyroid_activity;\n'                                         , str2num(obj.getParameterValue('parathyroid_activity'     )));
            fprintf(fid, 'thyroid_activity = %.3f		# thyroid_activity;\n'                                             , str2num(obj.getParameterValue('thyroid_activity'         )));
            fprintf(fid, 'thymus_activity = %.3f		# thymus_activity;\n'                                              , str2num(obj.getParameterValue('thymus_activity'          )));
            fprintf(fid, 'salivary_activity = %.3f		# salivary_activity;\n'                                            , str2num(obj.getParameterValue('salivary_activity'        )));
            fprintf(fid, 'pituitary_activity = %.3f		# pituitary_activity;\n'                                           , str2num(obj.getParameterValue('pituitary_activity'       )));
            fprintf(fid, 'eye_activity = %.3f		# eye_activity;\n'                                                     , str2num(obj.getParameterValue('eye_activity'             )));
            fprintf(fid, 'lens_activity = %.3f		# eye_lens_activity;\n'                                                , str2num(obj.getParameterValue('lens_activity'            )));
            fprintf(fid, 'lesn_activity = %.3f		# activity for heart lesion, plaque, or spherical lesion\n'            , str2num(obj.getParameterValue('lesn_activity'            )));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#-------------------------------------------- Energy ------------------------------------------------------------\n');
            fprintf(fid, 'energy  = %.1f 	# radionuclide energy in keV (range 1 - 40MeV, increments of 0.5 keV) ; for attn. map only\n', str2num(obj.getParameterValue('energy')));
            fprintf(fid, '#---------------------------------------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#---------------------Vector parameters------------------------------------SEE NOTE 12\n');
            fprintf(fid, 'vec_factor = %d		# higher number will increase the precision of the vector output\n', str2num(obj.getParameterValue('vec_factor')));
            fprintf(fid, '#-------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#---------------------Image parameters------------------------------------SEE NOTE 12\n');
            fprintf(fid, 'pixel_width =  %.3f   # pixel width (cm);  see NOTE 7\n'                                        , str2num(obj.getParameterValue('pixel_width'   )));
            fprintf(fid, 'slice_width = %.3f    # slice width (cm);\n'                                                    , str2num(obj.getParameterValue('slice_width'   )));
            fprintf(fid, 'array_size =  %d      # array size   \n'                                                        , str2num(obj.getParameterValue('array_size'    )));
            fprintf(fid, 'subvoxel_index = %d	# subvoxel_index (=1,2,3,4 -> 1,8,27,64 subvoxels/voxel, respectively)\n' , str2num(obj.getParameterValue('subvoxel_index')));
            fprintf(fid, 'startslice = %d       # start_slice; \n'                                                        , str2num(obj.getParameterValue('startslice'    )));
            fprintf(fid, 'endslice = %d         # end_slice;  \n'                                                         , str2num(obj.getParameterValue('endslice'      )));
            fprintf(fid, '#-------------------------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#----------------------Suppress Warnings-----------------------------------\n');
            fprintf(fid, 'nurbs_save      = 0\n');
            fprintf(fid, 'color_code      = 0\n');
            fprintf(fid, 'ct_output       = 0\n');
            fprintf(fid, 'body_flag       = 0\n');
            fprintf(fid, 'organ_flag      = 0\n');
            fprintf(fid, 'muscle_flag     = 0\n');
            fprintf(fid, 'skeleton_flag   = 0\n');
            fprintf(fid, 'bronch_flag     = 0\n');
            fprintf(fid, 'bvess_flag      = 0\n');
            fprintf(fid, 'heart_flag      = 0\n');
            fprintf(fid, 'lung_scale      = 1.0\n');
            fprintf(fid, 'lv_radius_scale = 1.0\n');
            fprintf(fid, 'lv_length_scale = 1.0\n');
            fprintf(fid, 'use_res         = 0\n');
            fprintf(fid, 'vol_rthyroid    = 0.0\n');
            fprintf(fid, 'vol_lthyroid    = 0.0\n');
            fprintf(fid, '#-------------------------------------------------------------------------\n');
            fprintf(fid, '\n');
            fprintf(fid, '#--------------------------------------------------------------------------\n');
            fprintf(fid, '#--------------------------------------------------------------------------\n');
            fprintf(fid, '#This is a general parameter file for the DYNAMIC XCAT phantom, version 2.0\n');
            fprintf(fid, '#--------------------------------------------------------------------------\n');
            fprintf(fid, '#THE PARAMETERS CAN BE IN ANY ORDER. THE PROGRAM WILL SORT THEM.\n');
            fprintf(fid, '#--------------------------------------------------------------------------\n');
            fprintf(fid, '#                             NOTES:\n');
            fprintf(fid, '#--------------------------------------------------------------------------\n');
            fprintf(fid, '#NOTE 0: The phantom program can be run in different modes as follows.  \n');
            fprintf(fid, '#  Mode 0: standard phantom generation mode that will generate phantoms of the\n');
            fprintf(fid, '#          body.\n');
            fprintf(fid, '#  Mode 1: heart lesion generator that will create phantoms of only the user\n');
            fprintf(fid, '#          defined heart lesion. Subtract these phantoms from those of mode 0\n');
            fprintf(fid, '#          to place the defect in the body.\n');
            fprintf(fid, '#  Mode 2: spherical lesion generator that will create phantoms of only the\n');
            fprintf(fid, '#          user defined lesion. Add these phantoms to those of mode 0 to place\n');
            fprintf(fid, '#          the lesions in the body.\n');
            fprintf(fid, '#  Mode 3: cardiac plaque generator that will create phantoms of only the\n');
            fprintf(fid, '#          user defined plaque. Add these phantoms to those of mode 0 to place\n');
            fprintf(fid, '#          the plaques in the body.\n');
            fprintf(fid, '#  Mode 4: vector generator that will output motion vectors as determined from \n');
            fprintf(fid, '#          the phantom surfaces. The vectors will be output as text files.\n');
            fprintf(fid, '#  Mode 5: anatomy generator will save the phantom produced from the user-defined anatomy \n');
            fprintf(fid, '#          parameters. The phantom is saved as two files, the organ file and the heart_base \n');
            fprintf(fid, '#          file. The names of these files can then be specified in the parfile for later runs\n');
            fprintf(fid, '#          with the program not having to take the time to generate the anatomy again. In using \n');
            fprintf(fid, '#	   a saved anatomy, be sure to set all scalings back to 1; otherwise, the anatomy will be \n');
            fprintf(fid, '#          scaled again.       \n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE 1: The average phantom is the average ONLY OF THOSE FRAMES GENERATED. That is,\n');
            fprintf(fid, '#  if you specify that only 2 frames be generated, then the average phantom is\n');
            fprintf(fid, '#  just the average of those 2 frames.\n');
            fprintf(fid, '#  ***************************************************************************\n');
            fprintf(fid, '#  ** FOR A GOOD AVERAGE, generate at least 8-16 frames per 1 complete heart\n');
            fprintf(fid, '#  ** cycle and/or per 1 complete respiratory cycle.\n');
            fprintf(fid, '#  ***************************************************************************\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE 2: Heart motion refers to heart BEATING or contraction, while resp.\n');
            fprintf(fid, '#  motion refers to organ motion due to breathing. Note that the entire heart is\n');
            fprintf(fid, '#  translated or rotated due to resp. motion, even if it is not contracting.\n');
            fprintf(fid, '#  ** IF motion_option=1 , THE HEART WILL MOVE (TRANSLATE) BUT NOT BEAT.****\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE 3:   Users sets the length and starting phase of both the heart\n');
            fprintf(fid, '#          and respiratory cycles. NORMAL values for length of heart beat and\n');
            fprintf(fid, '#          respiratory are cycles are 1 sec. and 5 secs., respectively,\n');
            fprintf(fid, '#          BUT THESE CAN VARY AMONG PATIENTS and will increase if the patient\n');
            fprintf(fid, '#          is under stress.\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#          An index value between 0 and 1 is used the specify the starting phase\n');
            fprintf(fid, '#          of the heart or resp cycles. IF NO MOTION IS SPECIFIED THEN THE STARTING\n');
            fprintf(fid, '#          PHASE IS USED AS THE SINGLE PHASE AT WHICH THE PHANTOM IS GENERATED.\n');
            fprintf(fid, '#          (see documentation for more details).\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE 3A:  These parameters control the LV volume curve of the heart. The user can specify the LV\n');
            fprintf(fid, '#	   volume at 5 points in the cardiac cycle. Check the logfile to see what the default volumes \n');
            fprintf(fid, '#          are.  The end-diastolic volume can only be reduced. The way to increase it would be to change\n');
            fprintf(fid, '#          the overall heart scale.  The end-systolic volume can be increased or reduced. The other volumes\n');
            fprintf(fid, '#          need to have values between the end-diastolic and end-systolic volumes.  The time durations for the\n');
            fprintf(fid, '#          different portions of the cardiac cycle must add up to a total of 1.\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#          Changing these parameters will alter the heart_curve.  The altered curve and heart files can be output using\n');
            fprintf(fid, '#          mode = 5.\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE 4:  These NORMAL values are for normal tidal breathing.\n');
            fprintf(fid, '#  ** Modeling a deep inhale may require higher values. **\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#  The AP_expansion parameter controls the anteroposterior diameter of the ribcage, body,\n');
            fprintf(fid, '#  and lungs. The ribs rotate upward to expand the chest cavity by the amount indicated by the \n');
            fprintf(fid, '#  AP_expansion parameter. The lungs and body move with the expanding ribs. There is maximum amount\n');
            fprintf(fid, '#  by which the AP diameter can expand, due to the size of the ribs (some expansions are impossible\n');
            fprintf(fid, '#  geometrically.) If the user specifies too great an expansion, the program will terminate with an\n');
            fprintf(fid, '#  error message. \n');
            fprintf(fid, '#\n');
            fprintf(fid, '#  The diaphragm motion controls the motion of the liver, the left diaphragm, stomach, spleen and\n');
            fprintf(fid, '#  all organs downstream from them. \n');
            fprintf(fid, '#\n');
            fprintf(fid, '#  The heart has its own parameters to control its motion. It can translate left or right (+/- values of hrt_motion_x respectively), \n');
            fprintf(fid, '#  to the anterior/posterior (+/- values of hrt_motion_y respectively), or up/down (+/- values of hrt_motion_z respectively) \n');
            fprintf(fid, '#  with the diaphragm motion. The heart can also rotate. The x-axis runs from the right side of the body to the left.  \n');
            fprintf(fid, '#  Changing the x-rot will tilt the heart up(+ values)/down (- values).  The y-axis runs from the front of the body to the back.  \n');
            fprintf(fid, '#  Changing the y-rot will tilt the heart from side to side.  The z-axis runs from the feet to the head.  \n');
            fprintf(fid, '#  The z-rot will spin the heart right or left.\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE 5: The phantom program outputs statistics on these anatomical parameters in the logfile it generates.  The logfile is \n');
            fprintf(fid, '#         named with the extension _log.  These statistics can be used to determine the amount of scaling desired. Be aware \n');
            fprintf(fid, '#	  the phantom scaling parameters scale the entire phantom; therefore, any body, heart or breast scalings  will\n');
            fprintf(fid, '#         be additional to this base scaling.\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE 6:  Location of air in the large intestine and rectum\n');
            fprintf(fid, '#          5 = air visible in the entire large intestine and rectum\n');
            fprintf(fid, '#          4 = air visible in ascending, transverse, descending, and sigmoid portions of the large intestine \n');
            fprintf(fid, '#          3 = air visible in ascending, transverse, and descending portions of the large intestine\n');
            fprintf(fid, '#          2 = air visible in ascending and transverse portions of the large intestine\n');
            fprintf(fid, '#          1 = air visible in ascending portion of the large intestine only\n');
            fprintf(fid, '#          0 = no air visible (entire large intestine and rectum filled with contents)\n');
            fprintf(fid, '#          \n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE 7:\n');
            fprintf(fid, '#        - The phantom dimensions do not necessarily have to be cubic. The array_size parameter \n');
            fprintf(fid, '#          determines the x and y dimensions of the images.  The number of slices in the z dimension \n');
            fprintf(fid, '#          is determined by the start_slice and end_slice parameters.  The total number of slices is\n');
            fprintf(fid, '#          end_slice - start_slice + 1.\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE 8:\n');
            fprintf(fid, '#        - rotation parameters determine\n');
            fprintf(fid, '#          initial orientation of beating (dynamic) heart LV long axis\n');
            fprintf(fid, '#        - d_zy_rotation : +y-axis rotates toward +z-axis (about x-axis) by beta\n');
            fprintf(fid, '#          d_xz_rotation : +z-axis rotates toward +x-axis (about y-axis) by phi\n');
            fprintf(fid, '#          d_yx_rotation : +x-axis rotates toward +y-axis (about z-axis) by psi\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#        - Based on patient data, the mean and SD heart orientations are:\n');
            fprintf(fid, '#                zy_rot = -110 degrees (no patient data for this rotation)\n');
            fprintf(fid, '#                xz_rot = 23 +- 10 deg.\n');
            fprintf(fid, '#                yx_rot = -52 +- 11 deg.\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#	 - Phantom will output total angles for the heart orientation in the logfile\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE 9: Creates lesion (defect) for the LEFT VENTRICLE ONLY.\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#--------------------------------\n');
            fprintf(fid, '#  theta_center: location of lesion center in circumferential dimension\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#  theta center =    0.0  => anterior wall\n');
            fprintf(fid, '#  theta center =  +90.0  => lateral   "\n');
            fprintf(fid, '#  theta center = +180.0  => inferior  "\n');
            fprintf(fid, '#  theta center = +270.0  => septal    "\n');
            fprintf(fid, '#--------------------------------\n');
            fprintf(fid, '#  theta_width : lesion width in circumferential dimension\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#  TOTAL width of defect in degrees. So for example a width of 90 deg.\n');
            fprintf(fid, '#  means that the width is 45 deg. on either side of theta center.\n');
            fprintf(fid, '#--------------------------------\n');
            fprintf(fid, '#  x center :   lesion center in long-axis dimension\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#  x center = 0    -> base of LV\n');
            fprintf(fid, '#  x center = 1.0  -> apex of LV\n');
            fprintf(fid, '#--------------------------------\n');
            fprintf(fid, '#  x width:  lesion width in long-axis dimension\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#  total width. Defect extend half the total width on either side of the\n');
            fprintf(fid, '#  x_center.\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#  NOTE: if the specified width extends beyond the boundaries of the LV\n');
            fprintf(fid, '#        then the defect is cut off and the effective width is less than the\n');
            fprintf(fid, '#        specified width. So for example...\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#--------------------------------\n');
            fprintf(fid, '#  Wall_fract : fraction of the LV wall that the lesion transgresses\n');
            fprintf(fid, '#  Wall_fract = 0.0 => transgresses none of the wall\n');
            fprintf(fid, '#  Wall_fract = 0.5 => transgresses the inner half of the wall\n');
            fprintf(fid, '#  Wall_fract = 1.0 => trangresses the entire wall\n');
            fprintf(fid, '#--------------------------------\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE 10: Creates a spherical lesion in the XCAT phantom. Depending on where the lesion is placed, it will move with\n');
            fprintf(fid, '#         the respiratory motion. Location of the lesion is specified in pixel values. Initial location of the lesion\n');
            fprintf(fid, '#         needs to be with respect to end-expiration. \n');
            fprintf(fid, '#\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE 11: Creates a plaque in the coronary vessel tree that will move with the cardiac/respiratory motion\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#---------------------------------------------------------------------------\n');
            fprintf(fid, '#  plaque_center: location of plaque along the length of the specified artery\n');
            fprintf(fid, '#    center = 0    -> base of artery\n');
            fprintf(fid, '#    center = 1.0  -> apex of artery\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#-------------------------------------------\n');
            fprintf(fid, '#  plaque_thickness : plaque thickness in mm.\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#-------------------------------------------\n');
            fprintf(fid, '#  plaque_width :   plaque width in mm.\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#-------------------------------------------\n');
            fprintf(fid, '#  plaque_length :  plaque length in mm.\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#------------------------------------------------------\n');
            fprintf(fid, '#  plaque_id  :  vessel to place the plaque in\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#        aorta \n');
            fprintf(fid, '#        rca1\n');
            fprintf(fid, '#        rca2\n');
            fprintf(fid, '#        lad1\n');
            fprintf(fid, '#        lad2\n');
            fprintf(fid, '#        lad3\n');
            fprintf(fid, '#        lcx\n');
            fprintf(fid, '#------------------------------------------------------\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#\n');
            fprintf(fid, '#NOTE12:  Using mode = 4, vectors are output for each voxel of frame 1 to the current frame. The vectors show the motion\n');
            fprintf(fid, '#         from the 1st frame to frame N. The vectors are output as text files with the format of \n');
            fprintf(fid, '#         output_name_vec_frame1_frameN.txt.\n');
            fprintf(fid, '#         The output vectors are a combination of known sampled points from the phantom objects and vectors interpolated\n');
            fprintf(fid, '#         from these sampled points.  The known vectors are designated as such in the vector output.  You can increase\n');
            fprintf(fid, '#         the number of known points (and accuracy of the vector output) by increasing the parameter vec_factor.\n');
            
        end
        
    end
    
end

