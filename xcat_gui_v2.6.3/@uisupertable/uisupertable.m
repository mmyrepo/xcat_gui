classdef uisupertable < handle
    %QUICKTABLE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        tableFigHandle = [];
        tableHandle    = [];
       
        data     = [];
        tooltips = {};
        
        dataChanged     = [];
        dataChangedIndx = 1;
    end
    
    methods
        function obj = uisupertable(data, varargin)
            if nargin > 1
                obj.checkVarargin(varargin);
            end
            obj.tableFigHandle = figure('Name','Parameters Table','Visible','On','NumberTitle','off', 'ToolBar', 'none', 'MenuBar', 'none');
            set(obj.tableFigHandle, 'Position',[400 400 500 520]);
            set(obj.tableFigHandle, 'CloseRequestFcn', @obj.closefigure_fcn);
            
            % Figure Callbacks
            set(obj.tableFigHandle, 'windowbuttonmotionfcn',@obj.windowcursormotion);
            set(obj.tableFigHandle, 'windowbuttondownfcn'  ,@obj.windowbuttondown);
            
            % Table construction
            obj.tableHandle = uitable('Parent', obj.tableFigHandle, 'Position',[20 20 460 500], 'ColumnEditable', [false true]);
            set(obj.tableHandle, 'Tag', 'tTable');
            
            % Table Callbacks
            set(obj.tableHandle, 'CellEditCallback'     , @obj.tableeditvalue );
            set(obj.tableHandle, 'CellSelectionCallback', @obj.tableselectcell);
            %set(obj.tableHandle, );
            
            if ~(nargin < 1)
                %set(obj.tableHandle, 'Data', model.parameters(:,1:2));
                set(obj.tableHandle, 'Data', data);
                obj.data = data;
            end
            set(obj.tableHandle, 'ColumnName' , {'Parameter', 'Value'});
            set(obj.tableHandle, 'ColumnWidth', {210, 180});
            set(obj.tableHandle, 'RowStriping', 'on');
            
            obj.dataChangedIndx = 1;
        end
    end
    
    
    %#==================# 
    %# Callback methods #
    %#==================#
    methods
        function windowcursormotion(obj, hObject, eventdata)
            currentPoint = get(obj.tableFigHandle, 'CurrentPoint');
            tablePos     = get(obj.tableHandle,'Position');
            
            if (currentPoint(2)>= tablePos(2)) && (currentPoint(2) <= tablePos(4))
                currentCell = obj.getTopRowIndex + (tablePos(4) - currentPoint(2)).*obj.getVisibleRowsNumber./(tablePos(4) - tablePos(2)) - 1;
                if currentCell > 0
                    %eventdata.Indices = [ceil(currentCell) 1];
                    %obj.tableselectcell(obj.tableHandle, eventdata);                    
                    set(obj.tableHandle, 'TooltipString', obj.tooltips{ceil(currentCell)});
                end
            end
        end
        
        function windowbuttondown(obj, hObject, eventdata)
            
        end
        
        function tableeditvalue(obj, hObject, eventdata)
            mdesktop     = com.mathworks.mde.desk.MLDesktop.getInstance;
            jFig     = mdesktop.getClient(get(obj.tableFigHandle, 'Name'));
            jTable   = jFig.getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0);
            
            indx = get(jTable, 'SelectedRow') + 1;
            data = get(hObject, 'Data');

            obj.data{indx,2} = data{indx,2};
            %obj.dataChanged = [obj.dataChanged indx];
            obj.dataChanged(obj.dataChangedIndx) = indx;
            obj.dataChangedIndx = obj.dataChangedIndx + 1;
        end
        
        function tableselectcell(obj, hObject, eventdata)
            if ~isempty(eventdata.Indices)
                mdesktop     = com.mathworks.mde.desk.MLDesktop.getInstance;
                jFig     = mdesktop.getClient(get(obj.tableFigHandle, 'Name'));
                jTable   = jFig.getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0);
                
                indx = get(jTable, 'SelectedRow') + 1;
                data = get(hObject, 'Data');
                if strcmpi(data{indx,1}, 'dia_filename') | strcmpi(data{indx,1}, 'ap_filename') | strcmpi(data{indx,1}, 'tumor_motion_filename')
                    [fname, pname] = uigetfile({'*.dat';'*.txt';'*.*'}, 'Select file');
                    if ~fname
                        return;
                    end
                    data{indx,2} = fullfile(pname, fname);
                    set(hObject, 'Data', []);
                    set(hObject, 'Data', data);
                    
                end
            end
            
        end
    end
    
    
    
    %#=============#
    %# Get methods #
    %#=============#
    methods
        function topRow      = getTopRowIndex(obj)
            mdesktop     = com.mathworks.mde.desk.MLDesktop.getInstance;
            jFig     = mdesktop.getClient(get(obj.tableFigHandle, 'Name'));
            jTable   = jFig.getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0);
        
            rowHeight = jTable.getRowHeight;
            topRow    = 1 + abs(jTable.getY)/rowHeight;
        end
        
        function lastRow     = getLastRowIndex(obj)
            mdesktop     = com.mathworks.mde.desk.MLDesktop.getInstance;
            jFig     = mdesktop.getClient(get(obj.tableFigHandle, 'Name'));
            jTable   = jFig.getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0);
        
            tableHeight  = get(jTable.getVisibleRect, 'Height');
            rowHeight    = jTable.getRowHeight;
            numberOfRows = floor(tableHeight/rowHeight);
            
            topRow  = 1 + abs(jTable.getY)/rowHeight;
            lastRow = topRow + numberOfRows-1;           
        end
        
        function numberOfVisibleRows = getVisibleRowsNumber(obj)
            numberOfVisibleRows = obj.getLastRowIndex - obj.getTopRowIndex + 1;
        end
        
        function tableHeight = getVisibleTableHeight(obj)
            mdesktop     = com.mathworks.mde.desk.MLDesktop.getInstance;
            jFig     = mdesktop.getClient(get(obj.tableFigHandle, 'Name'));
            jTable   = jFig.getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0).getComponent(0);
        
            tableHeight  = get(jTable.getVisibleRect, 'Height');
        end
        
        function rowHeight   = getRowHeight(obj)
            tableSize = get(obj.tableHandle, 'Extent'); % In pixels
            rowHeight = tableSize(4)/size(obj.data,1);
            
        end
    end
    
    methods
        function closefigure_fcn(obj, hObject, eventdata)
            %keyboard
            obj.data = get(get(hObject,'Children'), 'Data');
            delete(obj.tableFigHandle);
        end
    end
    
    %#=============#
    %# Set methods #
    %#=============#
    methods
        function setTooltipData(obj, data)
            if ~iscell(data)
                error('ErrDataType:', 'Data must be a cell array');
            end
            obj.tooltips = data;
        end
    end
    
    
    % Utility methods
    methods
        function checkVarargin(obj, vars)
            % Check for Tooltips
            nVars = length(vars);
            indx = 0;
            for i=1:nVars
                if (~iscell(vars{i})) && (ischar(vars{i}))
                    indx = strfind(lower(vars{i}), 'tooltipstring');
                    if indx
                        break;
                    end
                end
            end
            obj.tooltips = vars{i+1};
        end
    end
end

