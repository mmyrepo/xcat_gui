function [varargout ] = superdlg( labels, types, title, varargin)

if nargin < 3
    title = '';
end
if nargin < 1
    labels = {'input:'};
    types  = {'edit'};
    title = '';
end

figHandle = figure('Name',title, 'Visible', 'On', 'NumberTitle', 'off', 'ToolBar', 'none', 'MenuBar', 'none');
set(figHandle, 'Tag', 'tSuperDlg');
set(figHandle, 'Visible', 'off');
%set(figHandle, 'CloseRequestFcn', @closefigure);

handles = guihandles(figHandle);

handles.output = {};
handles.ht     = [];
handles.hl     = [];
handles.hb     = [];
handles.defs   = [];


guidata(handles.tSuperDlg, handles);

if nargin == 4
    handles.defs  = setDefaults(labels, types, varargin);
else
    handles.defs  = setDefaults(labels, types);
end
setupdlg(handles, labels, types);

set(figHandle, 'Visible', 'on');

uiwait(handles.tSuperDlg);
varargout = superdlg_OutputFcn(handles.tSuperDlg, handles);


function varargout = superdlg_OutputFcn(hObject, handles)
% Get default command line output from handles structure

if ishandle(handles.tSuperDlg)
    handles = guidata(handles.tSuperDlg);
    varargout{1} = {handles.output};
    delete(handles.tSuperDlg);
else
    varargout{1} = {};
    delete(hObject);
end

function closefigure(hObject, eventdata)
%delete(hObject);


function defs = setDefaults(labels, types, varargin)
if nargin == 3
    varargin = varargin{1,1}{1,1};
     for i=1:length(labels)
        defs{i} = varargin{i};
    end
else
    for i=1:length(labels)
        if strcmpi(types{i}, 'checkbox')
            defs{i} = '0';
        else
            defs{i} = '';
        end
    end
end




function setupdlg(handles, labels, types)

figHeight = 0;
for k=1:length(labels)
    if strcmpi(types{k}, 'checkbox')
        if k==1
            figHeight = figHeight + 70;
        else
            figHeight = figHeight + 25;
        end
    else
        if k==1
            figHeight = figHeight + 90;
        else
            figHeight = figHeight + 45;
        end
    end
    %fprintf('%d\n', figHeight);
end

scrSize = get(0,'MonitorPositions');
scrSize = scrSize(1,3:4);

% convert character length to pixels to set the size of edit uicontrol
pixelConvFactor = char2pix;
[val, valIndx]  = max(cellfun(@(x) numel(x), labels));
width           = val*pixelConvFactor(1); 
if strcmpi(types{valIndx}, 'checkbox')
    width = width + 80;
end
if width < 170; 
    width = 170; 
end
boxsize = [width 25];
set(handles.tSuperDlg, 'Position',[0.5*scrSize(1) 0.5*scrSize(2) boxsize(1)+10  figHeight]);

figPos = get(handles.tSuperDlg, 'Position');
boxPos  = [5 figPos(4)-30];

Ypos = boxPos(2);

for i=1:length(labels)
    if  strcmpi(types{i}, 'edit')
        handles.hl(i) = uicontrol('Style', 'text', 'String', labels{i}, 'HorizontalAlignment','Left', 'Position', [boxPos(1) Ypos boxsize], 'Parent', handles.tSuperDlg);
        handles.ht(i) = uicontrol('Style', types{i}, 'Position', [boxPos(1) Ypos-15 boxsize], 'Parent', handles.tSuperDlg);
        set(handles.ht(i), 'String', handles.defs{i});
        set(handles.hl(i), 'FontName', 'FixedWidth');
        set(handles.ht(i), 'FontName', 'FixedWidth');
        Ypos = Ypos - 45;
    elseif strcmpi(types{i}, 'checkbox')
        handles.hl(i) = 0;
        handles.ht(i) = uicontrol('Style', types{i}, 'String', labels{i}, 'Position', [boxPos(1) Ypos boxsize], 'Parent', handles.tSuperDlg);
        set(handles.ht(i), 'Value', str2num(handles.defs{i}));
        set(handles.ht(i), 'FontName', 'FixedWidth');
        Ypos = Ypos - 25;
    elseif strcmpi(types{i}, 'openfile')
        handles.hl(i) = uicontrol('Style', 'text', 'String', labels{i}, 'HorizontalAlignment','Left', 'Position', [boxPos(1) Ypos boxsize], 'Parent', handles.tSuperDlg);
        handles.ht(i) = uicontrol('Style', 'edit', 'Position', [boxPos(1) Ypos-15 boxsize(1)-30 25], 'Parent', handles.tSuperDlg);
        handles.hb(i) = uicontrol('Style', 'pushbutton', 'String', '...', 'Position', [boxsize(1)-25 Ypos-15 25 25], 'Tag', strcat('but_',num2str(i)), 'Callback', {@openfilebutton, handles});
        set(handles.ht(i), 'String', handles.defs{i});
        set(handles.ht(i), 'BackgroundColor', [1 1 1]);
        set(handles.ht(i), 'HorizontalAlignment', 'Left');
        set(handles.hl(i), 'FontName', 'FixedWidth');
        set(handles.ht(i), 'FontName', 'FixedWidth');
        set(handles.hb(i), 'FontName', 'FixedWidth');
        Ypos = Ypos - 45;
    end
    if strcmpi(types{i}, 'edit')
        set(handles.ht(i), 'BackgroundColor', [1 1 1]);
        set(handles.ht(i), 'HorizontalAlignment', 'Left');
    end
    

end

% Show Ok-Cancel button
hOk     = uicontrol('Style', 'pushbutton', 'String', 'OK'    , 'Position', [figPos(3)-110 Ypos 50 25], 'Parent', handles.tSuperDlg, 'Callback', {@OKbutton, handles});
hCancel = uicontrol('Style', 'pushbutton', 'String', 'Cancel', 'Position', [figPos(3)-54 Ypos 50 25] , 'Parent', handles.tSuperDlg, 'Callback', {@Cancelbutton, handles});

% Set figure background color
c = get(hOk, 'BackgroundColor');
set(handles.tSuperDlg, 'Color', c);

% Set focus to first object
uicontrol(handles.ht(1));

% Update guidata
guidata(handles.tSuperDlg, handles);


function OKbutton(hObject, eventdata, handles)
%keyboard
answer = [];
for i=1:length(handles.ht)
    style = get(handles.ht(i), 'Style');
    if strcmpi(style, 'edit')
        answer{i} = get(handles.ht(i), 'String');
    elseif strcmpi(style, 'checkbox')
        answer{i} = get(handles.ht(i), 'Value');
    elseif strcmpi(style, 'openfile')
        answer{i} = get(handles.ht(i), 'Value');
    end
end

handles.output = answer;
guidata(handles.tSuperDlg, handles);

uiresume(handles.tSuperDlg);

function Cancelbutton(hObject, eventdata, handles)
handles.output = {};
guidata(handles.tSuperDlg, handles);

uiresume(handles.tSuperDlg);

function openfilebutton(hObject, eventdata, handles)
[fname, pname] = uigetfile({'*.dat';'*.txt';'*.*'}, 'Select file','.');
if ~fname
    return;
end
filename = fullfile(pname, fname);
tag = get(hObject, 'Tag');
r = regexp(tag,'\d');
indx = str2num(tag(r(1):end));
set(handles.ht(indx), 'String', filename);

function pixelFactor = char2pix()
set(gcf,'Units','pixels');
size_pixels = get(gcf,'Position');

set(gcf,'Units','characters');
size_characters = get(gcf,'Position');

pixelFactor = size_pixels(3:4)./size_characters(3:4);
set(gcf,'Units','pixels');
