function DisplayPhantomImage(simHandles)

slice = simHandles.imgSlice;
%slice = xcatData{7,2};

img   = simHandles.img;

%imagesc(fliplr(rot90(permute(img(:,slice,:), [3 1 2]), -1)), 'Parent', simHandles.tPhantomAxes);
imagesc((squeeze(img(slice,:,:))), 'Parent', simHandles.tPhantomAxes);
axis equal; axis tight;
set(simHandles.tPhantomAxes, 'CLim', [0 50]);
axis xy

text(5,10, sprintf('%d/%d', slice, size(img,2)), 'FontName', 'FixedWidth', 'Color', [1 1 0]);

%data     = get(simHandles.tParameterTable, 'Data');

xcatData = get(simHandles.tXCATParameters, 'Data');
startIndx = xcatData{10,2};
endIndx   = xcatData{11,2};

line([startIndx startIndx], [ylim], 'Color', 'm', 'Tag', 'tStartIndexLine');
line([endIndx endIndx    ], [ylim], 'Color', 'm', 'Tag', 'tEndIndexLine'  );

lesionDiam = xcatData{9,2};
tum_x = xcatData{7,2};
tum_y = xcatData{6,2};
tum_z = xcatData{10,2} + xcatData{8,2};
if lesionDiam > 0
    y = tum_z - 0.5*(lesionDiam/xcatData{2,2});
    z = tum_y - 0.5*(lesionDiam/xcatData{1,2});
    w = lesionDiam/xcatData{1,2};
    h = lesionDiam/xcatData{2,2};
    
    x_lim = [tum_x - 0.5*(lesionDiam/xcatData{1,2}) tum_x + 0.5*(lesionDiam/xcatData{1,2})];
    y_lim = [tum_y - 0.5*(lesionDiam/xcatData{1,2}) tum_y + 0.5*(lesionDiam/xcatData{1,2})];
    if simHandles.imgSlice >= x_lim(1) & simHandles.imgSlice <= x_lim(2)
        rectangle('Position', [y z w h], 'Curvature', [1 1], 'FaceColor', [1 1 1]);
    end
end

