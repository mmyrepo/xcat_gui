function [Ratio, ScaleFactor, SIdist, APdist] = TumorDistanceFromEdge(lungEdge, Tx, Ty, Tz)
[x,y]  = find(lungEdge == 1);

% Diaphragm
lungUpy = y(x>Tx);
lungUpx = x(x>Tx);
[lungUpy, indx] = unique(lungUpy, 'last');
gfitUp = fit(lungUpy, lungUpx(indx), 'linearinterp');
Pu = gfitUp(Ty);

lungDowny = y(x<=Tx);
lungDownx = x(x<=Tx);
[lungDowny, indx] = unique(lungDowny, 'last');
gfitDown = fit(lungDowny, lungDownx(indx), 'linearinterp');
Pd = gfitDown(Ty);

SIdist(1) = sqrt((Pu-Tx).^2);
SIdist(2) = sqrt((Pd-Tx).^2);

Ratio(1,1)       = (Tx-Pu)./(Pd-Pu);
ScaleFactor(1,1) = 1./Ratio;

% Chest-Wall
lungRighty = y(y>Ty);
lungRightx = x(y>Ty);
[lungRightx, indx] = unique(lungRightx, 'first');
gfitRight = fit(lungRightx, lungRighty(indx), 'linearinterp');
Pr = gfitRight(Tx);

lungLefty = y(y<=Ty);
lungLeftx = x(y<=Ty);
[lungLeftx, indx] = unique(lungLeftx, 'first');
%keyboard
gfitLeft = fit(lungLeftx, lungLefty(indx), 'linearinterp');
Pl = gfitLeft(Tx);

APdist(1) = sqrt((Pl-Ty).^2);
APdist(2) = sqrt((Pr-Ty).^2);

Ratio(2,1)       = (Ty-Pr)./(Pl-Pr);
ScaleFactor(2,1) = 1./Ratio(2,1);

% Plot The above procedure
%subplot(2,2,4, 'Parent', hf);
hf = figure('Menubar', 'none', 'Toolbar', 'none');
plot(y , x , '.k'); hold on;
plot(Ty, Tx, 'om', 'MarkerFaceColor', 'r');
plot(Ty, Pu, 'or', 'MarkerSize', 10);
plot(Ty, Pd, 'or', 'MarkerSize', 10);
plot(Pr, Tx, 'or', 'MarkerSize', 10);
plot(Pl, Tx, 'or', 'MarkerSize', 10);

axis equal;
limy = ylim;
limx = xlim;
ylim(limy);

line([Ty Ty],[min(ylim) max(ylim)]);
line([min(xlim) max(xlim)], [Tx Tx]);

title('Edge Extraction and Tumor Distance');
