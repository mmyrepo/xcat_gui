% Instructions to create a phantom containing at tumor with the help of the XCAT GUI.
% Author: Matthieu Lafreniere 
% Affiliation: Department of Radiation Oncology - DFCI - BWH - HMS
% Contact: MatthieuLafreniere at DFCI dot Harvard dot edu
% Creation: 9th May 2017 

1) Start Matlab 

2) Go to the xcat_gui directory. The following files must be in the folder: 
"xcat_gui_addpath.m" and "xcat_gui.m" (and you must have the xcat repository that contains
the dxcat2.exe program as well as the accompanying parameter files).

3) Type "xcat_gui". The main GUI must have started and you will be in the main menu window.

4) Choose the option "Construct Phantom". The "Construct Phantom" GUI must have started.

5) Click on the "File" tab, click on "Read Parameter File" 
and open the parameter file that you need (e.g. default_xcat_parfile.par).

6) Right click on the patient image to add a tumor, then click on "add tumor here", 
and inside the "Parameter Window" that opens up, click on "generate tumor parameter file", then "ok".

7) Click on the "File" tab again, then click on "Generate Phantom", and first select the dxcat2.exe
repository, then select the directory in which to create the phantom, tumor, combined and DICOM output files.
If there is none yet, you have to create one, and do this for every new patient/project.
When the window " Parameter files" pops up, don't choose any existing parameter file yet, because we already
chose one during step #5! We will create a tumor file in the next steps which we will use later!
So for now click "Cancel", and in the window "Parameters" that appears wildly, asking you if you would like
to use the current parameters instead of a parameter file, click "Yes (please thank you)"!
Now wait for the phantom .bin files to be generated inside the subfolder "phantom".

8) Click on the "File" tab again, and click on "Generate Phantom" once more. We are now creating the 
tumor files. Select the dxcat2.exe repository, then the main output folder, and finally the parameter 
file "phantom_tumor.par" we have just created during the previous step, inside the main output folder.

9) Let's verify that the tumor files are ok, there should be nothing but a tumor inside it.
Click on "File", then "Main menu", then "Process Phantom" which will open the GUI to process the phantom.
Click on the "File" tab, then "Import Phantom", and go to the folder "phantom_tumor" where the tumor 
.bin files were created, and select all of them and open them. Select the "phantom_tumor.par" once more. 
Scroll to the slices where the tumor should be, if there's nothing redo all the previous steps carefully.

10) Now we are going to combine the phantom files with the tumor files. Click on "Processing", 
then "Combine Tumor", and select all of the .bin files inside the folder "phantom", then all the .bin tumor
files inside the folder "phantom_tumor". Now select the directory that contains both the folders "phantom"
and "phantom_tumor" to allow the program to automatically create the folder "combine" which will contain all
the .bin combined files. Finally select either the "phantom.par" or the "phantom_tumor.par" parameter file.

11) Let's see our combined phantom! In the "processPhantom" GUI, click "File", then "Import Phantom", 
and go to the folder "combined" to open all the .bin combined files. Select the parameter file again,
either one is fine. Double check that the tumor is really there!

12) It's time for the final step, which is the generation of the DICOM images!
Choose the option "Processing" from the "Process Phantom" GUI menu, and then click on "Create DICOM".
Select the main output directory to allow the program to automatically create the folder "dicom".
Et voilà!!!

Now you can import those DICOM (.dcm) files into eclipse and generate a treatment plan! Have fun! :)
