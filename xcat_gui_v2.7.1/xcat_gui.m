function xcat_gui()

clear all; clear; clc;

xcat_gui_addpath()

%% Check directory structure
[dirFlag, baseDir] = checkDirectories();
if ~dirFlag
    return;
end

%% Setup GUI
hMainMenu = hgload(fullfile('gui_figs','main.fig'));
set(hMainMenu, 'Name', 'XCAT GUI v2.7.0', 'NumberTitle', 'Off');
% handles.hMainMenu       = hMainMenu;
% handles.hConstruct      = findobj(hMainMenu, 'Tag', 'tMainMenuConstructPhantom');
% handles.hProcess        = findobj(hMainMenu, 'Tag', 'tMainMenuProcessPhantom'  );
% handles.hSimulate4DCT   = findobj(hMainMenu, 'Tag', 'tMainMenuSimulate4DCT'    );
% handles.hQuit           = findobj(hMainMenu, 'Tag', 'tMainMenuQuit'            );
% 
% set(handles.hConstruct   , 'Callback', {@MainMenuConstruct_Callback   , handles});
% set(handles.hProcess     , 'Callback', {@MainMenuProcess_Callback     , handles});
% set(handles.hSimulate4DCT, 'Callback', {@MainMenuSimulate4DCT_Callback, handles});
% set(handles.hQuit        , 'Callback', {@MainMenuQuit_Callback        , handles});

handles = guihandles(hMainMenu);

handles.baseDir = baseDir;

set(handles.tMainMenuConstructPhantom, 'Callback', {@MainMenuConstruct_Callback   });
set(handles.tMainMenuProcessPhantom  , 'Callback', {@MainMenuProcess_Callback     });
set(handles.tMainMenuSimulate4DCT    , 'Callback', {@MainMenuSimulate4DCT_Callback});
set(handles.tMainMenuQuit            , 'Callback', {@MainMenuQuit_Callback        });


%% Create objects
handles.xcat = xcat_model;

%% Update handles
guidata(handles.tMainMenu, handles);

%% Create listeners
addlistener(handles.xcat, 'guiSelectedFunction', 'PostSet', @(src, event) MainMenuSelectedFunction(src, event, handles));
end

%% GUI Callbacks
function MainMenuConstruct_Callback(hObject, ~)
handles = guidata(hObject);
handles.xcat.setFunction(1);
end

function MainMenuProcess_Callback(hObject, ~)
handles = guidata(hObject);
handles.xcat.setFunction(2);
end

function MainMenuSimulate4DCT_Callback(hObject, ~)
handles = guidata(hObject);
handles.xcat.setFunction(3);
end

function MainMenuQuit_Callback(hObject, ~)
handles = guidata(hObject);
rmpath(fullfile(handles.baseDir,'gui_figs'));
rmpath(fullfile(handles.baseDir,'gui_functions'));
%rmpath(handles.baseDir);
close(handles.tMainMenu);
clear all;
end


%% Check directory structure
function [dirFlag, baseDir] = checkDirectories()
% Check settings for User Path. If 'gui_figs' and 'gui_functions' are not in the path
% check in the current directory to find them. If they are not in the
% current directory ask for xcat_gui main directrory

baseDir =[];

UserPath = regexp(path, pathsep, 'split');
if ispc  % Windows is not case-sensitive
    % If 'gui_figs' in path return true
    figsPath = find(cellfun(@(x) ~isempty(strfind(x,'gui_figs')), UserPath) == 1);
    
    if isempty(figsPath)
        figs = dir(fullfile(pwd,'gui_figs'));
        if isempty(figs)
            baseDir = uigetdir(pwd, 'Select xcat_gui base dir');
            
            if ~baseDir
                uiwait(errordlg('Directory not set. Aborting program execution.'));
                dirFlag = false;
                return;
            end
            
            if ~exist(fullfile(baseDir, 'gui_figs'), 'dir')
                uiwait(errordlg('Directory structure cannot be found. Aborting program execution'));
                dirFlag = false;
                return;
            end
            
            try
                addpath(fullfile(baseDir,'gui_figs'     ));
                addpath(fullfile(baseDir,'gui_functions'));
                %addpath(fullfile(baseDir,'@xcat_model'  ));
                %addpath(fullfile(baseDir,'@uisupertable'));
                dirFlag = true;
            catch err
                dirFlag = false;
                return;
            end
            
        else
            addpath(fullfile(pwd,'gui_figs'     ));
            addpath(fullfile(pwd,'gui_functions'));
            baseDir = pwd;
            dirFlag = true;
        end
    else
       baseDir =  fileparts(UserPath{figsPath});
       dirFlag = true; 
       fprintf('XCAT_gui directory structure found in path\n');
    end
    
else
    figsPath = find(cellfun(@(x) ~isempty(strfind(x,'gui_figs')), UserPath) == 1);
    
    if isempty(figsPath)
        figs = dir(fullfile(pwd,'gui_figs'));
        if isempty(figs)
            baseDir = uigetdir(pwd, 'Select xcat_gui base dir');
            
            if ~baseDir
                uiwait(errordlg('Directory not set. Aborting program execution.'));
                dirFlag = false;
                return;
            end
            
            if ~exist(fullfile(baseDir, 'gui_figs'), 'dir')
                uiwait(errordlg('Directory structure cannot be found. Aborting program execution'));
                dirFlag = false;
                return;
            end
            
            try
                addpath(fullfile(baseDir,'gui_figs'     ));
                addpath(fullfile(baseDir,'gui_functions'));
                
                dirFlag = true;
            catch err
                dirFlag = false;
                return;
            end
            
        else
            addpath(fullfile(pwd,'gui_figs'));
            baseDir = pwd;
            dirFlag = true;
        end
        
    else
        baseDir =  fileparts(UserPath{figsPath});
        dirFlag = true;
    end
end

end

%% Listener Functions

function MainMenuSelectedFunction(src, event, handles)
fprintf('Selected function: %s (%d)\n', handles.xcat.getFunctionName(), handles.xcat.getFunction());

if handles.xcat.getFunction()     == 1
    constructPhantom(handles);
elseif handles.xcat.getFunction() == 2
    processPhantom(handles);
elseif handles.xcat.getFunction() == 3
    Simulate4DCT(handles);
end
end


