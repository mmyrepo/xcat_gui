function CursorMotion(hObject, ~)

simHandles = guidata(hObject);

if ~isempty(simHandles.lineHandle)
    selected = get(simHandles.lineHandle, 'Selected');
    
    if strcmpi(selected, 'on')
        % Get current mouse position
        p = get(gca,'CurrentPoint');
        p = p(1,1);
        
        % Get selected data
        dataIndx = get(simHandles.tSelectData, 'Value');
        t    = simHandles.data(:,1);
        data = simHandles.data(:,dataIndx+1);
        
        % Find time index of new position        
        indx = find( t<= p);
        if isempty(indx) | indx == 0 
            indx = 1 ;
        else
            indx = indx(end);
        end
        
       
        set(simHandles.lineHandle, 'XData', [t(indx) t(indx)]);
        axes(simHandles.tRespSignalAxes);
        delete(findobj('Tag', 'tStartValue'));
        hold on;
        plot(t(indx), data(indx), 'o', 'MarkerEdgeColor', 'g', 'MarkerFaceColor', 'b', 'MarkerSize', 7, 'Tag', 'tStartValue');
        hold off;
        
        statusTxt = sprintf('t=%.3f s, d=%.3f mm', t(indx), data(indx));
        set(simHandles.tStatusBar_Text, 'String', statusTxt);
        drawnow;
        
    end
end

guidata(hObject, simHandles);