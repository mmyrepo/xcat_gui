function hf = DisplayMotionSignals(simHandles)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

% Ask user to set which data column corrersponds to time, SI,AP etc
% Max number of figures is 4
% Assume first column is time

hf = figure('Position', [10 500 1000 800], 'Name','Right Click to Select Motion','Numbertitle','off', 'MenuBar', 'None', 'Toolbar', 'None');
for k=1:min(size(simHandles.data))-1
    hp(k) = subplot(2,2,k);
    plot(simHandles.data(:,1), simHandles.data(:,k+1), 'LineWidth', 2);
    title(sprintf('%s  Range: %.2f mm', simHandles.dataNames{k}, range(simHandles.data(:,k+1))), 'FontSize', 12, 'FontWeight', 'Bold');
    line(xlim, [0 0], 'LineStyle', '--', 'Color', [0.5 0.5 0.5]);
    set(gca, 'LineWidth', 2, 'FontSize', 12);
    xlabel('Time (s)');
    ylabel('Displacement (mm)');
    set(hp(k), 'ButtonDownFcn', {@SubplotClick, simHandles})
end

function SubplotClick(hObject, ~, handles)
figure('Position', [100 500 100 60], 'Menubar', 'none', 'Toolbar', 'none');
popup = uicontrol('Style', 'popup', 'String', {'10','25','50','20'}, 'Position', [10 10 60 50],'background','green', 'Value',2,'Callback',@PopupCallback);

% callback for the drop-down menu
function PopupCallback(obj,event)
sels = get(popup,'String');
idx  = get(popup,'Value');
basebet = str2double(sels{idx});

