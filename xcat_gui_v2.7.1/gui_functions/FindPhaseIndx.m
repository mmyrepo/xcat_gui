function [phaseIndx] = FindPhaseIndx(ap, cineTime, cineInterval, bins)


% Find Maxima and Minima 
% (findpeaks can also be used but it was not consistent. Sometimes it missed findinf the very first minimum)
[maxVal] = max(ap(:,2));
[maxValIndx] = find(ap(:,2) == maxVal);

[minVal] = min(ap(:,2));
[minValIndx] = find(ap(:,2) == minVal);

phaseIndx = [];

% Index separation
% Get only those values that their indices are separatede by more than 
% or equal to index separation
indxSep = floor(0.8*cineTime/min(diff(ap(:,1))));

dIMax = diff(maxValIndx);
if ~isempty(dIMax)
    keepIndx = find(dIMax > indxSep);
    maxValIndx = [maxValIndx(1); dIMax(keepIndx)+1];
end

dIMin = diff(minValIndx);
if ~isempty(dIMin)
    keepIndx = find(dIMin > indxSep);
    minValIndx = [minValIndx(1); dIMin(keepIndx)+1];
end

Use_Times = true;

if Use_Times
    % Shift time to zero
    ap_(:,1) = ap(:,1) - ap(minValIndx(1),1);
    
    % Image sampling times
    t(:,1) = ap_(1,1):cineInterval:(ap_(end,1) - cineInterval);
    %t(:,1) = ap(1,1):cineInterval:ap(end,1);
    
    % Find amplitude at sampling points
    a(:,1) = interp1(ap_(:,1), ap(:,2), t);
    %a(:,1) = interp1(ap(:,1), ap(:,2), t);
    
    % Time bins
    %bins = 10;
    %t_b(:,1) = linspace(0, cineTime, bins);
    
    t_b(:,1) = [0:(cineTime/bins):(cineTime - cineTime/bins)];
    
    % Find phase indx
    phaseIndx = [];
    for j=1:numel(t)
        phaseIndx(j,1) = floor(bins*(t(j,1) - t_b(1,1))/(t_b(end,1) - t_b(1,1)))+1;
    end
    phaseIndx(phaseIndx<1) = 10 + phaseIndx(phaseIndx<1);
    
else
    
    if length(minValIndx) < 2
        errStr = sprintf('Less than 2 valleys were detected.');
        herr = errordlg(errStr, 'ERROR');
        waitfor(herr);
    end

    t0   = ap(minValIndx(1), 1);
    t180 = ap(maxValIndx(1), 1);
    t360 = ap(minValIndx(2), 1);
    
    % Image sampling times
    t(:,1) = ap(1,1):cineInterval:ap(end,1);
        
    tindx = t>=t0 & t<=t180;
    phi =  (t(tindx) - t0)*180/(t180-t0);
    
    tindx = t>t180 & t<=t360;
    phi = [phi; (180 + (t(tindx) - t180)*180/(t360-t180))];
    
    dPhi = 360/bins;
    phiMax = 360 - dPhi;
    %phaseIndx(:,1) = floor((phiMax-phi).*bins./phiMax);
    phaseIndx(:,1) = floor(phi.*bins/phiMax)+1;
    phaseIndx(phaseIndx > bins) = bins;
    
   
end


