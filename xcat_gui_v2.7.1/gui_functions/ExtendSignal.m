function ExtendSignal(hObject, nTimes)

% Update handles
simHandles = guidata(hObject);

% Check if dataNames exist, i.e. a time signal is loaded
if any(strcmpi(fieldnames(simHandles), 'dataNames')) == 0
    errStr = sprintf('Before using this option load an external signal describing the movement\nalong SI, AP, LR dimensions or/and an external surrogate.\nUse the option "Construct lung motion" in XCAT Settings Menu');
    errordlg(errStr);
    return;
end

% dt in time data
dt = min(diff(simHandles.data(:, 1)));

for k=1:nTimes
    % Find new indices for AP motion
    [peakInhale, peakInhaleIndx ] = findpeaks( simHandles.data(:, simHandles.s_indx), 'MinPeakDistance', floor(2/dt), 'MINPEAKHEIGHT', (min( simHandles.data(:, simHandles.s_indx)) + 0.45*range(simHandles.data(:, simHandles.s_indx))));
    [peakExhale, peakExhaleIndx ] = findpeaks(-simHandles.data(:, simHandles.s_indx), 'MinPeakDistance', floor(2/dt), 'MINPEAKHEIGHT', (min(-simHandles.data(:, simHandles.s_indx)) - 0.40*range(simHandles.data(:, simHandles.s_indx))));
    
    
    % Start and end indices of the data to be added to the signal
    startIndx = peakInhaleIndx(1);
    endIndx   = numel(simHandles.data(:, simHandles.s_indx));
    %endIndx   = peakInhaleIndx(end) - 1;
    
    
    for k=1:length(simHandles.dataNames);
        
        % Create the new data
        newdata   = simHandles.data(startIndx:endIndx, k+1);
        
        % add the data immediately after the last peak inhale
        firstIndx = peakInhaleIndx(end);
        if k==1
            simHandles.data(firstIndx:end, :) = [];
            %simHandles.data(end:end+numel(newdata), :) = 0;
        end
        simHandles.data(firstIndx:firstIndx+numel(newdata)-1, k+1) = newdata;
        
    end
    
    % Fix time
    t0 = simHandles.data(1, 1);
    simHandles.data(:, 1) = t0:dt:(t0+(dt*numel(simHandles.data(:, 1)))-dt);
end


%DisplayPlot(simHandles);

guidata(hObject, simHandles)