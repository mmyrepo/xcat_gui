function ScrollWheelMotion(hObject, eventdata)
simHandles = guidata(hObject);
img = simHandles.img;
point = get (simHandles.tPhantomAxes, 'CurrentPoint');
if point(1,1) < 0 || point(1,1) > size(img,3) || point(1,2) < 0 || point(1,2) > size(img,1)
    return;
end



sliceIndex = simHandles.imgSlice;
sliceIndex = sliceIndex + eventdata.VerticalScrollCount;

if sliceIndex < 1; sliceIndex = 1; end
if sliceIndex > size(img,2); sliceIndex = size(img,2); end

simHandles.imgSlice = sliceIndex;

guidata(hObject, simHandles);

DisplayPhantomImage(simHandles);


function tFileMenu_ExportRespSignal_Callback(hObject, ~)

simHandles = guidata(hObject);

[respFile, respFileDir] = uiputfile('*.txt', 'Export Respiratory Signal', 'respSignal.txt');
if ~respFile
    return;
end

fid = fopen(fullfile(respFileDir, respFile), 'w');
for k=1:size(simHandles.data_, 1);
    %     for j=1:size(simHandles.data_,2)
    %
    %     end
    fprintf(fid, '%f ', simHandles.data_(k,:));
    fprintf(fid, '\n');
end

fclose(fid);

guidata(hObject, simHandles);
