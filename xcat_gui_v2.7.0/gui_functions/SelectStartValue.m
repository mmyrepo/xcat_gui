function SelectStartValue(hObject, ~, simHandles)

isSelected = get(simHandles.lineHandle, 'Selected');

if strcmpi(isSelected, 'off')
    set(simHandles.lineHandle, 'Selected', 'On');
else
    set(simHandles.lineHandle, 'Selected', 'Off');
    
    % Accept value or continue 
    choice = questdlg('New Start Value', 'Accept New Start Value?', 'Yes', 'No','Cancel','Yes');
    % Handle response
    switch choice
        case 'Yes'
            % Get selected data
            dataIndx = get(simHandles.tSelectData, 'Value');
            t        = simHandles.data(:,1);
            data     = simHandles.data(:,dataIndx+1);
            
            % Get selected value index
            startTime = get(simHandles.lineHandle, 'XData');
            startTime = startTime(1);
            
            % Find time index of new position
            indx = find( t<startTime);
            if isempty(indx) | indx == 0; indx = 1 ; end
            
            simHandles.data(indx, :) = [];
            
            delete(findobj('Tag', 'tStartValue'));
            
            simHandles.lineHandle = [];
            delete(simHandles.lineHandle);
            guidata(hObject, simHandles);
            
            DisplayPlot(simHandles);
            
        case 'No'
            % Do nothing
        case 'Cancel'
            delete(findobj('Tag', 'tStartValue'));
            simHandles.lineHandle = [];
            delete(simHandles.lineHandle);
            guidata(hObject, simHandles);
    end
end

guidata(hObject, simHandles);