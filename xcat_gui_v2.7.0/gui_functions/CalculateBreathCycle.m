function CalculateBreathCycle(hObject)
%CALCULATEBREATHCYCLE Summary of this function goes here
%   Detailed explanation goes here

simHandles = guidata(hObject);

% Find time step
dt = min(diff(simHandles.data(:,1)));

[peakInhale, peakInhaleIndx ] = findpeaks( simHandles.data(:, simHandles.s_indx), 'MinPeakDistance', floor(2/dt), 'MINPEAKHEIGHT', (min(simHandles.data(:, simHandles.s_indx))  + 0.45*range(simHandles.data(:, simHandles.s_indx))));
[peakExhale, peakExhaleIndx ] = findpeaks(-simHandles.data(:, simHandles.s_indx), 'MinPeakDistance', floor(2/dt), 'MINPEAKHEIGHT', (min(-simHandles.data(:, simHandles.s_indx)) - 0.55*range(simHandles.data(:, simHandles.s_indx))));

peakExhale = -peakExhale;


hf = figure('Position', [10 600 700 400], 'MenuBar', 'None', 'ToolBar', 'None', 'Name', 'Identified Inhale Peaks (close to continue)','Numbertitle','off');
plot(simHandles.data(:,1), simHandles.data(:, simHandles.s_indx), 'LineWidth', 2);
hold
plot(simHandles.data(peakInhaleIndx, 1), peakInhale, 'or', 'MarkerFaceColor', [0.5 0.5 0.5]);
set(gca, 'LineWidth', 2, 'FontSize', 12);
xlabel('time (s)');
ylabel('Displacement (mm)');
%plot(simHandles.data(peakExhaleIndx, 1), peakExhale, 'or', 'MarkerFaceColor', [0.0 0.5 0.0]);
%hold(simHandles.tRespSignalAxes);

waitfor(hf);

breathCycles = diff(simHandles.data(peakInhaleIndx, 1));

bCycles.cycle_mean   = mean(breathCycles);
bCycles.cycle_std    = std(breathCycles);
bCycles.cycle_std    = std(breathCycles);
bCycles.cycle_max    = max(breathCycles);
bCycles.cycle_min    = min(breathCycles);
bCycles.cycle_median = median(breathCycles);
bCycles.cycle_75     = quantile(breathCycles, 0.75);
bCycles.cycle_95     = quantile(breathCycles, 0.95);

bCycles.breathCycles = breathCycles;

data = get(simHandles.t4DCTParameters, 'Data');

% Calulcate Couch moves
Nslices = data{8,2}*10/data{2,2};
Ncouch = (Nslices/data{1,2}) - 1;

% Set Table values
data{4,2}  = bCycles.cycle_mean;
data{5,2}  = data{4,2} + data{3,2};
data{6,2}  = data{3,2};
data{7,2}  = floor(data{5,2}/data{6,2});
data{10,2} = (Ncouch + 1)*data{5,2} + Ncouch*data{9,2};

set(simHandles.t4DCTParameters, 'Data', data);

simHandles.bCycles = bCycles;

guidata(hObject, simHandles);