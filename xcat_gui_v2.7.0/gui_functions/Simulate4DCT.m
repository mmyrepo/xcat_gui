function Simulate4DCT( handles )
%Simulate4DCT Summary of this function goes here
%   Detailed explanation goes here

dctDefaults = {
    'Detector Rows'               , 4    ;
    'Detector Row Thickness (mm)' , 2.5  ;
    'X-Ray Tube Rotation Time (s)', 0.8  ;
    'Respiration Period (s)'      , ''   ;
    'Cine Duration (s)'           , ''    ;
    'Cine interval (s)'           , ''  ;
    'Images per Couch Position'   , ''   ;
    'Axial Scan Length (cm)'      , 25;
    'Couch Translation Time (s)'  , 1    ;
    'Total 4DCT Scan Time (s)'    , '';
    };

xcatDefaults = {
    'pixel size (mm)'     , 2    ;
    'slice thickness (mm)', 2.5  ;
    'X Pixels Array'      , 256  ;
    'Y Pixels Array'      , 256  ;
    'Z Pixels Array'      , 800  ;
    'Tumor X'             , 87   ;
    'Tumor Y'             , 145  ;
    'Tumor Z'             , 63   ;
    'Tumor Diameter (mm)' , 20   ;
    'Start Index'         , 510  ;
    'End Index'           , 610  ;
    };

mainPos = get(handles.tMainMenu, 'Position');
mainPos(1) = 10;

% Hide Main Menu
set(handles.tMainMenu,'Visible','off');

% Load the gui figure
hSimulate4DCT = hgload(fullfile('gui_figs','Simulate4DCT.fig'));

% set the gui position
pos     = get(hSimulate4DCT, 'Position');
set(hSimulate4DCT, 'Position', [mainPos(1)+mainPos(3) pos(2) pos(3) pos(4)]);

% create the handles to store data and properties
simHandles = guihandles(hSimulate4DCT);

% Create variables
simHandles.data   = [];
simHandles.resp_t = [];
simHandles.resp_s = [];

simHandles.resp_t_indx = [];
simHandles.resp_s_indx = [];

simHandles.resp_peakInhaleIndx = [];
simHandles.resp_peakExhaleIndx      = [];

simHandles.xcatExeDir = [];

simHandles.lineHandle = [];

simHandles.bCycles = [];

simHandles.phaseBins = 10;

simHandles.useDefaultMotion = false;

% Set default values to tables
set(simHandles.t4DCTParameters, 'Data', dctDefaults);
set(simHandles.tXCATParameters, 'Data', xcatDefaults  );

% #===========#
% # CALLBACKS #
% #===========#

% Callbacks for scrolling phantom slices
set(simHandles.tSimulate4DCTWindow, 'WindowButtonMotionFcn', @CursorMotion);
set(simHandles.tSimulate4DCTWindow, 'WindowScrollWheelFcn' , {@ScrollWheelMotion});
%set(simHandles.tSimulate4DCTWindow, 'WindowButtonUpFcn'    , {@buttonUp, conHandles});

% Create callback for close
set(simHandles.tSimulate4DCTWindow, 'CloseRequestFcn', {@tSimulate4DCTWindow_Close, simHandles, handles});

% Create the callback to change table data
set(simHandles.t4DCTParameters, 'CellEditCallback', {@t4DCTParameters_CellEdit_Callback, simHandles, handles});

% Create the callback to change table data
set(simHandles.tXCATParameters, 'CellEditCallback', {@tXCATParameters_CellEdit_Callback, simHandles, handles});

% Create callback for setting the XCAT executable folder
set(simHandles.tXCATSettingsMenu_ExeDir, 'Callback', {@tXCATSettingsMenu_ExeDir_Callback});

% Create the callback to simulate the 4DCT process
set(simHandles.tSimulate_Button, 'Callback', {@tSimulate_Button_Callback, handles});

% Create the callback to sort the 4DCT images
set(simHandles.t4DCT_Sort, 'Callback', {@t4DCT_Sort_Callback});

% Create callback to import motion data
set(simHandles.tFileMenu_LoadMotionData, 'Callback', {@tFileMenu_LoadMotionData_Callback});

% Create callback for default motion data
set(simHandles.tFileMenu_UseDefaultMotion, 'Callback', {@tFileMenu_UseDefaultMotion_Callback});

% Create the callback to export breathing signal
set(simHandles.tFileMenu_ExportRespSignal, 'Callback', {@tFileMenu_ExportRespSignal_Callback});

% Create the callback to quit
set(simHandles.tFileMenu_Quit, 'Callback', {@tFileMenu_Quit_Callback, simHandles, handles});

% Create the callback to select data for display
set(simHandles.tSelectData, 'Callback', {@tSelectData_Callback, handles});

% Create callback to change the start of the breathing signal
set(simHandles.tRespSignal_SelectStart, 'Callback', {@tRespSignal_SelectStart_Callback, simHandles});

% Create callback to shift the breathing signal to zero
set(simHandles.tRespSignal_ShiftZero, 'Callback', {@tRespSignal_ShiftZero_Callback, simHandles});

% Create callback to scale amplitude of breathing signal to zero
set(simHandles.tRespSignal_ScaleZero, 'Callback', {@tRespSignal_ScaleZero_Callback, simHandles});

% Create callback to scale amplitude of breathing signal to zero
set(simHandles.tRespSignal_AcceptSignal, 'Callback', {@tRespSignal_AcceptSignal_Callback});

% Create callback to invert amplitude of
set(simHandles.tRespSignal_Invert, 'Callback', {@tRespSignal_Invert_Callback});

% Create callback to fix time gaps
set(simHandles.tSetPhaseBins, 'Callback', {@tSetPhaseBins_Callback});

% Create callback to normalize data to 10
set(simHandles.tRespSignal_Normalize, 'Callback', {@tRespSignal_Normalize_Callback});

% Create callback to reset modified data to defaults values
set(simHandles.tRespSignal_Reset, 'Callback', {@tRespSignal_Reset_Callback});

% Callback for mous click on resp plot
set(simHandles.tRespSignalAxes, 'ButtonDownFcn', {@tRespSignalAxes_ButtonDownCallback, simHandles})

% Load the phantom image for scout view
simHandles.img      = [];
simHandles.imgSlice = [];
load('gui_figs/imgActivity_lowRes.mat');
simHandles.img     = img;
simHandles.img_    = img;
xcatData = get(simHandles.tXCATParameters, 'Data');
simHandles.imgSlice = xcatData{7,2};

% Display phantom image
DisplayPhantomImage(simHandles);

% Fix table size depending on platform
if ispc
    % Fix 4DCT table
    set(simHandles.t4DCTParameters, 'Units', 'Characters');
    tablePos = get(simHandles.t4DCTParameters, 'Position');
    set(simHandles.t4DCTParameters, 'Position', [tablePos(1) tablePos(2) tablePos(3) tablePos(4)-2]);
    
    set(simHandles.t4DCT_Panel, 'Units', 'Characters');
    panelPos = get(simHandles.t4DCT_Panel, 'Position');
    set(simHandles.t4DCT_Panel, 'Position', [panelPos(1) panelPos(2)+2 panelPos(3) panelPos(4)-2]);
    
    % Fix XCAT table
    set(simHandles.tXCATParameters, 'Units', 'Characters');
    tablePos = get(simHandles.tXCATParameters, 'Position');
    set(simHandles.tXCATParameters, 'Position', [tablePos(1) tablePos(2) tablePos(3) tablePos(4)-2]);
    
    set(simHandles.tXCAT_Panel, 'Units', 'Characters');
    panelPos = get(simHandles.tXCAT_Panel, 'Position');
    set(simHandles.tXCAT_Panel, 'Position', [panelPos(1) panelPos(2)+2 panelPos(3) panelPos(4)-2]);
    
end


% Update the handles
guidata(hSimulate4DCT, simHandles);


function t4DCTParameters_CellEdit_Callback(hObject, eventdata, simHandles, handles)

if isempty(eventdata.NewData)
    return;
end

simHandles = guidata(hObject);

% Retrieve table data for further calculations
data     = get(simHandles.t4DCTParameters, 'Data');
xcatData = get(simHandles.tXCATParameters, 'Data');

rowIndex = eventdata.Indices(1);

if rowIndex == 1    
    % Calulcate Couch moves
    Nslices = data{8,2}*10/data{2,2};
    Ncouch = (Nslices/data{1,2}) - 1;
    
    % Set Table values
    data{4,2}  = bCycles.cycle_mean;
    data{5,2}  = data{4,2} + data{3,2};
    data{6,2}  = data{3,2};
    data{7,2}  = floor(data{5,2}/data{6,2});
    data{10,2} = (Ncouch + 1)*data{5,2} + Ncouch*data{9,2};

    
elseif rowIndex == 2
    rows         = data{1,2};
    rowThickness = data{2,2}*0.1;
    cineDuration = data{5,2};
    axialLength  = data{8,2};
    couchTime    = data{9,2};
    
    slices     = axialLength/rowThickness;
    couchMoves = slices/rows - 1;
    
    pixelArrayZ = xcatData{5,2};
    tumorPosZ   = xcatData{8,2};
    startSlice  = xcatData{10,2};
    endSlice    = xcatData{11,2};
    
    % Calculate ratio of tumor position before change of thickness
    r_z = (tumorPosZ - startSlice)/(endSlice - startSlice);
    
    % Calculate ratio of first and last slice index to total slices before change
    r_start = (startSlice - 1)/(pixelArrayZ - 1);
    r_end   = (endSlice   - 1)/(pixelArrayZ - 1);
    
    % Change axial scan length and total scan time
    data{8,2}  = (endSlice -startSlice)*rowThickness;
    data{10,2} = (slices/rows)*cineDuration + couchMoves*couchTime;
    
    % Change XCAT slice thickness and pixel array along z axis
    xcatData{2,2} = rowThickness/0.1;
    xcatData{5,2} = floor(handles.xcat.imgLength/(0.1*xcatData{2,2}));
    
    pixelArrayZ_new = xcatData{5,2};
    
    % Calculate new first and last slice index
    startSlice_new = floor(r_start*(pixelArrayZ_new - 1) + 1);
    endSlice_new   = floor(  r_end*(pixelArrayZ_new - 1) + 1);
    
    % Calculate new tumor position
    tumorPosZ_new = floor(r_z*(endSlice_new - startSlice_new) + startSlice_new);
    
    % Change tumor position, start slice and end slice
    xcatData{8,2}  = tumorPosZ_new ;
    xcatData{10,2}  = startSlice_new;
    xcatData{11,2} = endSlice_new  ;
    
elseif rowIndex == 3
    data{5,2} = data{4,2} + data{3,2};

elseif rowIndex == 4
    Nslices = data{8,2}*10/data{2,2};
    Ncouch = (Nslices/data{1,2}) - 1;
    
    data{5,2}  = data{4,2} + data{3,2};
    data{7,2}  = floor(data{5,2}/data{6,2});
    data{10,2} = (Ncouch + 1)*data{5,2} + Ncouch*data{9,2};
    
elseif rowIndex == 5
    rows         = data{1,2};
    rowThickness = data{2,2}*0.1;
    cineDuration = data{5,2};
    cineInterval = data{6,2};
    axialLength  = data{8,2};
    couchTime    = data{9,2};
    
    slices     = axialLength/rowThickness;
    couchMoves = slices/rows - 1;
    
    data{7,2}  = floor(cineDuration/cineInterval);
    data{10,2} = (slices/rows)*cineDuration + couchMoves*couchTime;
    
elseif rowIndex == 6
    cineDuration = data{5,2};
    cineInterval = data{6,2};
    
    data{7,2} = floor(cineDuration/cineInterval);
    
elseif rowIndex == 7
    imagesPerCouch = data{7,2};
    cineDuration   = data{5,2};
    
    data{6,2} = cineDuration/imagesPerCouch;
    
elseif rowIndex == 8
    rows         = data{1,2};
    rowThickness = data{2,2}*0.1;
    cineDuration = data{5,2};
    axialLength  = data{8,2};
    couchTime    = data{9,2};
    
    startSlice   = xcatData{10,2};
    
    slices     = axialLength/rowThickness;
    couchMoves = slices/rows - 1;
    
    data{10,2} = (slices/rows)*cineDuration + couchMoves*couchTime;
    
    xcatData{11,2} = floor(startSlice + axialLength/rowThickness);
    
end


set(simHandles.t4DCTParameters, 'Data', data    );
set(simHandles.tXCATParameters, 'Data', xcatData);

guidata(hObject, simHandles);



function tXCATParameters_CellEdit_Callback(hObject, eventdata, simHandles, handles)

simHandles = guidata(hObject);

if isempty(eventdata.NewData)
    return;
end

% Retrieve table data for further calculations
data     = get(simHandles.t4DCTParameters, 'Data');
xcatData = get(simHandles.tXCATParameters, 'Data');

rowIndex = eventdata.Indices(1);
if rowIndex == 1
    pixelSize  = xcatData{1,2};
    tumorPosX  = xcatData{6,2};
    tumorPosY  = xcatData{7,2};
    
    % Tumor location ratio before change
    r_x = (tumorPosX - 1)/(xcatData{3,2} - 1);
    r_y = (tumorPosY - 1)/(xcatData{4,2} - 1);
    
    xcatData{3,2} = floor(handles.xcat.imgWidth/(0.1*pixelSize));
    xcatData{4,2} = floor(handles.xcat.imgWidth/(0.1*pixelSize));
    
    pixelArray = xcatData{3,2};
    
    tumorPosX_new = floor(r_x*(pixelArray - 1) + 1);
    tumorPosY_new = floor(r_y*(pixelArray - 1) + 1);
    
    xcatData{1,2} = 10*handles.xcat.imgWidth/pixelArray;
    xcatData{4,2} = pixelArray;
    xcatData{6,2} = tumorPosX_new;
    xcatData{7,2} = tumorPosY_new;
    
elseif rowIndex == 2
    if xcatData{2,2} ~= data{2,2}
        xcatData{2,2} = data{2,2};
    end
    
elseif rowIndex == 3
    pixelArray = xcatData{3,2};
    tumorPosX  = xcatData{6,2};
    tumorPosY  = xcatData{7,2};
    
    r_x = (tumorPosX - 1)/(eventdata.PreviousData - 1);
    r_y = (tumorPosY - 1)/(eventdata.PreviousData - 1);
    
    tumorPosX_new = floor(r_x*(pixelArray - 1) + 1);
    tumorPosY_new = floor(r_y*(pixelArray - 1) + 1);
    
    xcatData{1,2} = 10*handles.xcat.imgWidth/pixelArray;
    xcatData{4,2} = pixelArray;
    xcatData{6,2} = tumorPosX_new;
    xcatData{7,2} = tumorPosY_new;
    
elseif rowIndex == 4
    pixelArray = xcatData{4,2};
    tumorPosX  = xcatData{6,2};
    tumorPosY  = xcatData{7,2};
    
    r_x = (tumorPosX - 1)/(eventdata.PreviousData - 1);
    r_y = (tumorPosY - 1)/(eventdata.PreviousData - 1);
    
    tumorPosX_new = floor(r_x*(pixelArray - 1) + 1);
    tumorPosY_new = floor(r_y*(pixelArray - 1) + 1);
    
    xcatData{1,2} = 10*handles.xcat.imgWidth/pixelArray;
    xcatData{4,2} = pixelArray;
    xcatData{6,2} = tumorPosX_new;
    xcatData{7,2} = tumorPosY_new;
    
elseif rowIndex == 10
    tumorPosZ  = xcatData{8,2} ;
    startSlice = xcatData{10,2} ;
    endSlice   = xcatData{11,2};
    
    % find difference between start positions
    d_z = startSlice - eventdata.PreviousData;
    
    % Calculate new tumor position
    tumorPosZ_new = tumorPosZ - d_z;
    
    if tumorPosZ_new <= 0
        uiwait(errordlg('Tumor position is outside scan area!!!!'));
    end
    
    % Change the tumor position along z
    xcatData{8,2} = tumorPosZ_new;
    
    data{8,2} = (endSlice - startSlice)*0.1*data{2,2};
    
    rows         = data{1,2};
    rowThickness = data{2,2}*0.1;
    cineDuration = data{5,2};
    axialLength  = data{8,2};
    couchTime    = data{9,2};
    
    slices     = axialLength/rowThickness;
    couchMoves = slices/rows - 1;
    
    data{10,2} = (slices/rows)*cineDuration + couchMoves*couchTime;
    
elseif rowIndex == 11
    
    tumorPosZ  = xcatData{8,2} ;
    startSlice = xcatData{10,2} ;
    endSlice   = xcatData{11,2};
    
    % check if endslice <= tumorPosZ
    if endSlice <= tumorPosZ
        uiwait(errordlg('Tumor position is outside scan area!!!!'));
    end
    
    
    data{8,2} = (endSlice - startSlice)*0.1*data{2,2};
    
    rows         = data{1,2};
    rowThickness = data{2,2}*0.1;
    cineDuration = data{5,2};
    axialLength  = data{8,2};
    couchTime    = data{9,2};
    
    slices     = axialLength/rowThickness;
    couchMoves = slices/rows - 1;
    
    data{10,2} = (slices/rows)*cineDuration + couchMoves*couchTime;
    
end


set(simHandles.t4DCTParameters, 'Data', data    );
set(simHandles.tXCATParameters, 'Data', xcatData);


% Resize phantom image
simHandles.img = handles.xcat.scaleImage(simHandles.img_, [xcatData{3,2} xcatData{4,2} xcatData{5,2}]);

% Change the displayed phantom image
DisplayPhantomImage(simHandles);

guidata(hObject, simHandles);



function tSimulate_Button_Callback(hObject, ~, handles)

simHandles = guidata(hObject);

if isempty(simHandles.xcatExeDir) | strcmpi(simHandles.xcatExeDir, '')
    tXCATSettingsMenu_ExeDir_Callback(hObject);
    simHandles = guidata(hObject);
end


if isempty(simHandles.xcatExeDir) | strcmpi(simHandles.xcatExeDir, '')
    uiwait(errordlg('XCAT executable folder not specified'));
    return;
end

% Retrieve table data for further calculations
data     = get(simHandles.t4DCTParameters, 'Data');
xcatData = get(simHandles.tXCATParameters, 'Data');

% Check if scan time is greater than the loaded signal and extend the signal
totalScanTime = data{10,2};
dt = min(diff(simHandles.data(:, 1)));
if simHandles.data(end, 1) < totalScanTime
    %uiwait(errordlg(sprintf('The time in the respiratory signal data is less than the 4DCT scan time!!!')));
    %return;
    n = floor(totalScanTime/simHandles.data(end, 1));
    if simHandles.useDefaultMotion == false
        ExtendSignal(hObject, n);
        simHandles = guidata(hObject);
    elseif simHandles.useDefaultMotion == true
        newdataDC   = simHandles.data(1:end, 2);
        newdataAP   = simHandles.data(1:end, 3);
        for k=1:n
            firstIndx = length(simHandles.data(:,1)) + 1;
            simHandles.data(firstIndx:(firstIndx+numel(newdataDC)-1), 2) = newdataDC;
            simHandles.data(firstIndx:(firstIndx+numel(newdataAP)-1), 3) = newdataAP;
        end
        % Fix time
        t0 = simHandles.data(1, 1);
        simHandles.data(:, 1) = t0:dt:(t0+(dt*numel(simHandles.data(:, 1)))-dt);
        guidata(hObject, simHandles);
    end
end

simHandles = guidata(hObject);

pixel_width   = xcatData{1,2}*0.1;
slice_width   = xcatData{2,2}*0.1;  % in cm
array_size    = xcatData{3,2};
x_loc         = xcatData{6,2};
y_loc         = xcatData{7,2};
z_loc         = xcatData{8,2};
lesn_diameter = xcatData{9,2};
startslice    = xcatData{10,2};
endslice      = xcatData{11,2};


detectorRows = data{1,2};
respPeriod   = data{4,2};
cineTime     = data{5,2};
timePerFrame = data{6,2};
outFrames    = data{7,2};
couchTime    = data{9,2};

% Set the number of phase bins
%simHandles.phaseBins = outFrames;

% Set main XCAT parameters
handles.xcat.setParameterValue('slice_width'   , slice_width);
handles.xcat.setParameterValue('pixel_width'   , pixel_width);
handles.xcat.setParameterValue('array_size'    , array_size);
handles.xcat.setParameterValue('x_location'    , x_loc);
handles.xcat.setParameterValue('y_location'    , y_loc);
handles.xcat.setParameterValue('z_location'    , z_loc);
handles.xcat.setParameterValue('lesn_diameter' , lesn_diameter);
handles.xcat.setParameterValue('time_per_frame', timePerFrame);
handles.xcat.setParameterValue('out_frames'    , outFrames);
handles.xcat.setParameterValue('out_period'    , -1);

handles.xcat.setParameterValue('mode'           , 0);
handles.xcat.setParameterValue('act_phan_each'  , 0);
handles.xcat.setParameterValue('atten_phan_each', 1);
handles.xcat.setParameterValue('motion_option'  , 2);
handles.xcat.setParameterValue('resp_period'    , respPeriod);

handles.xcat.setParameterValue('energy'         , 120);

handles.xcat.setParameterValue('arms_flag'      , 0);

% Number of generated files
numberOfFiles = floor((xcatData{11,2}-xcatData{10,2}+1)/(data{1,2}));

outputDir = uigetdir('.', 'Select output directory');
if ~outputDir
    return;
end

if ~exist(fullfile(outputDir, 'DICOM'), 'dir')
    mkdir(fullfile(outputDir, 'DICOM'));
end

%Get only the required breathing data
ap_indx = simHandles.s_indx;
dc_indx = simHandles.tumSI_indx;

% Get the default data
%t  = simHandles.data_(:,t_indx);
%ap = simHandles.data_(:,ap_indx);
%dc = simHandles.data_(:,dc_indx);

t  = simHandles.data(:, 1);
%ap = simHandles.data(:, ap_indx);
%dc = simHandles.data(:, dc_indx);
ap = simHandles.data(:, 3);
dc = simHandles.data(:, 2);

% Normalize ap and dc tom max expansion
AP_Max = simHandles.APexpansion;
DC_Max = simHandles.DCexpansion;

ap = ap.*AP_Max./max(abs(ap));
dc = dc.*DC_Max./max(abs(dc));

% Starting phantom slices
currentStartSlice = startslice;
currentEndSlice   = currentStartSlice + detectorRows - 1;

% Starting time frame
t1 = 0;
t2 = cineTime;

% Starting Time Frame indices
t_i = find(t>=t1 & t<=t2);


% Cell matrix to store the phase index and the corresponding filename
phaseFrames = {};
hf = [];
hax = [];
for k=1:numberOfFiles
    outDir = fullfile(outputDir, sprintf('couch_pos_%d', k));
    if ~exist(outDir, 'dir')
        mkdir(outDir);
    end
    if ~exist(fullfile(outDir, 'phantom'), 'dir')
        mkdir(fullfile(outDir, 'phantom'));
    end
    if ~exist(fullfile(outDir, 'tumor'), 'dir')
        mkdir(fullfile(outDir, 'tumor'));
    end
    if ~exist(fullfile(outDir, 'vector'), 'dir')
        mkdir(fullfile(outDir, 'vector'));
    end
    
    str = sprintf('Generating parameter files for time frame: %.2f - %.2f s', t1, t2);
    set(simHandles.tStatusBar_Text, 'String', str);
    drawnow;
    
    % Get max AP and DC for the current time frame
    [~, DCmaxIndx] = max(abs(dc(t_i)));
    [~, APmaxIndx] = max(abs(ap(t_i)));
    DCmax = dc(t_i(DCmaxIndx));
    APmax = ap(t_i(APmaxIndx));
    
    if isempty(APmax); keyboard; end
     
    if k==1
        %hf = figure('Position', [10 500 1900 500]);
        %hax = axes('Parent', hf);
        %axes(simHandles.tRespSignalAxes);
        %plot(t(t<=totalScanTime), ap(t<=totalScanTime), 'k', 'LineWidth', 2);
        %hold on;
    end
    %axes(simHandles.tRespSignalAxes);
    plot(t(t_i), ap(t_i), 'Parent', simHandles.tRespSignalAxes, 'LineWidth', 3, 'Color', 'm'); 
    xlim([min(t(t_i)) max(t(t_i))]);
    %line([t1 t1], ylim, 'Color', 'r', 'LineWidth', 2);
    %line([t2 t2], ylim, 'Color', 'r', 'LineWidth', 2);
   
    % Normalize AP and DC curves to 10
    dc(t_i) = 10.*(dc(t_i)./max(abs(dc(t_i))));
    ap(t_i) = 10.*(ap(t_i)./max(abs(ap(t_i))));
    
    % Find phase indx
    [phaseIndx] = FindPhaseIndx([t(t_i) ap(t_i)], cineTime, timePerFrame, simHandles.phaseBins);
    

    
    for l=1:numel(phaseIndx)
        phase{l,1} = phaseIndx(l);
        phase{l,2} = fullfile(outDir, 'combined', sprintf('slice_%d_c_atn_%d.bin', k, l));
    end
    
    % Store to cell array
    phaseFrames = [phaseFrames;phase];
    
    % Write AP and DC files
    if ~exist(fullfile(outDir, 'ap.dat'), 'file')
        handles.xcat.writeAP([t(t_i) ap(t_i)], fullfile(outDir, 'ap.dat'));
    end
    if ~exist(fullfile(outDir, 'dc.dat'), 'file')
        handles.xcat.writeDC([t(t_i) dc(t_i)], fullfile(outDir, 'dc.dat'));
    end
    
    
    % Change XCAT parameters
    handles.xcat.setParameterValue('startslice'          , currentStartSlice);
    handles.xcat.setParameterValue('endslice'            , currentEndSlice);
    handles.xcat.setParameterValue('max_diaphragm_motion', 0.1*abs(DCmax));
    handles.xcat.setParameterValue('max_AP_exp'          , 0.1*abs(APmax));
    handles.xcat.setParameterValue('dia_filename'        , fullfile(outDir, 'dc.dat'));
    handles.xcat.setParameterValue('ap_filename'         , fullfile(outDir, 'ap.dat'));
    
    % Tumor position must be adjusted for each parameter file
    tumPosZ       = startslice + z_loc - 1;        % This is the actual tumor position
    z_loc_current = tumPosZ - currentStartSlice;   % This is the tumor position relative to the current start slice
    
    % Update xcat parameters
    handles.xcat.setParameterValue('z_location', z_loc_current);
    
    % Write Parameter files
    writeParameterFlag = get(simHandles.tWriteParameters, 'Value');
    if writeParameterFlag
        handles.xcat.writeParameterFile(fullfile(outDir, 'parameters.par'    ), 0, 2);
        handles.xcat.writeParameterFile(fullfile(outDir, 'parameters_tum.par'), 2, 2);
        handles.xcat.writeParameterFile(fullfile(outDir, 'parameters_vec.par'), 4, 2);
    end
    
    % Get next time frame
    t1 = t2 + couchTime;
    t2 = t1 + cineTime;
    
    % Get next Time Frame time indices
    t_i = find(t>=t1 & t<=t2);
    
    % Get XCAT slices for next couch position
    currentStartSlice = currentEndSlice + 1;
    currentEndSlice   = currentStartSlice + detectorRows - 1;
    
end

% Check if phantom and tumor files have already been generated
missingFiles    = [];
for f=1:numberOfFiles
    for ft = 1:outFrames
        if ~exist(fullfile(outputDir, sprintf('couch_pos_%d', f), 'phantom', sprintf('slice_%d_atn_%d.bin', f, ft)), 'file')
            missingFiles = [missingFiles; [f, ft]];
        end
    end
end

missingTumFiles = [];
for f=1:numberOfFiles
    for ft = 1:outFrames
        if ~exist(fullfile(outputDir, sprintf('couch_pos_%d', f), 'tumor', sprintf('slice_%d_atn_%d.bin', f, ft)), 'file')
            missingTumFiles = [missingTumFiles; [f, ft]];
        end
    end
end

missingVecFiles = [];
for f=1:numberOfFiles
    for ft = 1:outFrames
        if ~exist(fullfile(outputDir, sprintf('couch_pos_%d', f), 'vector', sprintf('slice_%d_atn_%d.bin', f, ft)), 'file')
            missingVecFiles = [missingVecFiles; [f, ft]];
        end
    end
end


% Create bash file for batch run in linux
if isunix | ismac
    useWine = 1;
    if ~isempty(missingFiles)
        fid = fopen(fullfile(outputDir, 'runAll.sh'), 'w');
        fprintf(fid, '#!/bin/bash\n');
        fprintf(fid, 'XCAT_HOME=%s\n', simHandles.xcatExeDir);
        fprintf(fid, 'PARFILE_DIR=%s\n', outputDir);
        %fprintf(fid, 'for j in {1..%d}\n', numberOfFiles);
        fprintf(fid, 'for j in ');
        fprintf(fid, '%d ', unique(missingFiles(:,1)));
        fprintf(fid, '\n');
        fprintf(fid, 'do\n');
        fprintf(fid, 'cd $XCAT_HOME\n');
        if useWine == 1
            fprintf(fid, 'wine dxcat2.exe $PARFILE_DIR/couch_pos_${j}/parameters.par $PARFILE_DIR/couch_pos_${j}/phantom/slice_${j}\n');
        else
            fprintf(fid, './dxcat2 $PARFILE_DIR/couch_pos_${j}/parameters.par $PARFILE_DIR/couch_pos_${j}/phantom/slice_${j}\n');
        end
        fprintf(fid, 'done\n');
        
        fclose(fid);
    end
    
    
    % Same file for tumor
    if ~isempty(missingTumFiles)
        fid = fopen(fullfile(outputDir, 'runAll_Tumor.sh'), 'w');
        fprintf(fid, '#!/bin/bash\n');
        fprintf(fid, 'XCAT_HOME=%s\n', simHandles.xcatExeDir);
        fprintf(fid, 'PARFILE_DIR=%s\n', outputDir);
        %fprintf(fid, 'for j in {1..%d}\n', numberOfFiles);
        fprintf(fid, 'for j in ');
        fprintf(fid, '%d ', unique(missingTumFiles(:,1)));
        fprintf(fid, '\n');
        fprintf(fid, 'do\n');
        fprintf(fid, 'cd $XCAT_HOME\n');
        if useWine == 1
            fprintf(fid, 'wine dxcat2.exe $PARFILE_DIR/couch_pos_${j}/parameters_tum.par $PARFILE_DIR/couch_pos_${j}/tumor/slice_${j}\n');
        else
            fprintf(fid, './dxcat2 $PARFILE_DIR/couch_pos_${j}/parameters_tum.par $PARFILE_DIR/couch_pos_${j}/tumor/slice_${j}\n');
        end
        fprintf(fid, 'done\n');
        
        fclose(fid);
    end
    
    % Same file for vector
    if ~isempty(missingVecFiles)
        fid = fopen(fullfile(outputDir, 'runAll_Vector.sh'), 'w');
        fprintf(fid, '#!/bin/bash\n');
        fprintf(fid, 'XCAT_HOME=%s\n', simHandles.xcatExeDir);
        fprintf(fid, 'PARFILE_DIR=%s\n', outputDir);
        %fprintf(fid, 'for j in {1..%d}\n', numberOfFiles);
        fprintf(fid, 'for j in ');
        fprintf(fid, '%d ', unique(missingVecFiles(:,1)));
        fprintf(fid, '\n');
        fprintf(fid, 'do\n');
        fprintf(fid, 'cd $XCAT_HOME\n');
        if useWine == 1
            fprintf(fid, 'wine dxcat2.exe $PARFILE_DIR/couch_pos_${j}/parameters_vec.par $PARFILE_DIR/couch_pos_${j}/vector/slice_${j}\n');
        else
            fprintf(fid, './dxcat2 $PARFILE_DIR/couch_pos_${j}/parameters_vec.par $PARFILE_DIR/couch_pos_${j}/vector/slice_${j}\n');
        end
        fprintf(fid, 'done\n');
        
        fclose(fid);
    end
    
elseif ispc
    fid = fopen(fullfile(outputDir, 'runAll.bat'), 'w');
    
    fprintf(fid, '@echo off\n');
    fprintf(fid, 'set XCAT_HOME=%s\n', simHandles.xcatExeDir);
    fprintf(fid, 'set PARFILE_DIR=%\n', outputDir);
    fprintf(fid, 'for /L %%%%j in (1,1,%d) do (\n', numberOfFiles);
    fprintf(fid, 'cd %%XCAT_HOME%%\n');
    fprintf(fid, 'dxcat2.exe %%PARFILE_DIR%%\couch_pos_%%%%j\parameters.par %%PARFILE_DIR%%\couch_pos_%%%%j\phantom\slice_%%%%j\n');
    fprintf(fid, 'cd %%PARFILE_DIR%%\n');
    fprintf(fid, ')\n');
    
    fclose(fid);
    
    % Tumor files
    fid = fopen(fullfile(outputDir, 'runAll_tum.bat'), 'w');
    
    fprintf(fid, '@echo off\n');
    fprintf(fid, 'set XCAT_HOME=%s\n', simHandles.xcatExeDir);
    fprintf(fid, 'set PARFILE_DIR=%\n', outputDir);
    fprintf(fid, 'for /L %%%%j in (1,1,%d) do (\n', numberOfFiles);
    fprintf(fid, 'cd %%XCAT_HOME%%\n');
    fprintf(fid, 'dxcat2.exe %%PARFILE_DIR%%\couch_pos_%%%%j\parameters_tum.par %%PARFILE_DIR%%\couch_pos_%%%%j\tumor\slice_%%%%j\n');
    fprintf(fid, 'cd %%PARFILE_DIR%%\n');
    fprintf(fid, ')\n');
    
    fclose(fid);
    
    % Vector files
    fid = fopen(fullfile(outputDir, 'runAll_vec.bat'), 'w');
    
    fprintf(fid, '@echo off\n');
    fprintf(fid, 'set XCAT_HOME=%s\n', simHandles.xcatExeDir);
    fprintf(fid, 'set PARFILE_DIR=%\n', outputDir);
    fprintf(fid, 'for /L %%%%j in (1,1,%d) do (\n', numberOfFiles);
    fprintf(fid, 'cd %%XCAT_HOME%%\n');
    fprintf(fid, 'dxcat2.exe %%PARFILE_DIR%%\couch_pos_%%%%j\parameters_vec.par %%PARFILE_DIR%%\couch_pos_%%%%j\vector\slice_%%%%j\n');
    fprintf(fid, 'cd %%PARFILE_DIR%%\n');
    fprintf(fid, ')\n');
    
    fclose(fid);
    
end

% Now call the xcat executable and generate the phantom and tumor files
generatePhantoms    = get(simHandles.tGeneratePhantomFiles, 'Value');
generateTumors      = get(simHandles.tGenerateTumorFiles  , 'Value');
generateVectors     = get(simHandles.tGenerateVectorFiles , 'Value');
combineTumorPhantom = get(simHandles.tCombineTumorPhantom , 'Value');


% Create files to run
%if generatePhantoms
%    if isunix | ismac
%        cmdLine1 = sprintf('sh %s &', fullfile(outputDir, 'runAll.sh'));
%        if ~isempty(missingFiles)
%            s  = sprintf('couch_pos_%d/phantom/slice_%d_atn_%d.bin', k, k, j);
%            [status1,cmdout1] = system(cmdLine1);
%            fprintf(cmdout1);
%        end
%    end
%end

if generatePhantoms
    UseWine = true;
    cd(simHandles.xcatExeDir);
    for k=1:numberOfFiles
        s  = sprintf('Generating phantom at couch_pos_%d', k);
        set(simHandles.tStatusBar_Text, 'String', s); drawnow
        parFile = fullfile(outputDir, sprintf('couch_pos_%d', k), 'parameters.par');
        outFile = fullfile(outputDir, sprintf('couch_pos_%d', k), 'phantom', sprintf('slice_%d', k));
        exeStr = '';
        if UseWine
            exeStr = sprintf('wine dxcat2.exe %s %s', parFile, outFile);
        else
        end
        [status1,cmdout1] = system(exeStr);
        fprintf(cmdout1);
        if status1
            s  = sprintf('couch_pos_%d...Done', k);
            set(simHandles.tStatusBar_Text, 'String', s); drawnow
        end
    end
end

set(simHandles.tStatusBar_Text, 'String', 'Finished phantom generation'); drawnow

if generateTumors
    UseWine = true;
    cd(simHandles.xcatExeDir);
    for k=1:numberOfFiles
        s  = sprintf('Generating tumor at couch_pos_%d', k);
        set(simHandles.tStatusBar_Text, 'String', s); drawnow
        parFile = fullfile(outputDir, sprintf('couch_pos_%d', k), 'parameters_tum.par');
        outFile = fullfile(outputDir, sprintf('couch_pos_%d', k), 'tumor', sprintf('slice_%d', k));
        exeStr = '';
        if UseWine
            exeStr = sprintf('wine dxcat2.exe %s %s', parFile, outFile);
        else
        end
        [status1,cmdout1] = system(exeStr);
        fprintf(cmdout1);
        if status1
            s  = sprintf('couch_pos_%d...Done', k);
            set(simHandles.tStatusBar_Text, 'String', s); drawnow
        end
    end
end

set(simHandles.tStatusBar_Text, 'String', 'Finished tumor generation'); drawnow

%if generateTumors
%    if isunix | ismac
%        cmdLine2 = sprintf('sh %s &', fullfile(outputDir, 'runAll_Tumor.sh'));
%        if ~isempty(missingTumFiles)
%            [status2,cmdout2] = system(cmdLine2);
%            fprintf(cmdout2);
%        end
%    end
%end

%if generateVectors
%    if isunix | ismac
%        cmdLine3 = sprintf('sh %s &', fullfile(outputDir, 'runAll_Vector.sh'));
%        if ~isempty(missingTumFiles)
%            [status3,cmdout3] = system(cmdLine3);
%            fprintf(cmdout3);
%        end
%    end
%end


%for k=1:numberOfFiles
%    for j=1:outFrames
%        s  = sprintf('couch_pos_%d/phantom/slice_%d_atn_%d.bin', k, k, j);
%        st = sprintf('couch_pos_%d/tumor/slice_%d_atn_%d.bin', k, k, j);
%        curfile  = fullfile(outputDir, s);
%        curfilet = fullfile(outputDir, st);
%        while ~exist(curfile, 'file') | ~exist(curfilet, 'file')
%            if ~exist(curfile, 'file')
%                s2 = sprintf('Generating phantom %d at time frame %d', k, j);
%                set(simHandles.tStatusBar_Text, 'String', s2); drawnow
%            elseif ~exist(curfilet, 'file')
%                s2 = sprintf('Generating tumor %d at time frame %d', k, j);
%                set(simHandles.tStatusBar_Text, 'String', s2); drawnow
%            end
%        end
%        
%    end
%end

if combineTumorPhantom
    % Combine Phantom with tumor at each couch position
    for k=1:numberOfFiles
        for j=1:outFrames
            s1 = sprintf('Combining phantom and tumor at couch pos %d', k);
            set(simHandles.tStatusBar_Text, 'String', s1); drawnow
            s = sprintf('couch_pos_%d', k);
            cmbFileName = fullfile(outputDir, s, 'combined', sprintf('slice_%d_c_atn_%d.bin', k, j));
            %fprintf('%s...', cmbFileName);
            if ~exist(cmbFileName, 'file')
                phnDir = fullfile(outputDir, s, 'phantom'       );
                tumDir = fullfile(outputDir, s, 'tumor'         );
                outDir = fullfile(outputDir, s, 'combined'      );
                parNam = fullfile(outputDir, s, 'parameters.par');
                
                if ~exist(outDir)
                    mkdir(outDir);
                end
                
                
                handles.xcat.combineTumorAutomated(phnDir, tumDir, outDir, parNam);
                %handles.xcat.combineTumor;
                fprintf('Created!\n');
            else
                fprintf('Exists!\n');
            end
        end
    end
    
    set(simHandles.tStatusBar_Text, 'String', 'Files combined'); drawnow
    
end

save(fullfile(outputDir, 'phaseFiles.mat'), 'phaseFrames');


% Sort the 4DCT combined files and store them to a new folder
sort4DCT = get(simHandles.t4DCT_Sort, 'Value');
if sort4DCT
    if ~exist(fullfile(outputDir, 'phase_sorted'), 'dir')
        mkdir(fullfile(outputDir, 'phase_sorted'));
    end
    
    % Fix negative phase indices
    p(:,1) = [phaseFrames{:,1}];
    %i_p = find(p>10);
    %i_n = find(p<0);
    %p(i_n) = (floor(abs(p(i_n))/10)+1)*10 + p(i_n);
    %p(i_p) = p(i_p) - 10;
    
   
    for pf = 1:numel(p)
        phaseFrames{pf,1} = p(pf,1);
    end
    
    % Matrix: rows are phase indices and columns arecouch position indices
    p2d = reshape(p, simHandles.phaseBins,[]);
    
    for si=1:simHandles.phaseBins
        if ~exist(fullfile(outputDir, 'phase_sorted', sprintf('phase_%d', si-1)), 'dir')
            mkdir(fullfile(outputDir, 'phase_sorted', sprintf('phase_%d', si-1)));
        end
        sortedOutDir = fullfile(outputDir, 'phase_sorted', sprintf('phase_%d', si-1));
        
        % Find indices corresponding to each phase (si)
        %fl_i = find(p==si);
        
        [phaseIndex, couchPosIndex] = find(p2d==si);
        phaseFrames_cur = [];
        phaseFrames_cur = phaseFrames(sub2ind(size(p2d),phaseIndex,couchPosIndex),2);
        
        % Get the differential and keep only those indices that have
        % difference greater than the phase bins. i.e. they belong to
        % different couch positions.
        dCouchPosIndex = diff(couchPosIndex);
        sameCouchPosIndx = find(dCouchPosIndex == 0) + 1;
        phaseFrames_cur(sameCouchPosIndx) = [];
        
        %keyboard
         
        img = [];
        for i=1:length(phaseFrames_cur)
            
            fprintf('%s\n', phaseFrames_cur{i});
            
            fid = fopen(phaseFrames_cur{i}, 'r');
            img2 = fread(fid, 'float32');
            
            img2 = reshape(img2, array_size, array_size, []);
            fclose(fid);
            
            copyfile(phaseFrames_cur{i}, sortedOutDir);
            
            img = cat(3,img, img2);
        end
        
        % Write phase sorted image for specific phase
        fileout = fopen(fullfile(sortedOutDir, sprintf('phaseSortedImage_%d.bin', si-1)), 'w');
        fwrite(fileout, img, 'float32');
        fclose(fileout);
        
    end
    set(simHandles.tStatusBar_Text, 'String', 'Finished!');
end


guidata(hObject, simHandles);


function t4DCT_Sort_Callback(hObject, ~)
simHandles = guidata(hObject);

guidata(hObject, simHandles);

function tFileMenu_Quit_Callback(hObject, ~, simHandles, handles)

if ishandle(handles.tMainMenu)
    delete(handles.tMainMenu);
end

delete(simHandles.tSimulate4DCTWindow);

rmpath('gui_figs');
rmpath('gui_functions');


function tSelectData_Callback(hObject, ~, handles)
simHandles = guidata(hObject);

DisplayPlot(simHandles);

guidata(hObject, simHandles)



function tFileMenu_LoadMotionData_Callback(hObject, ~)

simHandles = guidata(hObject);

data = zeros(100,3);

hf = figure('Position', [100 500 650 700], 'MenuBar', 'None', 'Toolbar', 'None', 'Name','Respiratory Motion Data','Numbertitle','off');
ht = uitable(hf,'Data',data,'ColumnWidth',{160}, 'Position', [10 30 550 650]);
set(ht, 'ColumnName', {'Time (s)', 'DC Displacement (mm)', 'AP Displacement (mm)'});
set(ht, 'ColumnEditable', [true true true]);

hm = uicontextmenu();
m1 = uimenu(hm, 'Label', 'Paste', 'Callback', @PasteClipboard);
set(ht, 'UiContextMenu', hm);


function PasteClipboard(hObject, eventdata)
data = clipboard('pastespecial');
if isempty(data)
    return;
end

fldname = fieldnames(data);
if isfield(data, 'data');
    data = data.data;
else
    data = data.(fldname{1});
end
data = data(:, 1:3);
hTable = findobj(gcf, 'Type', 'uitable');
set(hTable, 'Data', data);


% [fname, pname] = uigetfile({'*.txt';'.dat'},'Select breathing/tumor motion file');
% if ~fname
%     return;
% end
% 
% fid = fopen(fullfile(pname, fname), 'r');
% data = importdata(fullfile(pname, fname), ' ');
% fclose(fid);
% 
% if isstruct(data)
%     data = data.data;
% end
% 
% simHandles.data  = data;
% simHandles.data_ = data;
% 
% simHandles.dataNames = [];
% 
% Attempt to identify Motion automatically
% rgn = [];
% for k=2:min(size(simHandles.data))
%     rng(k-1) = range(simHandles.data(:,k));
% end
% 
% [~, indx] = sort(rng, 'descend');
% simHandles.data(:, 2:end) = simHandles.data(:, indx+1);
% simHandles.dataNames = {'Tumor SI', 'Surrogate', 'Tumor AP', 'Tumor ML'};
% 
% Subtract Mean
% for k=2:min(size(simHandles.data))
%     simHandles.data(:,k) = simHandles.data(:,k) - mean(simHandles.data(:,k));
% end
% 
% hf = DisplayMotionSignals(simHandles)
% waitfor(hf);
% 
% 
% set(simHandles.tSelectData, 'String', simHandles.dataNames);
% 
% 
% simHandles.t_indx     = 1;
% simHandles.s_indx     = 3;
% simHandles.tumSI_indx = 2;
% simHandles.tumAP_indx = 4;
% 
% simHandles.resp_t     = simHandles.data(:,1);
% simHandles.resp_tumSI = simHandles.data(:,2);
% if length(indx)>2
%     simHandles.resp_s     = simHandles.data(:,3);
%     simHandles.resp_tumAP = simHandles.data(:,4);
% end
% 
% simHandles.APexpansion = range(simHandles.data(:, 4));
% simHandles.DCexpansion = range(simHandles.data(:, 2));
% 
% Update 4DCT Parameter table
% data = get(simHandles.t4DCTParameters, 'Data');
% 
% simHandles.useDefaultMotion = false;
% 
% guidata(hObject, simHandles);
% 
% Caclulate Breath Cycles
% CalculateBreathCycle(hObject);
% 
% DisplayPlot(simHandles);


function tFileMenu_UseDefaultMotion_Callback(hObject, ~)

simHandles = guidata(hObject);

motionDC = [0,0;0.250000000000000,-0.380600000000000;0.500000000000000,-1.46447000000000;0.750000000000000,-3.08658000000000;1,-5;1.25000000000000,-6.91342000000000;1.50000000000000,-8.53553000000000;1.75000000000000,-9.61940000000000;2,-10;2.25000000000000,-9.82963000000000;2.50000000000000,-9.33013000000000;2.75000000000000,-8.53553000000000;3,-7.50000000000000;3.25000000000000,-6.29410000000000;3.50000000000000,-5;3.75000000000000,-3.70590000000000;4,-2.50000000000000;4.25000000000000,-1.46447000000000;4.50000000000000,-0.669870000000000;4.75000000000000,-0.170370000000000;5,0];
motionAP = [0,0;0.250000000000000,0.380600000000000;0.500000000000000,1.46447000000000;0.750000000000000,3.08658000000000;1,5;1.25000000000000,6.91342000000000;1.50000000000000,8.53553000000000;1.75000000000000,9.61940000000000;2,10;2.25000000000000,9.82963000000000;2.50000000000000,9.33013000000000;2.75000000000000,8.53553000000000;3,7.50000000000000;3.25000000000000,6.29410000000000;3.50000000000000,5;3.75000000000000,3.70590000000000;4,2.50000000000000;4.25000000000000,1.46447000000000;4.50000000000000,0.669870000000000;4.75000000000000,0.170370000000000;5,0];

data(:,1) = motionDC(:,1);
data(:,2) = motionDC(:,2);  % Diaphragm
data(:,3) = motionAP(:,2);  % AP or Surrogate

simHandles.data_ = data;
simHandles.data  = data;

simHandles.APexpansion = 12;  % mm
simHandles.DCexpansion = 20;  % mm

simHandles.dataNames = {'Diaphragm', 'AP'};
set(simHandles.tSelectData, 'String', simHandles.dataNames);

simHandles.t_indx     = 1;
simHandles.s_indx     = 3;
simHandles.tumSI_indx = 2;
simHandles.tumAP_indx = [];

simHandles.resp_t     = simHandles.data(:,1);
simHandles.resp_s     = simHandles.data(:,3);
simHandles.resp_tumSI = simHandles.data(:,2);

simHandles.resp_tumAP = [];

simHandles.useDefaultMotion = true;

guidata(hObject, simHandles);

% Caclulate Breath Cycles
bCycles.cycle_mean   = range(simHandles.data(:,1));
bCycles.cycle_std    = 0;
bCycles.cycle_max    = range(simHandles.data(:,1));
bCycles.cycle_min    = range(simHandles.data(:,1));
bCycles.cycle_median = range(simHandles.data(:,1));
bCycles.cycle_75     = range(simHandles.data(:,1));
bCycles.cycle_95     = range(simHandles.data(:,1));

bCycles.breathCycles = [];

data = get(simHandles.t4DCTParameters, 'Data');

% Calulcate Couch moves
Nslices = data{8,2}*10/data{2,2};
Ncouch = (Nslices/data{1,2}) - 1;

% Set Table values
data{4,2}  = bCycles.cycle_mean;
data{5,2}  = data{4,2} + data{3,2};
data{6,2}  = data{3,2};
data{7,2}  = floor(data{5,2}/data{6,2});
data{10,2} = (Ncouch + 1)*data{5,2} + Ncouch*data{9,2};

set(simHandles.t4DCTParameters, 'Data', data);

simHandles.bCycles = bCycles;
guidata(hObject, simHandles);

DisplayPlot(simHandles);



function tXCATSettingsMenu_ExeDir_Callback(hObject, ~)

simHandles = guidata(hObject);

xcatExeDir = uigetdir('.', 'Select XCAT Executable folder');
if ~xcatExeDir
    return;
end

simHandles.xcatExeDir = xcatExeDir;

guidata(hObject, simHandles)


function tRespSignal_SelectStart_Callback(hObject, ~, simHandles)

% Update handles
simHandles = guidata(hObject);



% Check if dataNames exist, i.e. a time signal is loaded
if any(strcmpi(fieldnames(simHandles), 'dataNames')) == 0
    errStr = sprintf('Before using this option load an external signal describing the movement\nalong SI, AP, LR dimensions or/and an external surrogate.\nUse the option "Construct lung motion" in XCAT Settings Menu');
    errordlg(errStr);
    return;
end

% Find current plot index
dataIndx = get(simHandles.tSelectData, 'Value');

% Get current plot data
t    = simHandles.data(:, 1);
data = simHandles.data(:, dataIndx+1);

%keyboard
%delete(findobj('Tag', 'tPeakExhalePlot', 'Parent', simHandles.tRespSignalAxes));

%hold(simHandles.tRespSignalAxes);
%plot(simHandles.data(peakExhaleIndx, simHandles.t_indx), peakExhale, 'or', 'MarkerSize', 10, 'MarkerFaceColor', 'g', 'Parent', simHandles.tRespSignalAxes, 'Tag', 'tPeakExhalePlot', 'ButtonDownFcn', {@PeakExhaleSelect, simHandles});
%plot(simHandles.data(peakInhaleIndx, simHandles.t_indx), peakInhale, 'or', 'MarkerSize', 10, 'MarkerFaceColor', 'g', 'Parent', simHandles.tRespSignalAxes, 'Tag', 'tPeakExhalePlot', 'ButtonDownFcn', {@PeakExhaleSelect, simHandles});

simHandles.lineHandle = line([t(1) t(1)], ylim, 'Color', 'm', 'LineWidth', 1);
guidata(hObject, simHandles);

set(simHandles.lineHandle, 'ButtonDownFcn', {@SelectStartValue, simHandles});

%title(sprintf('%s', simHandles.dataNames{dataIndx}));
%hold(simHandles.tRespSignalAxes);

%simHandles.resp_peakInhaleIndx = peakInhaleIndx;
%simHandles.resp_peakExhaleIndx = peakExhaleIndx;

guidata(hObject, simHandles);


function tRespSignal_ShiftZero_Callback(hObject, ~, simHandles)

% Update handles
simHandles = guidata(hObject);

% Check if dataNames exist, i.e. a time signal is loaded
if any(strcmpi(fieldnames(simHandles), 'dataNames')) == 0
    errStr = sprintf('Before using this option load an external signal describing the movement\nalong SI, AP, LR dimensions or/and an external surrogate.\nUse the option "Construct lung motion" in XCAT Settings Menu');
    errordlg(errStr);
    return;
end

t_indx   = simHandles.t_indx;

% Shift time to zero
simHandles.data(:,t_indx) = simHandles.data(:,t_indx) - simHandles.data(1,t_indx);

DisplayPlot(simHandles);

guidata(simHandles.tRespSignalAxes, simHandles);


function tRespSignal_ScaleZero_Callback(hObject, ~, simHandles)

% Update handles
simHandles = guidata(hObject);

% Check if dataNames exist, i.e. a time signal is loaded
if any(strcmpi(fieldnames(simHandles), 'dataNames')) == 0
    errStr = sprintf('Before using this option load an external signal describing the movement\nalong SI, AP, LR dimensions or/and an external surrogate.\nUse the option "Construct lung motion" in XCAT Settings Menu');
    errordlg(errStr);
    return;
end

% Find data index
dataIndx = get(simHandles.tSelectData, 'Value');
%dataIndx = simHandles.dataNames{dataIndx, 3};

% Scale amplitude to zero
simHandles.data(:,dataIndx+1) = simHandles.data(:,dataIndx+1) - (simHandles.data(1,dataIndx+1));

DisplayPlot(simHandles);

guidata(simHandles.tRespSignalAxes, simHandles);


function tRespSignal_Invert_Callback(hObject, ~)
% Update handles
simHandles = guidata(hObject);

% Check if dataNames exist, i.e. a time signal is loaded
if any(strcmpi(fieldnames(simHandles), 'dataNames')) == 0
    errStr = sprintf('Before using this option load an external signal describing the movement\nalong SI, AP, LR dimensions or/and an external surrogate.\nUse the option "Construct lung motion" in XCAT Settings Menu');
    errordlg(errStr);
    return;
end

% Find data index
dataIndx = get(simHandles.tSelectData, 'Value');

simHandles.data(:,dataIndx+1) = -simHandles.data(:,dataIndx+1);

DisplayPlot(simHandles);

guidata(hObject, simHandles);


function tSetPhaseBins_Callback(hObject, ~)
% Update handles
simHandles = guidata(hObject);

% Check if dataNames exist, i.e. a time signal is loaded
if any(strcmpi(fieldnames(simHandles), 'dataNames')) == 0
    errStr = sprintf('Before using this option load an external signal describing the movement\nalong SI, AP, LR dimensions or/and an external surrogate.\nUse the option "Construct lung motion" in XCAT Settings Menu');
    errordlg(errStr);
    return;
end

prompt = {'Number of phase bins:'};
dlg_title = 'Set numbr of phase bins';
num_lines = 1;
defaultans = {num2str(simHandles.phaseBins)};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);

if isempty(answer)
    return;
end

simHandles.phaseBins = str2num(answer{1});

%DisplayPlot(simHandles);

guidata(hObject, simHandles);


function tRespSignal_Normalize_Callback(hObject, ~)
% Update handles
simHandles = guidata(hObject);

% Check if dataNames exist, i.e. a time signal is loaded
if any(strcmpi(fieldnames(simHandles), 'dataNames')) == 0
    errStr = sprintf('Before using this option load an external signal describing the movement\nalong SI, AP, LR dimensions or/and an external surrogate.\nUse the option "Construct lung motion" in XCAT Settings Menu');
    errordlg(errStr);
    return;
end

%for k=1:length(simHandles.dataNames);
%    indx = simHandles.dataNames{k};
%    simHandles.data(:, indx+1) = (simHandles.data(:,indx+1)./max(abs(simHandles.data(:,indx+1)))).*10;
%end

%DisplayPlot(simHandles);

prompt = {'DC Expansion (mm):','AP Expansion (mm):'};
dlg_title = 'Enter maximum DC and AP expansion';
num_lines = 1;
defaultans = {'20','12'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);

if isempty(answer)
    return;
end

DCmax = str2num(answer{1});
APmax = str2num(answer{2});

simHandles.DCexpansion = DCmax;
simHandles.APexpansion = APmax;

%handles.xcat.setParameterValue('max_diaphragm_motion', 0.1*abs(DCmax));
%handles.xcat.setParameterValue('max_AP_exp'          , 0.1*abs(APmax));

guidata(hObject, simHandles);


function tRespSignal_AcceptSignal_Callback(hObject, ~)
% Update handles
simHandles = guidata(hObject);

% Check if dataNames exist, i.e. a time signal is loaded
if any(strcmpi(fieldnames(simHandles), 'dataNames')) == 0
    errStr = sprintf('Before using this option load an external signal describing the movement\nalong SI, AP, LR dimensions or/and an external surrogate.\nUse the option "Construct lung motion" in XCAT Settings Menu');
    errordlg(errStr);
    return;
end

simHandles.data_ = simHandles.data;

guidata(hObject, simHandles);





function tRespSignal_Reset_Callback(hObject, ~)
% Update handles
simHandles = guidata(hObject);

% Check if dataNames exist, i.e. a time signal is loaded
if any(strcmpi(fieldnames(simHandles), 'dataNames')) == 0
    errStr = sprintf('Before using this option load an external signal describing the movement\nalong SI, AP, LR dimensions or/and an external surrogate.\nUse the option "Construct lung motion" in XCAT Settings Menu');
    errordlg(errStr);
    return;
end

simHandles.data = simHandles.data_;

DisplayPlot(simHandles);


guidata(hObject, simHandles);


function tRespSignalAxes_ButtonDownCallback(hObject, ~, simHandles)

%plotHandle = findobj('Tag', 'tPeakExhalePlot', 'Parent', simHandles.tRespSignalAxes));
guidata(hObject, simHandles);



function tSimulate4DCTWindow_Close(hObject, ~, simHandles, handles)

delete(simHandles.tSimulate4DCTWindow);
delete(handles.tMainMenu);

rmpath('gui_figs');
rmpath('gui_functions');








