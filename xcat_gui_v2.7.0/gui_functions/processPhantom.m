function processPhantom(handles)

mainPos = get(handles.tMainMenu, 'Position');
mainPos(1) = 10;
set(handles.tMainMenu, 'Position',mainPos);


hProcess   = hgload(fullfile('gui_figs','processPhantom.fig'));
proPos     = get(hProcess, 'Position');
set(hProcess, 'Position', [mainPos(1)+mainPos(3) proPos(2) proPos(3) proPos(4)]);

proHandles = guihandles(hProcess);

proHandles.imageType = 'grayscale';
proHandles.colormap  = 'jet';

set(handles.tMainMenu,'Visible','off');

%#===========#
%# Callbacks #
%#===========#
set(proHandles.tProcessWindow, 'WindowButtonMotionFcn', {@cursorMotion     , proHandles, handles.xcat});
set(proHandles.tProcessWindow, 'WindowScrollWheelFcn' , {@scrollWheelMotion, handles.xcat});

set(proHandles.tFileMenu_Quit               , 'Callback', {@tFileMenu_Quit_Callback               , proHandles, handles});
set(proHandles.tFileMenu_MainReturn         , 'Callback', {@tFileMenu_MainReturn_Callback         , proHandles, handles});

set(proHandles.tFileMenu_ExportAnimation    , 'Callback', {@tFileMenu_ExportAnimation_Callback    , proHandles, handles.xcat});
set(proHandles.tFileMenu_ExportWorkspace    , 'Callback', {@tFileMenu_ExportWorkspace_Callback    , proHandles, handles.xcat});
set(proHandles.tFileMenu_ExportASCII        , 'Callback', {@tFileMenu_ExportASCII_Callback        , proHandles, handles.xcat});

set(proHandles.tFileMenu_ImportMatlab       , 'Callback', {@tFileMenu_ImportMatlab_Callback       , proHandles, handles.xcat});
set(proHandles.tFileMenu_ImportWorkspace    , 'Callback', {@tFileMenu_ImportWorkspace_Callback    , proHandles, handles.xcat});
set(proHandles.tFileMenu_ImportPhantom      , 'Callback', {@tFileMenu_ImportPhantom_Callback      , proHandles, handles.xcat});

set(proHandles.tProcessMenu_CombineTumor , 'Callback', {@tProcessMenu_CombineTumor_Callback , proHandles, handles.xcat});
set(proHandles.tProcessMenu_CenterTumor  , 'Callback', {@tProcessMenu_CenterTumor_Callback  , proHandles, handles.xcat});
set(proHandles.tProcessMenu_CompareImages, 'Callback', {@tProcessMenu_CompareImages_Callback, handles.xcat});
set(proHandles.tProcessMenu_ConvertHU    , 'Callback', {@tProcessMenu_ConvertHU_Callback    , proHandles, handles.xcat});
set(proHandles.tProcessMenu_ConvertDICOM , 'Callback', {@tProcessMenu_ConvertDICOM_Callback , proHandles, handles.xcat});
set(proHandles.tProcessMenu_AIP          , 'Callback', {@tProcessMenu_ConvertAIP_Callback   , proHandles, handles.xcat});
set(proHandles.tProcessMenu_MIP          , 'Callback', {@tProcessMenu_ConvertMIP_Callback   , proHandles, handles.xcat});

set(proHandles.tViewMenu_Coronal   , 'Callback', {@tViewMenu_Coronal_Callback   , proHandles, handles.xcat});
set(proHandles.tViewMenu_Sagittal  , 'Callback', {@tViewMenu_Sagittal_Callback  , proHandles, handles.xcat});
set(proHandles.tViewMenu_Transverse, 'Callback', {@tViewMenu_Transverse_Callback, proHandles, handles.xcat});
set(proHandles.tViewMenu_Original  , 'Callback', {@tViewMenu_Original_Callback  , proHandles, handles.xcat});

set(proHandles.tSelectSlice, 'Callback', {@tSelectSlice_Callback, proHandles, handles.xcat});

% Image Context Menu
set(proHandles.tImageContextMenu, 'Callback', {@tImageContextMenu_Callback, proHandles});

% Image Context Menu Scale
set(proHandles.tImageContextMenu_Aspect       , 'Callback', {@tImageContextMenu_Aspect_Callback       , proHandles, handles.xcat});
set(proHandles.tImageContextMenu_Aspect_Normal, 'Callback', {@tImageContextMenu_Aspect_Normal_Callback, proHandles, handles.xcat});
set(proHandles.tImageContextMenu_Aspect_Square, 'Callback', {@tImageContextMenu_Aspect_Square_Callback, proHandles, handles.xcat});
set(proHandles.tImageContextMenu_Aspect_Equal , 'Callback', {@tImageContextMenu_Aspect_Equal_Callback , proHandles, handles.xcat});

set(proHandles.tImageContextMenu_CreateROI    , 'Callback', {@tImageContextMenu_CreateROI_Callback    , handles.xcat});   
set(proHandles.tImageContextMenu_ModifyROI    , 'Callback', {@tImageContextMenu_ModifyROI_Callback    , handles.xcat});   
set(proHandles.tImageContextMenu_DeleteROI    , 'Callback', {@tImageContextMenu_DeleteROI_Callback    , proHandles, handles.xcat}); 
set(proHandles.tImageContextMenu_InfoROI      , 'Callback', {@tImageContextMenu_InfoROI_Callback      , handles.xcat}); 

set(proHandles.tImageContextMenu_ImageType_GrayScale , 'Callback', {@tImageContextMenu_ImageType_GrayScale_Callback , handles.xcat});   
set(proHandles.tImageContextMenu_ImageType_Color_Jet , 'Callback', {@tImageContextMenu_ImageType_Color_Jet_Callback , handles.xcat}); 
set(proHandles.tImageContextMenu_ImageType_Color_Hot , 'Callback', {@tImageContextMenu_ImageType_Color_Hot_Callback , handles.xcat}); 
set(proHandles.tImageContextMenu_ImageType_Color_Gray, 'Callback', {@tImageContextMenu_ImageType_Color_Gray_Callback, handles.xcat});
set(proHandles.tImageContextMenu_ImageType_Color_Bone, 'Callback', {@tImageContextMenu_ImageType_Color_Bone_Callback, handles.xcat}); 

set(proHandles.tImageContextMenu_PixelValues_Default   , 'Callback', {@tImageContextMenu_ImageType_PixelValues_Default_Callback   , handles.xcat});   
set(proHandles.tImageContextMenu_PixelValues_Absolute  , 'Callback', {@tImageContextMenu_ImageType_PixelValues_Absolute_Callback  , handles.xcat}); 
set(proHandles.tImageContextMenu_PixelValues_Squared   , 'Callback', {@tImageContextMenu_ImageType_PixelValues_Squared_Callback   , handles.xcat}); 
set(proHandles.tImageContextMenu_PixelValues_Normalized, 'Callback', {@tImageContextMenu_ImageType_PixelValues_Normalized_Callback, handles.xcat});

set(proHandles.tImageSlider      , 'Callback', {@tImageSlider_Callback      , proHandles, handles.xcat});


%set(proHandles.tImageAxes,'ButtonDownFcn',{@tImageAxes_Callback, proHandles, handles.xcat});

%#============#
%# Listeners  #
%#============#
addlistener(handles.xcat, 'loopIndex', 'PostSet', @(src, event) updateStatusBarText(src, event, proHandles, handles.xcat));

handles.hProcess = hProcess;

guidata(hProcess, proHandles);


function updateStatusBarText(src, event, proHandles, xcat)

set(proHandles.tStatusBar_Text, 'Units', 'characters');

statusBarStr = get(proHandles.tStatusBar_Text, 'String');
pos = get(proHandles.tStatusBar_Text,'Position');

w =  0.55*pos(3) - 5;

barLength = w*(xcat.loopIndex/xcat.loopEnd);
%fprintf('%f - %f / %f\n', xcat.loopIndex, barLength, w);

if xcat.loopIndex == 1
   set(proHandles.tStatusBar_Text, 'BackgroundColor', [1 1 1]);
   set(proHandles.tStatusBar_Text, 'ForegroundColor', [1 0 0]); 
end


str = sprintf('%s %.0f%%', repmat(hex2dec('2588'), 1, floor(barLength)), barLength*100/w);
set(proHandles.tStatusBar_Text, 'String', str);


if ceil(barLength) >= w
   bgColor = get(proHandles.tStatusBar, 'BackgroundColor');
   set(proHandles.tStatusBar_Text, 'BackgroundColor', bgColor);
   set(proHandles.tStatusBar_Text, 'ForegroundColor', [0 0 0]);
   set(proHandles.tStatusBar_Text, 'String', 'Ready');
end

drawnow

guidata(proHandles.tProcessWindow, proHandles);


function cursorMotion(hObject, ~, ~, xcat)
if isempty(xcat.getImage)
    return;
end

% UGet handles with current information
proHandles = guidata(hObject);


% Get position under mouse in axes data units i.e. pixels
img = xcat.getImageSlice(xcat.getSliceIndex);
point = get (proHandles.tImageAxes, 'CurrentPoint');
if point(1,1) < 0 || point(1,1) > size(img,2) || point(1,2) < 0 || point(1,2) > size(img,1)
    return;
end

currentImage = getimage(proHandles.tImageAxes);

px = ceil(point(1,1));
py = ceil(point(1,2));
if ~isempty(currentImage) 
    himgtext2 = findobj(proHandles.tImageAxes, 'Tag', 'tImageText_Position');
    if isempty(himgtext2)
        return;
    end
    if ishandle(himgtext2)
        if strcmpi(xcat.getImageView, 'transverse')
            %px = size(img,1) - ceil(point(1,1)) + 1;
            %py = ceil(point(1,2));
        elseif strcmpi(xcat.getImageView, 'sagittal')
            px = ceil(point(1,1));
            py = size(img,1) - ceil(point(1,2)) + 1;
        elseif strcmpi(xcat.getImageView, 'coronal')
            %px = size(img,2) - ceil(point(1,1)) + 1;
            %py = size(img,1) - ceil(point(1,2)) + 1;
        elseif strcmpi(xcat.getImageView, 'original')
            %px = ceil(point(1,1));
            %py = ceil(point(1,2));
        end
        
        pointStr = sprintf('x: %d\ny: %d\nI: %.5f', px, py, img(ceil(point(1,2)), ceil(point(1,1))));
        set(himgtext2, 'String', pointStr);
    end
end

guidata(hObject, proHandles);

function scrollWheelMotion(hObject, callbackdata, xcat)

proHandles = guidata(hObject);

if isempty(xcat.getImage)
    return;
end

%get(proHandles.tProcessWindow,'selectiontype')

img = xcat.getImage;
point = get (proHandles.tImageAxes, 'CurrentPoint');
if point(1,1) < 0 || point(1,1) > size(img,2) || point(1,2) < 0 || point(1,2) > size(img,1)
    return;
end


sliceIndex = xcat.getSliceIndex;
if isempty(sliceIndex)
    sliceIndex = get(proHandles.tImageSlider, 'Value');
end


sliceIndex = sliceIndex + sign(callbackdata.VerticalScrollCount);

if sliceIndex < 1; sliceIndex = 1; end
if sliceIndex > size(img,3); sliceIndex = size(img,3); end

xcat.setSliceIndex(sliceIndex);

%displayImage(handles, conHandles, xcat, sliceIndex);
displayImage(hObject, proHandles, xcat, sliceIndex);

guidata(hObject, proHandles);

%#==================#
%# File Menu Items  #
%#==================#
function tFileMenu_Quit_Callback(hObject, ~, proHandles, handles)
close(proHandles.tProcessWindow);
close(handles.tMainMenu);

rmpath('gui_figs');
rmpath('gui_functions');

function tFileMenu_MainReturn_Callback(hObject, ~, proHandles, handles)
close(proHandles.tProcessWindow);
set(handles.tMainMenu,'Visible','on');
handles.hProcess = [];
handles.xcat.reset;

function tFileMenu_ExportAnimation_Callback(hObject, callbackdata, proHandles, xcat)
xcat.exportAnimation();


function tFileMenu_ExportWorkspace_Callback(hObject, callbackdata, proHandles, xcat)
xcat.exportWorkspace;


function tFileMenu_ExportASCII_Callback(hObject, callbackdata, proHandles, xcat)
xcat.exportASCII();


function tFileMenu_ImportMatlab_Callback(hObject, callbackdata, proHandles, xcat)
xcat.importMatlabPhantom();
if isempty(xcat.getImage)
    return;
end

xcat.adjustImage(xcat.getImageView);
displayImage(hObject, proHandles, xcat, 1);

proHandles = guidata(hObject);


if isfield(proHandles, 'rectROI')
    proHandles = rmfield(proHandles, 'rectROI');
    proHandles = rmfield(proHandles, 'imgROI');
end

guidata(hObject, proHandles);


    
function tFileMenu_ImportWorkspace_Callback(hObject, callbackdata, proHandles, xcat)
xcat.importWorkspacePhantom;

if ~isempty(xcat.getImage)
    displayImage(hObject, proHandles, xcat, 1);
end





function tFileMenu_ImportPhantom_Callback(hObject, ~, proHandles, xcat)

xcat.importPhantom();
if isempty(xcat.getImage)
    return;
end
xcat.adjustView(xcat.getImageView);
displayImage(hObject, proHandles, xcat, 1);

proHandles = guidata(hObject);

if isfield(proHandles, 'rectROI')
    proHandles = rmfield(proHandles, 'rectROI');
    proHandles = rmfield(proHandles, 'imgROI');
end

guidata(hObject, proHandles);


%#************************#
%# Processing Menu Items  #
%#************************#
function tProcessMenu_CombineTumor_Callback(hObject, ~, proHandles, xcat)
try
    xcat.combineTumor;
catch err
    keyboard
end


function tProcessMenu_CenterTumor_Callback(hObject, callbackdata, proHandles, xcat)
xcat.centerTumor;


function tProcessMenu_CompareImages_Callback(hObject, ~, xcat)
proHandles = guidata(hObject);
xcat.compareImages;
xcat.adjustView(xcat.getImageView);
displayImage(hObject, proHandles, xcat, 1);
guidata(hObject, proHandles);


function tProcessMenu_ConvertHU_Callback(hObject, callbackdata, proHandles, xcat)
xcat.convertHU;
displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);


function tProcessMenu_ConvertDICOM_Callback(hObject, callbackdata, proHandles, xcat)
xcat.convertDICOM;


function tProcessMenu_ConvertAIP_Callback(hObject, callbackdata, proHandles, xcat)
xcat.createAIP;


function tProcessMenu_ConvertMIP_Callback(hObject, callbackdata, proHandles, xcat)
xcat.createMIP;


%#******************#
%# View Menu Items  #
%#******************#
function tViewMenu_Coronal_Callback(hObject, callbackdata, proHandles, xcat)

if ~strcmpi(xcat.getImageViewOriginal, 'Transverse');
    return;
end

xcat.setCurrentView('Coronal');

xcat.getImageOriginal;
xcat.adjustView(xcat.getImageView);

displayImage(hObject, proHandles, xcat, 1);


proHandles = guidata(hObject);
guidata(hObject, proHandles);


function tViewMenu_Sagittal_Callback(hObject, callbackdata, proHandles, xcat)

if ~strcmpi(xcat.getImageViewOriginal, 'Transverse');
    return;
end

xcat.setCurrentView('Sagittal');

xcat.getImageOriginal;
xcat.adjustView(xcat.getImageView);
displayImage(hObject, proHandles, xcat, 1);


proHandles = guidata(hObject);
guidata(hObject, proHandles);


function tViewMenu_Transverse_Callback(hObject, callbackdata, proHandles, xcat)

if ~strcmpi(xcat.getImageViewOriginal, 'Transverse');
    return;
end

xcat.setCurrentView('Transverse');

xcat.getImageOriginal;
xcat.adjustView(xcat.getImageView);

displayImage(hObject, proHandles, xcat, 1);

proHandles = guidata(hObject);
guidata(hObject, proHandles);

function tViewMenu_Original_Callback(hObject, ~, ~, xcat)

proHandles = guidata(hObject);

if ~strcmpi(xcat.getImageViewOriginal, 'Transverse');
    return;
end

xcat.setCurrentView('Original');

xcat.getImageOriginal;
xcat.adjustView(xcat.getImageView);

displayImage(hObject, proHandles, xcat, 1);

guidata(hObject, proHandles);

function tSelectSlice_Callback(hObject, ~, proHandles, xcat)

proHandles = guidata(hObject);

currentSlice = floor(get(proHandles.tImageSlider, 'Value'));

xcat.createAnimation(currentSlice, xcat.getImageView);

displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);
%displayImage(hObject, proHandles, xcat, sliceIndx, imgROI)

guidata(hObject, proHandles);


function tImageSlider_Callback(hObject, ~, proHandles, xcat)

indx = floor(get(hObject, 'Value'));

displayImage(hObject, proHandles, xcat, indx);

proHandles = guidata(hObject);

xcat.setSliceIndex(indx);

guidata(hObject, proHandles);


%#==================#
%# Image Callbacks  #
%#==================#
function tImage_Callback(hObject, ~, proHandles, xcat)

selection = get(proHandles.tProcessWindow, 'SelectionType');
% % Check if double click 
% persistent chk
% if isempty(chk)
%       chk = 1;
%       pause(0.5); %Add a delay to distinguish single click from a double click
%       if chk > 1
%           uiresume(proHandles.tPorcessWindow);
%           chk = [];
%           return;
%       end
% end

if strcmpi(get(proHandles.tProcessWindow,'selectiontype'), 'alt')
    
    set(proHandles.tProcessWindow, 'Units', 'pixels');
    pos  = get (proHandles.tProcessWindow, 'CurrentPoint');
    set(proHandles.tProcessWindow, 'Units', 'normalized');
    
    set(proHandles.tImageContextMenu, 'Position', [pos(1) pos(2)]);
    set(proHandles.tImageContextMenu, 'Visible', 'on');
end

%#=============#
%# Image Scale #
%#=============#
function tImageContextMenu_Aspect_Callback(hObject, callbackdata, proHandles, xcat)


function tImageContextMenu_Aspect_Normal_Callback(hObject, callbackdata, proHandles, xcat)
xcat.setImageScale('normal');
displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);


function tImageContextMenu_Aspect_Square_Callback(hObject, callbackdata, proHandles, xcat)
xcat.setImageScale('square');
displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);


function tImageContextMenu_Aspect_Equal_Callback(hObject, callbackdata, proHandles, xcat)
xcat.setImageScale('equal');
displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);


function tImageContextMenu_CreateROI_Callback(hObject, ~, xcat)

% Get Handles
proHandles = guidata(hObject);

% Get current image slice
img = xcat.getImageSlice(xcat.currentSliceIndex);

if ~isfield(proHandles, 'imgROI')
    imgROI  = impoly(proHandles.tImageAxes);
    roiMask = imgROI.createMask;
    roiMask = imclearborder(roiMask, 4);
    img_    = img.*roiMask;
    roiMask = img_ >= 0.5*max(max(img_));
    
    roiMask_outline = edge(roiMask, 'canny');
    [x,y] = find(roiMask_outline == 1);
    roiPos = [y, x];

    %keyboard
    
    proHandles.imgROI.roiMask  = imgROI.createMask;
    %proHandles.imgROI.roiPos   = imgROI.getPosition;
    proHandles.imgROI.roiPos   = roiPos;
    proHandles.imgROI.roiColor = imgROI.getColor;
    
    %roiPos = proHandles.imgROI.roiPos;
    
    delete(imgROI);
else
    roiMask = proHandles.imgROI.roiMask;
    roiMask = imclearborder(roiMask, 4);
    img_    = img.*roiMask;
    roiMask = img_ >= 0.5*max(max(img_));    
    
    % Create the new position of the mask
    roiMask_outline = edge(roiMask, 'canny');
    [x,y] = find(roiMask_outline == 1);
    roiPos = [y, x];
end

% Create ROI information
roiStats = regionprops(roiMask, img, 'Area', 'Centroid', 'Perimeter', 'PixelValues', 'MaxIntensity', 'MinIntensity', 'MeanIntensity');
if isempty(roiStats)
    uiwait(errordlg('No stats are found under ROI'));
    return;
end

% Display the ROI outline
roiColor = proHandles.imgROI.roiColor;

% sort rows of roi pos
% keyboard
% d = sqrt(roiPos(:,1).^2 + roiPos(:,2).^2);
% 
% 
% for k=1:length(roiPos)-1
%     line([roiPos(k,1) roiPos(k+1,1)], [roiPos(k,2) roiPos(k+1,2)], 'Color', roiColor);
% end
% line([roiPos(end,1) roiPos(1,1)], [roiPos(end,2) roiPos(1,2)], 'Color', roiColor);


% Create variables 
voxelVolume = prod(xcat.getVoxelDimensions);
% roiVolume   = sum(sum(roiMask*voxelVolume));
try
    roiInfo = {xcat.currentSliceIndex, roiStats.Area, roiStats.Centroid, roiStats.Perimeter, voxelVolume, voxelVolume*roiStats.Area, roiStats.PixelValues, roiStats.MaxIntensity, roiStats.MinIntensity, roiStats.MeanIntensity, roiMask, xcat.getImageView};
catch err
    if numel(roiStats) ~= 1
        [~,i] = max([roiStats.Area]);
        roiStats = roiStats(i);
        roiInfo = {xcat.currentSliceIndex, roiStats.Area, roiStats.Centroid, roiStats.Perimeter, voxelVolume, voxelVolume*roiStats.Area, roiStats.PixelValues, roiStats.MaxIntensity, roiStats.MinIntensity, roiStats.MeanIntensity, roiMask, xcat.getImageView};
    end    
end
if isfield(proHandles.imgROI, 'roiInfo')
    % Check if ROI calculation already stored
    indx = find([proHandles.imgROI.roiInfo{:,1}] == xcat.currentSliceIndex);
    if isempty(indx)
        proHandles.imgROI.roiInfo = [proHandles.imgROI.roiInfo; roiInfo];
    else
        proHandles.imgROI.roiInfo(indx,:) = roiInfo; 
    end
else
    proHandles.imgROI.roiInfo = roiInfo;
end


hold(proHandles.tImageAxes, 'on');
plot(roiPos(:,1), roiPos(:,2), 'o', 'Color', roiColor, 'MarkerSize', 3, 'MarkerFaceColor', roiColor, 'MarkerEdgeColor', roiColor);
hold(proHandles.tImageAxes, 'off');

guidata(hObject, proHandles);


function tImageContextMenu_ModifyROI_Callback(hObject, ~, xcat)

% Get Handles
proHandles = guidata(hObject);

% Get current image slice
img = xcat.getImageSlice(xcat.currentSliceIndex);

if ~isfield(proHandles, 'imgROI')
    uiwait(errordlg('No ROI found. Create a ROI first.'));
    return;
end

imgROI = impoly(proHandles.tImageAxes, proHandles.imgROI.roiPos);
waitfor(proHandles.tProcessWindow, 'SelectionType', 'Open');

roiMask = imgROI.createMask();
roiMask = imclearborder(roiMask, 4);
img_    = img.*roiMask;
roiMask = img_ >= 0.5*max(max(img_));

proHandles.imgROI.roiMask  = imgROI.createMask;
proHandles.imgROI.roiPos   = imgROI.getPosition;
proHandles.imgROI.roiColor = imgROI.getColor;

roiPos   = imgROI.getPosition;

% Create ROI information
roiStats = regionprops(roiMask, img, 'Area', 'Centroid', 'Perimeter', 'PixelValues', 'MaxIntensity', 'MinIntensity', 'MeanIntensity');
if isempty(roiStats)
    uiwait(errordlg('No are found under ROI'));
    return;
end

% Display the ROI outline
roiColor = proHandles.imgROI.roiColor;

hold(proHandles.tImageAxes, 'on');
plot(roiPos(:,1), roiPos(:,2), 'o', 'Color', roiColor, 'MarkerSize', 3, 'MarkerFaceColor', roiColor, 'MarkerEdgeColor', roiColor);
hold(proHandles.tImageAxes, 'off');

% Create variables 
voxelVolume = prod(xcat.getVoxelDimensions);
% roiVolume   = sum(sum(roiMask*voxelVolume));

%roiInfo = {xcat.currentSliceIndex, roiStats.Area, roiStats.Centroid, roiStats.Perimeter, voxelVolume, voxelVolume*roiStats.Area, roiStats.PixelValues, roiStats.MaxIntensity, roiStats.MinIntensity, roiStats.MeanIntensity, roiMask, xcat.getImageView};
try
    roiInfo = {xcat.currentSliceIndex, roiStats.Area, roiStats.Centroid, roiStats.Perimeter, voxelVolume, voxelVolume*roiStats.Area, roiStats.PixelValues, roiStats.MaxIntensity, roiStats.MinIntensity, roiStats.MeanIntensity, roiMask, xcat.getImageView};
catch err
    if numel(roiStats) ~= 1
        [~,i] = max([roiStats.Area]);
        roiStats = roiStats(i);
        roiInfo = {xcat.currentSliceIndex, roiStats.Area, roiStats.Centroid, roiStats.Perimeter, voxelVolume, voxelVolume*roiStats.Area, roiStats.PixelValues, roiStats.MaxIntensity, roiStats.MinIntensity, roiStats.MeanIntensity, roiMask, xcat.getImageView};
    end    
end
if isfield(proHandles.imgROI, 'roiInfo')
    % Check if ROI calculation already stored
    indx = find([proHandles.imgROI.roiInfo{:,1}] == xcat.currentSliceIndex);
    if isempty(indx)
        proHandles.imgROI.roiInfo = [proHandles.imgROI.roiInfo; roiInfo];
    else
        proHandles.imgROI.roiInfo(indx,:) = roiInfo; 
    end
else
    proHandles.imgROI.roiInfo = roiInfo;
end


guidata(hObject, proHandles);


function tImageContextMenu_DeleteROI_Callback(hObject, ~, ~, xcat)
proHandles = guidata(hObject);
if isfield(proHandles, 'imgROI')
    proHandles = rmfield(proHandles, 'imgROI');
    
    fprintf('ROI erased\n\n');
    
    img = xcat.getImageSlice(xcat.currentSliceIndex);
    displayImage(hObject, proHandles, xcat, xcat.currentSliceIndex, img);
end
guidata(hObject, proHandles);


function tImageContextMenu_InfoROI_Callback(hObject, ~, xcat)
proHandles = guidata(hObject);

if ~isfield(proHandles, 'imgROI')
    return;
end

% Sort imgROI
[~, i] = sort([proHandles.imgROI.roiInfo{:,1}]);
roiInfo = proHandles.imgROI.roiInfo(i,:);

fprintf('\tSlice\tPixels\tCentroid\tVolume(cm3)\tView\n');

for k=1:size(roiInfo,1)
    fprintf('\t%d\t%d\t(%3.1f %3.1f)\t%.3f\t\t%s\n', roiInfo{k,1}, roiInfo{k,2}, roiInfo{k,3}, roiInfo{k,6}, roiInfo{k, 12});
end
fprintf('\n\tTotal volume: %f cm3\n\n', sum([roiInfo{:,6}]));

roifname = sprintf('ROIinfo_%d.mat', randi(100));
N = 100;
k = 1;
while exist(roifname, 'file')
    roifname = sprintf('ROIinfo_%d.mat', randi(N));
    k = k + 1;
    if k>=100; N = N+100; end
end
clear N k;
    
save(roifname, 'roiInfo');

guidata(hObject, proHandles);


function tImageContextMenu_ImageType_GrayScale_Callback(hObject, ~, xcat)
proHandles = guidata(hObject);
proHandles.imageType = 'grayscale';

guidata(hObject, proHandles);
displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);


function tImageContextMenu_ImageType_Color_Jet_Callback(hObject, ~, xcat)
proHandles = guidata(hObject);
proHandles.imageType = 'color';
proHandles.colormap  = 'jet';

guidata(hObject, proHandles);
displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);


function tImageContextMenu_ImageType_Color_Hot_Callback(hObject, ~, xcat)
proHandles = guidata(hObject);
proHandles.imageType = 'color';
proHandles.colormap  = 'hot';

guidata(hObject, proHandles);
displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);


function tImageContextMenu_ImageType_Color_Gray_Callback(hObject, ~, xcat)
proHandles = guidata(hObject);
proHandles.imageType = 'color';
proHandles.colormap  = 'gray';

guidata(hObject, proHandles);
displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);


function tImageContextMenu_ImageType_Color_Bone_Callback(hObject, ~, xcat)
proHandles = guidata(hObject);
proHandles.imageType = 'color';
proHandles.colormap = 'bone';

guidata(hObject, proHandles);
displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);


function tImageContextMenu_ImageType_PixelValues_Default_Callback(hObject, ~, xcat)

proHandles = guidata(hObject);
xcat.img = xcat.imgOriginal;
xcat.adjustView(xcat.getImageView);
displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);

guidata(hObject, proHandles);


function tImageContextMenu_ImageType_PixelValues_Absolute_Callback(hObject, ~, xcat)

proHandles = guidata(hObject);
img  = xcat.getImage();

img = abs(img);
xcat.img = img;

displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);
guidata(hObject, proHandles);


function tImageContextMenu_ImageType_PixelValues_Squared_Callback(hObject, ~, xcat)

proHandles = guidata(hObject);
img  = xcat.getImage();

img = img.^2;
xcat.img = img;

displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);

guidata(hObject, proHandles);


function tImageContextMenu_ImageType_PixelValues_Normalized_Callback(hObject, ~, xcat)

proHandles = guidata(hObject);
img  = xcat.getImage();

img = img./max(max(max(img)));
xcat.img = img;

displayImage(hObject, proHandles, xcat, xcat.getSliceIndex);

guidata(hObject, proHandles);



function displayImage(hObject, proHandles, xcat, sliceIndx, imgROI)
if nargin < 5
    imgROI = [];
end

img  = xcat.getImage();
Imax = max(max(max(img)));
Imin = min(min(min(img)));


if sliceIndx > size(img,3); sliceIndx = 1; end
if isempty(sliceIndx); sliceIndx = 1; end

proHandles = guidata(hObject);

if ~isfield(proHandles, 'imageType')
    keyboard;
end

if ~isempty(imgROI)
    
    if strcmpi(proHandles.imageType, 'grayscale')
        if Imin == Imax
            himg = imshow(imgROI, 'Parent', proHandles.tImageAxes, 'DisplayRange', []);
        else
            himg = imshow(imgROI, 'Parent', proHandles.tImageAxes, 'DisplayRange', [Imin Imax]);
        end
    else
        himg = imagesc(imgROI, 'Parent', proHandles.tImageAxes);
        colormap(proHandles.tImageAxes, proHandles.colormap);
        colorbar;
    end
else
    if strcmpi(proHandles.imageType, 'grayscale')
        if Imin == Imax
            himg = imshow(img(:,:,sliceIndx), 'Parent', proHandles.tImageAxes, 'DisplayRange', []);
        else
            try
                himg = imshow(img(:,:,sliceIndx), 'Parent', proHandles.tImageAxes, 'DisplayRange', [Imin Imax]);
            catch err
                keyboard
            end
        end
    else
        himg = imagesc(img(:,:,sliceIndx), 'Parent', proHandles.tImageAxes);
        colormap(proHandles.tImageAxes, proHandles.colormap);
        colorbar;
    end
end

xcat.setSliceIndex(sliceIndx);
axis(xcat.getImageScale);
setupSlider(proHandles, img, sliceIndx);

displayImageText(hObject, proHandles, xcat, sliceIndx);

%proHandles = guidata(hObject);


set(himg,'ButtonDownFcn',{@tImage_Callback, proHandles, xcat});

guidata(hObject, proHandles);


function setupSlider(proHandles, img, sliceIndx)

if sliceIndx == 1
    set(proHandles.tImageSlider, 'Max'  , size(img,3));
    set(proHandles.tImageSlider, 'Value', 1);
    set(proHandles.tImageSlider, 'Min'  , 1);
    
    set(proHandles.tImageSliceCurrent, 'String', num2str(1));
    set(proHandles.tImageSliceEnd    , 'String', num2str(size(img,3)));
    
    set(proHandles.tImageSlider, 'SliderStep', [1/(size(img,3)-1) 1/(size(img,3)-1)]);
    
else
    set(proHandles.tImageSlider, 'Value', sliceIndx);
    set(proHandles.tImageSliceCurrent, 'String', num2str(sliceIndx));
end


function displayImageText(hObject, proHandles, xcat, sliceIndx)
img = xcat.getImageSlice(sliceIndx);

fontSize = 8;
if ismac
    fontSize = 10;
end
textColor = [1 1 1];


sliceMax = max(max(xcat.getImageSlice(xcat.currentSliceIndex)));
sliceMin = min(min(xcat.getImageSlice(xcat.currentSliceIndex)));

imgStr    = sprintf('Max: %.4f\nMin: %.4f', sliceMax, sliceMin);
text(0.016*size(img,2), 0.05*size(img,1), imgStr, 'Parent', proHandles.tImageAxes, 'FontName', 'FixedWidth', 'FontSize', fontSize+1, 'Color', textColor);

pointStr  = sprintf('x: %d\ny: %d\nI: %.5f',[] , [], []);
text(0.016*size(img,2), 0.17*size(img,1), pointStr, 'FontName', 'FixedWidth', 'FontSize', fontSize, 'Color', textColor, 'Tag', 'tImageText_Position');

viewStr   = sprintf('%s', xcat.getImageView);
text(0.8594*size(img,2), 0.025*size(img,1), viewStr, 'FontName', 'FixedWidth', 'FontSize', fontSize+1, 'Color', textColor);

nameStr = sprintf('%s', xcat.getPhantomFileName);
text(0.8594*size(img,2), 0.060*size(img,1), nameStr, 'FontName', 'FixedWidth', 'FontSize', fontSize+1, 'Color', textColor);

if ~isempty(xcat.nrmse)
    nrmseStr = sprintf('NRMSE: %.3f', xcat.nrmse);
    text(0.8594*size(img,2), 0.120*size(img,1), nrmseStr, 'FontName', 'FixedWidth', 'FontSize', fontSize+1, 'Color', textColor);
end

guidata(hObject, proHandles)
