function [phaseIndx] = FindPhaseIndxOld(ap, cineTime, cineInterval, bins)
%respCycle = 4;

[peakInh, peakInhIndx] = findpeaks( ap(:,2), 'MinPeakDistance', floor(0.8*cineTime/min(diff(ap(:,1)))), 'MinPeakHeight', 9);
%[peakExh, peakExhIndx] = findpeaks(-ap(:,2), 'MinPeakDistance', floor(cineTime/min(diff(ap(:,1)))));
%peakExh = -peakExh;

if isempty(peakInhIndx)
    [peakInh, peakInhIndx] = findpeaks( ap(:,2), 'MinPeakDistance', floor(0.8*cineTime/min(diff(ap(:,1)))));
end

if isempty(peakInhIndx)
    if ap(1,2) >= 9.5
        peakInh     = ap(1,2);
        peakInhIndx = 1;
    end
end

if length(peakInhIndx) > 1
    [~, i] = max(peakInh);
    peakInh = peakInh(i);
    peakInhIndx = peakInhIndx(i);
end

try
    ap_(:,1) = ap(:,1) - ap(peakInhIndx,1);
catch err
    errStr = sprintf('An unexpected error occured during parameter file generation.\nPeak inhale was not properly identified in the signal\nThe appliction will now close');
    herr = errordlg(errStr);
    %close(gcf);
    waitfor(herr);
    keyboard
end

% Image sampling times
%cineInterval = 0.5;
t(:,1) = ap_(1,1):cineInterval:ap_(end,1);

% Find amplitude at sampling points
a(:,1) = interp1(ap_(:,1), ap(:,2), t);

% Time bins
%bins = 10;
t_b(:,1) = linspace(0, cineTime, bins);

% Find phase indx
phaseIndx = [];
for j=1:numel(t)
    phaseIndx(j,1) = floor(bins*(t(j,1) - t_b(1,1))/(t_b(end,1) - t_b(1,1)));
end

%phaseIndx(phaseIndx < 0) = bins + phaseIndx(phaseIndx < 0 );