function DisplayPlot(simHandles)

dataIndx = get(simHandles.tSelectData, 'Value');
dataName = simHandles.dataNames{dataIndx};
%dataIndx = simHandles.dataNames{dataIndx, 3};

t_indx = simHandles.t_indx;

axes(simHandles.tRespSignalAxes);
plot(simHandles.data(:,t_indx), simHandles.data(:, dataIndx+1), 'LineWidth', 2, 'Parent', simHandles.tRespSignalAxes);
title(sprintf('%s', dataName), 'Parent', simHandles.tRespSignalAxes, 'FontWeight', 'bold');
set(simHandles.tRespSignalAxes, 'LineWidth', 2, 'FontSize', 12);
xlabel('time (s)'    , 'FontWeight', 'Bold', 'Parent', simHandles.tRespSignalAxes);
ylabel('Displacement', 'FontWeight', 'Bold', 'Parent', simHandles.tRespSignalAxes);

line(xlim, [0 0], 'LineStyle', '--', 'Color', [0.6 0.6 0.6], 'Parent', simHandles.tRespSignalAxes);


if strcmpi(dataName, 'Surrogate')
    xl = xlim;
    yl = ylim;
    
    %dt = min(diff(simHandles.data(:,t_indx)));
    
    %[peakInhale, peakInhaleIndx ] = findpeaks( simHandles.data(:,dataIndx+1), 'MinPeakDistance', floor(2/dt)); %, 'MINPEAKHEIGHT', (min(simHandles.data(:,dataIndx))  + 0.45*range(simHandles.data(:,dataIndx))));
    %[peakExhale, peakExhaleIndx ] = findpeaks(-simHandles.data(:,dataIndx+1), 'MinPeakDistance', floor(2/dt)); %, 'MINPEAKHEIGHT', (min(-simHandles.data(:,dataIndx)) - 0.55*range(simHandles.data(:,dataIndx))));
    
    %peakExhale = -peakExhale;
    
    
    %hold(simHandles.tRespSignalAxes);
    %plot(simHandles.data(peakInhaleIndx, t_indx), peakInhale, 'or', 'MarkerFaceColor', [0.5 0.5 0.5]);
    %plot(simHandles.data(peakExhaleIndx, t_indx), peakExhale, 'or', 'MarkerFaceColor', [0.0 0.5 0.0]);
    %hold(simHandles.tRespSignalAxes);
    
    %breathCycles = diff(simHandles.data(peakInhaleIndx, t_indx));
    
    %cycle_mean = mean(breathCycles);
    %cycle_std  = std(breathCycles);
    %cycle_max  = max(breathCycles);
    %cycle_min  = min(breathCycles);
    
    %n = min([numel(peakInhale) numel(peakExhale)]);
    
    
    displacement = range(simHandles.data(:,dataIndx+1));
    
    text(0.05*xl(2), 0.9*yl(2) , sprintf('Mean: %.2f%s%.2f s', simHandles.bCycles.cycle_mean, char(177), simHandles.bCycles.cycle_std), 'Parent', simHandles.tRespSignalAxes);
    text(0.35*xl(2), 0.9*yl(2) , sprintf('Min: %.2f s'       , simHandles.bCycles.cycle_min), 'Parent', simHandles.tRespSignalAxes);
    text(0.55*xl(2), 0.9*yl(2) , sprintf('Max: %.2f s'       , simHandles.bCycles.cycle_max), 'Parent', simHandles.tRespSignalAxes);
    
    text(0.75*xl(2), 0.9*yl(2) , sprintf('Range: %.2f mm', displacement), 'Parent', simHandles.tRespSignalAxes);
end

if strcmpi(dataName, 'Tumor SI')
    xl = xlim;
    yl = ylim;
    
    displacement = range(simHandles.data(:,dataIndx+1));
    text(0.75*xl(2), 0.9*yl(2) , sprintf('Range: %.2f mm', displacement), 'Parent', simHandles.tRespSignalAxes);
end