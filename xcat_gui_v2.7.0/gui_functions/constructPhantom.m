function constructPhantom(handles)
mainPos = get(handles.tMainMenu, 'Position');
mainPos(1) = 10;

% Hide Main Menu
set(handles.tMainMenu,'Visible','off');

% Display this message while loading
hMsg = msgbox('Please wait....');

hConstruct = hgload(fullfile('gui_figs','constructPhantom.fig'));
conPos     = get(hConstruct, 'Position');
set(hConstruct, 'Position', [mainPos(1)+mainPos(3) conPos(2) conPos(3) conPos(4)]);
% set(hConstruct, 'Position', [mainPos(1)+mainPos(3)+40 conPos(2) conPos(3)+40 conPos(4)]);

conHandles = guihandles(hConstruct);


try
    img = handles.xcat.activityImage();
catch err
    if strfind(err.identifier, 'nomem')
%         img = handles.xcat.activityImage([256 256 800]);
        img = handles.xcat.activityImage([256 256 1000]);
    end
    err
end
handles.xcat.setImage(img);
handles.xcat.setImageOriginal(img);
handles.xcat.setVoxelDimensionsOriginal([size(img,1) size(img,2) size(img,3)]);

handles.xcat.voxelDims          = [0.2 0.2 0.2];
handles.xcat.voxelArray         = [256 1000 256];
handles.xcat.voxelDimsOriginal  = [0.2 0.2 0.2];
handles.xcat.voxelArrayOriginal = [256 256 1000];
handles.xcat.currentSliceIndex  = 128;
handles.xcat.pixelWidth         = 0.2;
handles.xcat.sliceWidth         = 0.2;
handles.xcat.startSlice         = 10;
handles.xcat.endSlice           = 990;
handles.xcat.subvoxelIndx       = 1;

% Delete the message box
delete(hMsg);

% Add handles for slice selector and associated text
conHandles.hl1     = [];
conHandles.hl2     = [];
conHandles.hltext1 = [];
conHandles.hltext2 = [];

conHandles.hl1XData    = [10 10];
conHandles.hl1YData    = [1 256];
conHandles.hl2XData    = [990 990];
conHandles.hl2YData    = [1 256];
conHandles.hl1Selected = 0;
conHandles.hl2Selected = 0;


% Add handles for parameter table
conHandles.hParameterTable = [];

% Add handles to organ tooltip
conHandles.hOrganText = [];

% Add recentfolder
conHandles.recentFolder = '.';

%#===========#
%# Callbacks #
%#===========#
set(conHandles.tConstructWindow, 'WindowButtonMotionFcn', {@cursorMotion     , handles.xcat});
set(conHandles.tConstructWindow, 'WindowScrollWheelFcn' , {@scrollWheelMotion, handles.xcat});
set(conHandles.tConstructWindow, 'WindowButtonUpFcn'    , {@buttonUp         , handles.xcat});


set(conHandles.tFileMenu_Quit               , 'Callback', {@tFileMenu_Quit_Callback               , handles}     );
set(conHandles.tFileMenu_MainReturn         , 'Callback', {@tFileMenu_MainReturn_Callback         , handles}     );
set(conHandles.tFileMenu_ReadParFile        , 'Callback', {@tFileMenu_ReadParFile_Callback        , handles.xcat});
set(conHandles.tFileMenu_ExportParFile      , 'Callback', {@tFileMenu_ExportParFile_Callback      , handles.xcat});
set(conHandles.tFileMenu_GeneratePhantom    , 'Callback', {@tFileMenu_GeneratePhantom_Callback    , handles}     );

set(conHandles.tFileMenu_ImportStructureSet , 'Callback', {@tFileMenu_ImportStructureSet_Callback , handles.xcat});

set(conHandles.tParametersButton, 'Callback', {@tParametersButton_Callback, handles.xcat});

set(conHandles.tImageSlider     , 'Callback', {@tImageSlider_Callback     , handles.xcat});
set(conHandles.tImageContextMenu, 'Callback', {@tImageContextMenu_Callback, handles.xcat});

% Update the handles
guidata(hConstruct, conHandles);

displayImage(conHandles.tImageAxes, handles.xcat, 128);

function cursorMotion(hObject, ~, xcat)
if isempty(xcat.getImage)
    return;
end

% Update function handles with current information
conHandles = guidata(hObject);



% Get position under mouse in axes data units i.e. pixels
img = xcat.getImageSlice(xcat.getSliceIndex);
point = get (conHandles.tImageAxes, 'CurrentPoint');
if point(1,1) < 0 || point(1,1) > size(img,2) || point(1,2) < 0 || point(1,2) > size(img,1)
    return;
end

hLineStart = findobj('Parent', conHandles.tImageAxes, 'Tag', 'tSliceStartLine');
hLineEnd   = findobj('Parent', conHandles.tImageAxes, 'Tag', 'tSliceEndLine');

startLineXData = get(hLineStart, 'XData');
endLineXData   = get(hLineEnd  , 'XData');


if floor(point(1)) == startLineXData(1) | floor(point(1)) == startLineXData(1)-1 | floor(point(1)) == startLineXData(1)+1 | floor(point(1)) == endLineXData(1) | floor(point(1)) == endLineXData(1)-1 | floor(point(1)) == endLineXData(1)+1
    setptr(conHandles.tConstructWindow, 'hand');
end

% Show current position
pos    = point(1,:);
pos(3) = get(conHandles.tImageSlider, 'Value');
hText = findobj('Parent', conHandles.tImageAxes, 'Tag', 'tPosText');
set(hText, 'String', sprintf('x: %.0f y: %.0f z: %.0f', ceil(pos(2)), ceil(pos(3)), ceil(pos(1))), 'color', [1 1 1]);

% Check if select lines have been selected
if conHandles.hl1Selected | conHandles.hl2Selected
    setptr(conHandles.tConstructWindow, 'closedhand');
    if conHandles.hl1Selected
        if (conHandles.hl2XData(1) - ceil(point(1,1)) < 1)
            conHandles.hl1XData(1) = conHandles.hl2XData(1) - 1;
            conHandles.hl1XData(2) = conHandles.hl2XData(2) - 1;
            point(1) = conHandles.hl1XData(1);
        end
        
        % Move line to new position
        
        set(hLineStart, 'XData', [ceil(point(1)) ceil(point(1))]);
        
        % Move text to new position and change the displayed slice index
        hLineStartText = findobj('Parent', conHandles.tImageAxes, 'Tag', 'tSliceStartLineText');
        textStr1 = sprintf('Start:\n%.0f', ceil(conHandles.hl1XData(1)));
        
        set(hLineStartText, 'Position', [ceil(point(1)) -15]);
        set(hLineStartText, 'String'  , textStr1            );
        
        % Update slide position data
        conHandles.hl1XData = get(hLineStart, 'XData');
        conHandles.hl1YData = get(hLineStart, 'YData');
                
        % Assign start and end slice to parameter data
        xcat.setParameterValue('startslice', ceil(conHandles.hl1XData(1)));
        
        guidata(hObject, conHandles);
        
    elseif conHandles.hl2Selected
        if (ceil(point(1,1)) - conHandles.hl1XData(1) < 1)
            conHandles.hl2XData(1) = conHandles.hl1XData(1) + 1;
            conHandles.hl2XData(2) = conHandles.hl1XData(2) + 1;
            point(1) = conHandles.hl2XData(1);
        end
        
        % Move line to new position
        
        set(hLineEnd, 'XData', [ceil(point(1)) ceil(point(1))]);
        
        % Move text to new position and change the displayed slice index
        hLineEndText = findobj('Parent', conHandles.tImageAxes, 'Tag', 'tSliceEndLineText');
        textStr2 = sprintf('End:\n%.0f', ceil(conHandles.hl2XData(1)));
        set(hLineEndText, 'Position', [ceil(point(1)) -15]);
        set(hLineEndText, 'String', textStr2);
        
        % Update slide position data
        conHandles.hl2XData = get(hLineEnd, 'XData');
        conHandles.hl2YData = get(hLineEnd, 'YData');
        
        % Assign start and end slice to parameter data
        xcat.setParameterValue('endslice', ceil(conHandles.hl2XData(1)));
       
        guidata(hObject, conHandles);
        
    end
    
else
    % Display coordinates
    
    % Display organ information
    organ = [];
    indx  = img(ceil(point(1,2)), ceil(point(1,1)));
    if indx == 0
%         organ = xcat.getOrgan(indx);
        if ishandle(conHandles.hOrganText)
            set(conHandles.hOrganText, 'Visible', 'off');
        end
        %organ = {'air'};
    else
        organ = xcat.getOrgan(indx);
%         organStr = sprintf('MatID: %.1f %s\n', indx, organ{:});
%         organStr = sprintf('MatID: %d | %s \n', indx, organ{:})
        organStr = sprintf('%s\n', organ{:});
%         organStr = sprintf('%s MatID: %d \n', organ{:}, indx)
%         organStr = sprintf('MatID: %.1f %s\n', indx, organ{(0:end),(0:end)});
%         organStr = sprintf('MatID: %.1f \t %s\n', indx, organ{:});
% organStr
        if isempty(conHandles.hOrganText) || (~ishandle(conHandles.hOrganText))
%             organStr
            conHandles.hOrganText = text(1.1*point(1,1), 1.1*point(1,2), organStr,  'BackgroundColor', [1 1 0.8], 'Interpreter', 'none', 'FontName', 'FixedWidth', 'FontSize', 12);
%             conHandles.hOrganText = text(1.1*point(1,1), 1.1*point(1,2), organStr,  'BackgroundColor', [1 1 0.8], 'Interpreter', 'none', 'FontName', 'FontSize', 12);
%             conHandles.hOrganText = text(1.1*point(1,1), 1.1*point(1,2), organStr,  'BackgroundColor', [0.2 0.2 0.8], 'Interpreter', 'none', 'FontName', 'FixedWidth', 'FontSize', 12);
        else
%             conHandles.hOrganText = text(1.1*point(1,1), 1.1*point(1,2), organStr,  'BackgroundColor', [1 1 0.8], 'Interpreter', 'none', 'FontName', 'FixedWidth', 'FontSize', 12);
%             organStr = sprintf('MatID: %.1f %s\n', indx, organ{:});
            set(conHandles.hOrganText, 'Visible', 'on');
            set(conHandles.hOrganText, 'Position', [1.1*point(1,1) 1.1*point(1,2)]);
            set(conHandles.hOrganText, 'String', organStr);
%             set(conHandles.hOrganText, '%s', organStr);
%             set(conHandles.hOrganText, 'String', organStr, 'Interpreter', 'none', 'FontName', 'FixedWidth', 'FontSize', 12);
        end
    end
      
end

guidata(hObject, conHandles);

function setupSliceSelector(hObject)
% Update conHandles
conHandles = guidata(hObject);

% If left line does not exist - create it
hLineStart = findobj('Parent', conHandles.tImageAxes, 'Tag', 'tSliceStartLine');
if isempty(hLineStart)
    hLineStart  = line(conHandles.hl1XData , conHandles.hl1YData, 'Color', 'm', 'LineWidth', 3, 'Parent', conHandles.tImageAxes, 'Tag', 'tSliceStartLine', 'buttonDownFcn', {@lineSelect, conHandles});
    textStr1    = sprintf('Start:\n%.0f', ceil(conHandles.hl1XData(1)));
    hltext1     = text(conHandles.hl1XData(1), -15, textStr1, 'HorizontalAlignment', 'Right', 'Tag', 'tSliceStartLineText');
end

% If right line does not exist - create it
hLineEnd = findobj('Parent', conHandles.tImageAxes, 'Tag', 'tSliceEndLine');
if isempty(hLineEnd)
    hLineEnd = line(conHandles.hl2XData , conHandles.hl2YData, 'Color', 'm', 'LineWidth', 3, 'Parent', conHandles.tImageAxes, 'Tag', 'tSliceEndLine', 'buttonDownFcn', {@lineSelect, conHandles});
    textStr2 = sprintf('End:\n%.0f', ceil(conHandles.hl2XData(1)));
    hltext2  = text(conHandles.hl2XData(1), -15, textStr2, 'HorizontalAlignment', 'Left', 'Tag', 'tSliceEndLineText');
end

guidata(hObject, conHandles);



function lineSelect(hObject, ~, conHandles)

% [ crosshair | fullcrosshair | {arrow} | ibeam | watch | topl | topr | botl | botr | left | top | right | bottom | circle | cross | fleur | custom | hand ]
% with setptr : ‘hand’, ‘hand1′, ‘hand2′, ‘closedhand’, ‘glass’, ‘glassplus’, ‘glassminus’, ‘lrdrag’, ‘ldrag’, ‘rdrag’, ‘uddrag’, ‘udrag’, ‘ddrag’, ‘add’, ‘addzero’, ‘addpole’, ‘eraser’, ‘help’, ‘modifiedfleur’, ‘datacursor’, and ‘rotate’
tag = get(hObject, 'Tag');
if strcmpi(tag, 'tSliceStartLine') | strcmpi(tag, 'tSliceStartLineText')
    conHandles.hl1Selected = 1;
    conHandles.hl2Selected = 0;
elseif strcmpi(tag, 'tSliceEndLine') | strcmpi(tag, 'tSliceEndLineText')
    conHandles.hl1Selected = 0;
    conHandles.hl2Selected = 1;
end

guidata(hObject, conHandles);


function buttonUp(hObject, ~, xcat)

figureObjects = findall(hObject);
tagList       = get(figureObjects, 'Tag');

startLineIndx = cellfun(@(x) strcmpi(x, 'tSliceStartLine'), tagList);
endLineIndx   = cellfun(@(x) strcmpi(x, 'tSliceEndLine'  ), tagList);

hStartLine = get(figureObjects(startLineIndx));
hEndLine   = get(figureObjects(endLineIndx)  );

startLineXData = hStartLine.XData;
endLineXData   = hEndLine.XData;

conHandles = guidata(hObject);

img = xcat.getImageSlice(xcat.getSliceIndex);
point = get (conHandles.tImageAxes, 'CurrentPoint');
if point(1,1) < 0 || point(1,1) > size(img,2) || point(1,2) < 0 || point(1,2) > size(img,1)
    return;
end

conHandles.hl1XData = hStartLine.XData;
conHandles.hl2XData = hEndLine.XData;

conHandles.hl1Selected = 0;
conHandles.hl2Selected = 0;

setptr(conHandles.tConstructWindow, 'arrow');

guidata(hObject, conHandles);


function scrollWheelMotion(hObject, callbackdata, xcat)
if isempty(xcat.getImage)
    return;
end

conHandles = guidata(hObject);

img = xcat.getImage;
point = get (conHandles.tImageAxes, 'CurrentPoint');
if point(1,1) < 0 || point(1,1) > size(img,2) || point(1,2) < 0 || point(1,2) > size(img,1)
    return;
end


sliceIndex = xcat.getSliceIndex;
if isempty(sliceIndex)
    sliceIndex = get(conHandles.tConstructWindow_ImageSliceCurrent, 'Value');
end

sliceIndex = sliceIndex + callbackdata.VerticalScrollCount;

if sliceIndex < 1; sliceIndex = 1; end
if sliceIndex > size(img,3); sliceIndex = size(img,3); end

xcat.setSliceIndex(sliceIndex);

displayImage(hObject, xcat, sliceIndex);


function tParametersButton_Callback(hObject, ~, xcat)

conHandles = guidata(hObject);

paramTable = uisupertable(xcat.parameters(:,1:2), 'TooltipString', xcat.parameters(:,3));
uiwait(paramTable.tableFigHandle);

if isempty(paramTable.dataChanged)
    return;
else
    unique(paramTable.dataChanged)
end

try
    parameters(:,1:2) = paramTable.data;
    
    % Check if slice_width is changed and calculate new start & end slices
    % and tumor position
    slice_width_new = str2num(getParameterValue('slice_width', parameters));
    slice_width_old = str2num(xcat.getParameterValue('slice_width'));
    
    if slice_width_new ~= slice_width_old
        % Calculate new start and end slices
        arrayLength_old = xcat.imgLength/slice_width_old;         
        startslice_old  = str2num(xcat.getParameterValue('startslice'));
        endslice_old    = str2num(xcat.getParameterValue('endslice'  ));
        
        f_start = startslice_old/arrayLength_old;
        f_end   = endslice_old  /arrayLength_old;
        
        arrayLength_new = xcat.imgLength/slice_width_new;
        startslice_new  = floor(f_start*arrayLength_new);
        endslice_new    = floor(f_end  *arrayLength_new);
        
        parameters = setParameterValue('startslice', startslice_new, parameters);
        parameters = setParameterValue('endslice'  , endslice_new  , parameters);
        
        % Calculate new tumor position
        x_loc_old  = str2num(xcat.getParameterValue('x_location'));
        %y_loc_old  = str2num(xcat.getParameterValue('y_location'));
        %z_loc_old  = str2num(xcat.getParameterValue('z_location'));
        
        f_x_loc = x_loc_old/(endslice_old - startslice_old);
        %f_y_loc = y_loc_old/arrayLength_new;
        %f_z_loc = z_loc_old/arrayLength_new;
        
        x_loc_new = floor(f_x_loc*(endslice_new - startslice_new));        
        
        parameters = setParameterValue('x_location'  , x_loc_new  , parameters);
    end
    
    % Check if pixel width is changed
    pixel_width_new = str2num(     getParameterValue('pixel_width', parameters));
    pixel_width_old = str2num(xcat.getParameterValue('pixel_width'));
    
    if pixel_width_new ~= pixel_width_old
        y_loc_old  = str2num(xcat.getParameterValue('y_location'));
        z_loc_old  = str2num(xcat.getParameterValue('z_location'));
        
        arraySize_old = xcat.imageWidth/pixel_width_old;
        f_pixel = pixel_width_old/arraySize_old;
        
        arraySize_new = xcat.imageWidth/pixel_width_new;
        
        y_loc_new = floor(f_pixel*arraySize_new);
        z_loc_new = floor(f_pixel*arraySize_new);
        
        parameters = setParameterValue('y_location'  , y_loc_new  , parameters);
        parameters = setParameterValue('z_location'  , z_loc_new  , parameters);
    end
    
    xcat.parameters(:,1:2) = parameters(:,1:2);
    
    xcat.assignParameters;

catch err
    uiwait(errordlg('Something went wrong. Please re-check parameters table\n'));
    %keyboard
end

% Get new parameters

% Update image dimensions
arraySize = [xcat.imgWidth/xcat.pixelWidth xcat.imgWidth/xcat.pixelWidth xcat.imgLength/xcat.sliceWidth];
img = xcat.activityImage(arraySize);

% Update line selectors
conHandles.hl1XData = [xcat.startSlice xcat.startSlice];
conHandles.hl2XData = [xcat.endSlice   xcat.endSlice  ];

guidata(conHandles.tConstructWindow, conHandles);

displayImage(conHandles.tConstructWindow, xcat, floor(0.8*size(xcat.img,3)));

% Move line selectors
h1 = findobj('Tag', 'tSliceStartLine');
h2 = findobj('Tag', 'tSliceEndLine'  );

set(h1, 'XData', conHandles.hl1XData);
set(h2, 'XData', conHandles.hl2XData);

% Move associate text labels above the line selectors
ht1 = findobj('Tag', 'tSliceStartLineText');
ht2 = findobj('Tag', 'tSliceEndLineText'  );

textStr1 = sprintf('Start:\n%.0f', conHandles.hl1XData(1));
textStr2 = sprintf('End:\n%.0f'  , conHandles.hl2XData(1));

set(ht1, 'String', textStr1);
set(ht2, 'String', textStr2);

set(ht1, 'Position', [conHandles.hl1XData(1) -11]);
set(ht2, 'Position', [conHandles.hl2XData(1) -11]);

guidata(hObject, conHandles);


%#==================#
%# File Menu Items  #
%#==================#
function tFileMenu_Quit_Callback(hObject, ~, handles)

conHandles = guidata(hObject);

close(conHandles.tConstructWindow);
close(handles.tMainMenu);

%rmpath(fullfile('.','gui_figs'));
%rmpath(fullfile('.','gui_figs'));


function tFileMenu_MainReturn_Callback(hObject, ~, handles)

conHandles = guidata(hObject);

close(conHandles.tConstructWindow);

set(handles.tMainMenu,'Visible','on');

handles.hConstruct = [];

handles.xcat.reset;

function tFileMenu_ReadParFile_Callback(hObject, ~, xcat)

conHandles = guidata(hObject);

[fname, pname] = uigetfile('*.par','Select Parameter File', conHandles.recentFolder);
if ~fname
    return;
end

conHandles.recentFolder = pname;

xcat.readParametersFromFile(fullfile(pname, fname));
conHandles.hl1XData = [xcat.startSlice xcat.startSlice];
conHandles.hl2XData = [xcat.endSlice   xcat.endSlice  ];

% Update image dimensions
%xcat.adjustActivityImage;
newArray(1) = xcat.imgWidth /str2num(xcat.getParameterValue('pixel_width'));
newArray(2) = xcat.imgLength/str2num(xcat.getParameterValue('slice_width'));
newArray(3) = newArray(1);
xcat.img = xcat.scaleImage(xcat.img, newArray, 'coronal_rotated');

% Update Slice Bar Y Length
conHandles.hl1YData = [1 size(xcat.img,3)];
conHandles.hl2YData = [1 size(xcat.img,3)];

size(xcat.img)

guidata(conHandles.tConstructWindow, conHandles);

displayImage(conHandles.tConstructWindow, xcat, floor(0.5*size(xcat.img,3)));

% Move line selectors
h1 = findobj('Tag', 'tSliceStartLine');
h2 = findobj('Tag', 'tSliceEndLine'  );

set(h1, 'XData', conHandles.hl1XData);
set(h1, 'YData', conHandles.hl1YData);
set(h2, 'XData', conHandles.hl2XData);
set(h2, 'YData', conHandles.hl2YData);


% Move associate text labels above the line selectors
ht1 = findobj('Tag', 'tSliceStartLineText');
ht2 = findobj('Tag', 'tSliceEndLineText'  ); 

textStr1 = sprintf('Start:\n%.0f', conHandles.hl1XData(1));
textStr2 = sprintf('End:\n%.0f'  , conHandles.hl2XData(1));

set(ht1, 'String', textStr1);
set(ht2, 'String', textStr2);

set(ht1, 'Position', [conHandles.hl1XData(1) -20]);
set(ht2, 'Position', [conHandles.hl2XData(1) -20]);


guidata(hObject, conHandles);


function tFileMenu_ExportParFile_Callback(hObject, ~, xcat)

conHandles = guidata(hObject);
[fname, pname] = uiputfile('*.par','Save Phantom Parameter File', fullfile(conHandles.recentFolder, 'phantom.par'));
if isequal(fname,0) || isequal(pname,0)
    return;
end

params = xcat.parameters;

% Write phantom
writeParameterFile(fullfile(pname, fname), xcat, -1, -1);

% Write tumor
if xcat.includeTumor == 1
    [~, f, s] = fileparts(fullfile(pname, fname));
    f = strcat(f,'_tumor',s);
    writeParameterFile(fullfile(pname, f), xcat, 2, -1);
end

% Write plaque
if xcat.includeHeartPlaque == 1
    [~, f, s] = fileparts(fullfile(pname, fname));
    f = strcat(f,'_plq',s);
    writeParameterFile(fullfile(pname, f), xcat, 3, -1);
end

% Write Defect
if xcat.includeHeartDefect == 1
    [~, f, s] = fileparts(fullfile(pname, fname));
    f = strcat(f,'_dfc',s);
    writeParameterFile(fullfile(pname, f), xcat, 1, -1);
end

guidata(hObject, conHandles);


function tFileMenu_ImportStructureSet_Callback(hObject, ~, xcat)
conHandles = guidata(hObject);

xcat.importStructureSet();

guidata(hObject, conHandles);


function tFileMenu_GeneratePhantom_Callback(hObject, ~, handles)
% Get handles
conHandles = guidata(hObject);

% Store current dir
guiDir = pwd;
addpath(guiDir);

% Check for xcat executable
% exeFound = dir('xctools/dxcat2.exe');
exeFound = [];
if isempty(exeFound)
%    exeDir = uigetdir(conHandles.recentFolder, 'XCAT executable directory');
    exeDir = uigetdir(conHandles.recentFolder, 'Select the directory in which the XCAT executable is located');
    if isempty(exeDir) | ~exeDir
        return;
    end
    
%     % Create output folder    
%     outDir = uigetdir(guiDir, 'output directory');
%     if isempty(outDir) | ~outDir
%         return;
%     end

%     outDir = uigetdir(guiDir, 'output directory');
      outDir = uigetdir(guiDir, 'Select the directory in which to create the folder for the output files');
%    if handles.xcat.includeTumor == 0
%        outDir = uigetdir(guiDir, 'Select the directory in which to create the folder for the output phantom files');
%    else
%        outDir = uigetdir(guiDir, 'Select the directory in which to create the folder for the output tumor files');
%    end
    if isempty(outDir) | ~outDir
        return;
    end

%    % Create phantom folder
%    if handles.xcat.includeTumor == 1
%        if ~exist(fullfile(outDir, 'phantom'), 'dir')
%            mkdir(outDir, 'phantom');
%            outDir = fullfile(outDir, 'phantom');
%        else
%            outDir = fullfile(outDir, 'phantom');
%        end 
%    % Create tumor folder      
%    else handles.xcat.includeTumor == 0
%        if ~exist(fullfile(outDir, 'tumor'), 'dir')
%            mkdir(outDir, 'tumor');
%            outDir = fullfile(outDir, 'tumor');
%        else
%            outDir = fullfile(outDir, 'tumor');
%        end         
%    end
                
    
    
    [parFile, parDir] = uigetfile('*.par', 'Parameter File', outDir);
    if isempty(parFile) | ~parFile
        choice = questdlg('Would you like to use the current parameters instead of a parameter file?', 'Parameters', 'Yes', 'No', 'No');
        switch choice
            case 'Yes'
                params = handles.xcat.parameters;
                
                parFile = strcat('phantom.par');
                parDir  = outDir;
                
                % Write phantom parameter file
%                if handles.xcat.includeTumor == 0
                    writeParameterFile(fullfile(parDir, parFile), handles.xcat, -1, -1);
%                end
%                if handles.xcat.includeTumor == 1
%                    [~, f, s] = fileparts(fullfile(parDir, parFile))
%                    f = strcat(f,'_tumor',s)
%%                     writeParameterFile(fullfile(parDir, parFile, '_tumor'), handles.xcat, -1, -1);                    
%                    writeParameterFile(fullfile(parDir, f), handles.xcat, -1, -1);                    
%                else
%                    writeParameterFile(fullfile(parDir, parFile), handles.xcat, -1, -1);
%                end
                
                % Write tumor parameter file
                if handles.xcat.includeTumor == 1
                    [~, f, s] = fileparts(fullfile(parDir, parFile));
                    f = strcat(f,'_tumor',s);
                    writeParameterFile(fullfile(parDir, f), handles.xcat, 2, -1);
                end
                
                % Write plaque
                if handles.xcat.includeHeartPlaque == 1
                    [~, f, s] = fileparts(fullfile(parDir, parFile));
                    f = strcat(f,'_plq',s);
                    writeParameterFile(fullfile(parDir, f), handles.xcat, 3, -1);
                end
                
                % Write Defect
                if handles.xcat.includeHeartDefect == 1
                    [~, f, s] = fileparts(fullfile(parDir, parFile));
                    f = strcat(f,'_dfc',s);
                    writeParameterFile(fullfile(parDir, f), handles.xcat, 1, -1);
                end
            case 'No'
                return;
        end
    end
    
    [~, phantomName] = fileparts(fullfile(parDir, parFile));
    
    if ~exist(fullfile(outDir, phantomName), 'dir')
        mkdir(fullfile(outDir, phantomName));
%         mkdir(fullfile(outDir, 'dcm'));
    end
    cd(exeDir);
    
    exeStr = '';
    if isunix
%%        exeStr = sprintf('wine dxcat2.exe %s %s', fullfile(parDir, parFile), fullfile(outDir, phantomName, phantomName));
        exeStr = sprintf('wine dxcat2.exe "%s" "%s"', fullfile(parDir, parFile), fullfile(outDir, phantomName, phantomName));
%         exeStr = sprintf('wine dxcat2.exe "%s" "%s"', fullfile(parDir, parFile), fullfile(outDir, phantomName));
%                if handles.xcat.includeTumor == 1
%                    [~, f, s] = fileparts(fullfile(outDir, phantomName))
%                    f = strcat(f,'_tumor',s)
%                    exeStr = sprintf('wine dxcat2.exe "%s" "%s"', fullfile(parDir, parFile), fullfile(outDir, f))
%                else
%                    exeStr = sprintf('wine dxcat2.exe "%s" "%s"', fullfile(parDir, parFile), fullfile(outDir, phantomName))
%                end
    elseif ispc
%%        exeStr = sprintf('dxcat2.exe %s %s', fullfile(parDir, parFile), fullfile(outDir, phantomName, phantomName))
        exeStr = sprintf('dxcat2.exe "%s" "%s"', fullfile(parDir, parFile), fullfile(outDir, phantomName, phantomName))
%         exeStr = sprintf('dxcat2.exe "%s" "%s"', fullfile(parDir, parFile), fullfile(outDir, phantomName))
%                if handles.xcat.includeTumor == 1
%                    [~, f, s] = fileparts(fullfile(outDir, phantomName))
%                    f = strcat(f,'_tumor',s)
%                    exeStr = sprintf('dxcat2.exe "%s" "%s"', fullfile(parDir, parFile), fullfile(outDir, f))
%                else
%                    exeStr = sprintf('dxcat2.exe "%s" "%s"', fullfile(parDir, parFile), fullfile(outDir, phantomName))
%                end
    end
    
    % Display this message while loading
    hMsg = msgbox('Please wait. Generating phantom....');

%     [status, cmnds] = system(exeStr, '-echo');
    [status, cmnds] = system([exeStr], '-echo');
    
    delete(hMsg);
    
    if ~status
        str = sprintf('Phantom generation completed succesfully.\nCheck %s folder', fullfile(outDir, phantomName));
        uiwait(msgbox(str, 'Phantom generation'));
    else
        str = sprintf('There was an error during phantom generation.');
        uiwait(errordlg(str, 'Phantom generation'));
    end
    
    cd(guiDir);
    rmpath(guiDir);
    
end

guidata(hObject, conHandles);

function writeParameterFile(filename, xcat, phmode, motion)
xcat.writeParameterFile(filename, phmode, motion);

function val = getParameterValue(parameterName, parameterTable)
indx = cellfun(@(x) strcmpi(x,parameterName), [parameterTable(:,1)]);
indx = find(indx>0);
val = parameterTable{indx,2};

function parameterTable = setParameterValue(parametername, value, parameterTable)
indx = cellfun(@(x) strcmpi(x,parametername), [parameterTable(:,1)]);
indx = find(indx>0);
if strcmpi(parametername, 'bladder_activity')
    indx = indx(2);
end

if ~ischar(value)
    parameterTable{indx,2} = num2str(value);
else
    parameterTable{indx,2} = value;
end



%#==================#
%# Image Callbacks  #
%#==================#
function tImage_Callback(hObject, ~, xcat)

conHandles = guidata(hObject);

if strcmpi(get(conHandles.tConstructWindow,'selectiontype'), 'alt')
    
    % Update function handles with current information
    conHandles = guidata(conHandles.tConstructWindow);
    
    % Get position under mouse in axes data units i.e. pixels
    img = xcat.getImageSlice(xcat.getSliceIndex);
    point = get (conHandles.tImageAxes, 'CurrentPoint');
    if point(1,1) < 0 || point(1,1) > size(img,2) || point(1,2) < 0 || point(1,2) > size(img,1)
        return;
    end
    
    % This is needed to empty the context menu from previous entries.
    % It is populated dynamically
    delete(get(conHandles.tImageContextMenu, 'Children'));
    
    % Find organs under cursor
    organ = [];
    indx  = round(img(ceil(point(1,2)), ceil(point(1,1))));
    fprintf('Organ index = %d\n', indx);
    if indx == 0
        organ{end+1} = 'change phantom array size';
        organ{end+1} = 'change voxel dimensions'  ;
        %return;
    else
        organ{end+1} = 'add tumor here';
        if indx == 7
            organ{end+1} = 'add lung motion';
        end
        if indx == 41
            organ{end+1} = 'add heart defect';
            organ{end+1} = 'add heart plaque';
            organ{end+1} = 'setup heart motion';
        end
    end
    
    % uncomment the following line to add the underlying organ
    % names to the context menu
%     organ = xcat.getOrgan(indx);  
   
    
    set(conHandles.tConstructWindow, 'Units', 'pixels');
    pos  = get (conHandles.tConstructWindow, 'CurrentPoint');
    set(conHandles.tConstructWindow, 'Units', 'normalized');
    
    
    hContextMenu = [];
    for i=1:length(organ)
        hContextMenu(i) = uimenu(conHandles.tImageContextMenu, 'Label', organ{i}, 'Callback',{@tImageContextMenu_Callback, xcat});
    end

    set(conHandles.tImageContextMenu, 'Position', [pos(1) pos(2)]);
    set(conHandles.tImageContextMenu, 'Visible', 'on');
end

guidata(hObject, conHandles);



function tImageSlider_Callback(hObject,  ~, xcat)
conHandles = guidata(hObject);

sliceIndx = floor(get(hObject, 'Value'));

displayImage(conHandles.tImageAxes, xcat, sliceIndx);

guidata(hObject, conHandles);


function tImageContextMenu_Callback(hObject, ~, xcat)

conHandles = guidata(hObject);

% #===========================#
% # Change phantom array size #
% #===========================#
if strcmpi(get(hObject, 'Label'), 'change phantom array size')
    promptStr = 'Select new phantom array';
    strSelect = {};
    i=1;
    for k=128:128:1024
        for j=[200:100:1000 1500 2000]
            strSelect{i} = sprintf('%dx%dx%d', k, k, j); 
            i = i + 1;
        end
    end
    [s,~] = listdlg('PromptString', promptStr, 'SelectionMode', 'single','ListString', strSelect);
    
    if isempty(s)
        return;
    end
    
    newArray = str2num(regexprep(strSelect{s}, 'x', ' '));
    
    imgSize = size(xcat.getImageSlice(xcat.getSliceIndex));
    
    startIndex = conHandles.hl1XData(1);
    endIndex   = conHandles.hl2XData(1);
    
    f_start = startIndex/imgSize(2);
    f_end   = endIndex/imgSize(2);
   
    
    pixel_width = xcat.imgWidth/newArray(1);
    slice_width = xcat.imgLength/newArray(3);
    
    xcat.setParameterValue('array_size' , newArray(1));
    xcat.setParameterValue('pixel_width', pixel_width);
    xcat.setParameterValue('slice_width', slice_width);

    img = xcat.activityImage(newArray);
    xcat.setImage(img);
    
    % Change Slice Selector Line Length and position
    conHandles.hl1XData = [floor(f_start*newArray(3)) floor(f_start*newArray(3))];
    conHandles.hl2XData = [floor(f_end*newArray(3))   floor(f_end*newArray(3))  ];
    
    conHandles.hl1YData = [1 newArray(1)];
    conHandles.hl2YData = [1 newArray(1)];
    
    xcat.setParameterValue('startslice', conHandles.hl1XData(1));
    xcat.setParameterValue('endslice'  , conHandles.hl2XData(1));
    
    guidata(hObject, conHandles);
    
    displayImage(hObject, xcat, floor(0.5*newArray(1)));
end


% #=========================#
% # Change voxel dimensions #
% #=========================#
if strcmpi(get(hObject, 'Label'), 'change voxel dimensions')
end


% #===============#
% #   Add tumor   #
% #===============#
if strcmpi(get(hObject, 'Label'), 'add tumor here')
    set(conHandles.tConstructWindow, 'Units', 'pixels');
    pos    = get (conHandles.tImageAxes, 'CurrentPoint');
    pos    = pos(1,:);
    pos(3) = get(conHandles.tImageSlider, 'Value');
    set(conHandles.tConstructWindow, 'Units', 'normalized');
    
    startSliceIndex = xcat.getParameterValue('startslice');
    
    pos(1) = pos(1) - str2num(startSliceIndex);
    
    answer = [];
    try
        answer = superdlg({'generate tumor parameter file';'x position (LR Direction):'; 'y position (SI Direction):'  ;...
                           'z position (AP Direction):'   ; 'size (mm):'               ; 'tumor motion'; 'Motion file:';}, {...
            'checkbox';...
            'edit'    ;...
            'edit'    ;...
            'edit'    ;...
            'edit'    ;...
            'checkbox';...
            'openfile';...
            },...
            'Tumor Parameters',...
            {...
            '0'            ;...
            num2str(floor(pos(2)));...
            num2str(floor(pos(3)));...
            num2str(floor(pos(1)));...
            '20'           ;...
            '0'            ;...
            ''             ;...
            });
    catch err
        for i=1:length([err.stack.line])
            fprintf('%s : %d\n', err.stack(i).name, err.stack(i).line);
        end
        
        answer = {};
    end
    
    if numel(answer) ~= 0
        if isempty(answer{1})
            return;
        else
            xcat.includeTumor = answer{1};
            xcat.setParameterValue('x_location'           , answer{2});
            xcat.setParameterValue('y_location'           , answer{3});
            xcat.setParameterValue('z_location'           , answer{4});
            xcat.setParameterValue('lesn_diameter'        , answer{5});
            xcat.setParameterValue('tumor_motion_flag'    , answer{6});
            xcat.setParameterValue('tumor_motion_filename', answer{7});
            fprintf('Tumor parameters set');
        end
    else
        return;
    end
    
    
    
    ldiam = 0.1*str2num(answer{5});
    ldiam_pixels(1) = ldiam/str2num(xcat.getParameterValue('pixel_width'));
    ldiam_pixels(2) = ldiam/str2num(xcat.getParameterValue('slice_width'));
    
    recpos = [pos(1)+str2num(startSliceIndex)-0.5*ldiam_pixels(1) pos(2)-0.5*ldiam_pixels(2) ldiam_pixels(1) ldiam_pixels(2)];
    hLesionPos = findobj('Parent', conHandles.tImageAxes, 'Tag', 'tLesionPos');
    if ~isempty(hLesionPos)
        delete(hLesionPos);
    end
    rectangle('Position', recpos, 'Parent', conHandles.tImageAxes, 'Curvature', [1 1], 'EdgeColor', 'm', 'Tag', 'tLesionPos');
    
end


% #=====================#
% #   Add lung motion   #
% #=====================#
if strcmpi(get(hObject, 'Label'), 'add lung motion')
    set(conHandles.tConstructWindow, 'Units', 'pixels');
    pos    = get (conHandles.tConstructWindow, 'CurrentPoint');
    pos(3) = get(conHandles.tImageSlider, 'Value');
    set(conHandles.tConstructWindow, 'Units', 'normalized');
    
    answer = [];
    try
        answer = superdlg({'respiratory cycle (s):';'respiration start phase (0-1):';'maximum diaphragm motion (mm):';'maximum AP expansion:';'diaphragm motion file:';'AP motion file:';'height of right_diaphragm/liver dome:';'height of left diaphragm dome'}, {'edit';'edit';'edit';'edit';'openfile'; 'openfile'; 'edit'; 'edit'}, 'Respiratory Motion Parameters', {...
            xcat.findParameterValue('resp_period'         );...
            xcat.findParameterValue('resp_start_ph_index' );...
            xcat.findParameterValue('max_diaphragm_motion');...
            xcat.findParameterValue('max_AP_exp'          );...
            xcat.findParameterValue('dia_filename'        );...
            xcat.findParameterValue('ap_filename'         );...
            xcat.findParameterValue('rdiaph_liv_scale'    );...
            xcat.findParameterValue('ldiaph_scale'        )});
    catch err
        answer = {};
    end
    
    if numel(answer) ~= 0
        if isempty(answer{1})
            return;
        else
            xcat.setParameterValue('resp_period', answer{1});
            xcat.setParameterValue('resp_start_ph_index', answer{2})
            xcat.setParameterValue('max_diaphragm_motion', answer{3});
            xcat.setParameterValue('max_AP_exp', answer{4});
            xcat.setParameterValue('dia_filename', answer{5});
            xcat.setParameterValue('ap_filename', answer{6});
            xcat.setParameterValue('rdiaph_liv_scale', answer{7});
            xcat.setParameterValue('ldiaph_scale', answer{8});
        end
    else
        return;
    end
    
end


% #=====================#
% #   Add heart defect  #
% #=====================#
if strcmpi(get(hObject, 'Label'), 'add heart defect')
    set(conHandles.tConstructWindow, 'Units', 'pixels');
    pos    = get (conHandles.tConstructWindow, 'CurrentPoint');
    pos(3) = get(conHandles.tImageSlider, 'Value');
    set(conHandles.tConstructWindow, 'Units', 'normalized');
    
    answer = [];
    try
        answer = superdlg({...
            'regional motion abnormality in the LV'                               ;...
            'theta center (deg):'                                                 ;...
            'theta width (deg):'                                                  ;...
            'x center [0-1]:'                                                     ;...
            'x width (mm):'                                                       ;...
            'fraction of the outer wall transgressed by the lesion:'              ;...
            'scales the motion of the defect region [0-1]:'                       ;...
            'longitudinal width of transition between abnormal and normal motion:';...
            'radial width of transition between abnormal and normal motion:'      ;...
            }, ...
            {...
            'checkbox';...
            'edit';...
            'edit';...
            'edit';...
            'edit';...
            'edit';...
            'edit';...
            'edit';...
            'edit';...
            },...
            'Heart Defect Parameters',...
            {...
            xcat.findParameterValue('motion_defect_flag');...
            xcat.findParameterValue('ThetaCenter'       );...
            xcat.findParameterValue('ThetaWidth'        );...
            xcat.findParameterValue('XCenterIndex'      );...
            xcat.findParameterValue('XWidthIndex'       );...
            xcat.findParameterValue('Wall_fract'        );...
            xcat.findParameterValue('motion_scale'      );...
            xcat.findParameterValue('border_zone_long'  );...
            xcat.findParameterValue('border_zone_radial');...
            });
    catch err
        answer = {};
    end
    
    if numel(answer) ~= 0
        if isempty(answer{1})
            return;
        else
            xcat.setParameterValue('motion_defect_flag', answer{1});
            xcat.setParameterValue('ThetaCenter'       , answer{2});
            xcat.setParameterValue('ThetaWidth'        , answer{3});
            xcat.setParameterValue('XCenterIndex'      , answer{4});
            xcat.setParameterValue('XWidthIndex'       , answer{5});
            xcat.setParameterValue('Wall_fract'        , answer{6});
            xcat.setParameterValue('motion_scale'      , answer{7});
            xcat.setParameterValue('border_zone_long'  , answer{8});
            xcat.setParameterValue('border_zone_radial', answer{9});
            
            xcat.includeHeartDefect = answer{1};
        end
    else
        return;
    end
end


% #=====================#
% #   Add heart plaque  #
% #=====================#
if strcmpi(get(hObject, 'Label'), 'add heart plaque')
    set(conHandles.tConstructWindow, 'Units', 'pixels');
    pos    = get (conHandles.tConstructWindow, 'CurrentPoint');
    pos(3) = get(conHandles.tImageSlider, 'Value');
    set(conHandles.tConstructWindow, 'Units', 'normalized');
    
    answer = [];
    try
        answer = superdlg({...
            'generate heart plaque parameter file'                      ;...
            'plaque center along the length of the artery [0-1]:'       ;...
            'plaque center along the circumference of the artery [0-1]:';...
            'plaque thickness (mm):'                                    ;...
            'plaque width (mm):'                                        ;...
            'plaque length (mm):'                                       ;...
            'vessel ID to place the plaque in'                          }, ...
            {...
            'checkbox'
            'edit';...
            'edit';...
            'edit';...
            'edit';...
            'edit';...
            'edit';...
            },...
            'Heart Plaque Parameters', {...
            '0'                                   ;...
            xcat.findParameterValue('p_center_v');...
            xcat.findParameterValue('p_center_u');...
            xcat.findParameterValue('p_height'  );...
            xcat.findParameterValue('p_width'   );...
            xcat.findParameterValue('p_length'  );...
            xcat.findParameterValue('p_id'      )});
    catch err
        answer = {};
    end
    
    if ~isempty(answer)
        xcat.setParameterValue('p_center_v', answer{2});
        xcat.setParameterValue('p_center_u', answer{3});
        xcat.setParameterValue('p_height'  , answer{4});
        xcat.setParameterValue('p_width'   , answer{5});
        xcat.setParameterValue('p_length'  , answer{6});
        xcat.setParameterValue('p_id'      , answer{7});
        
        xcat.includeHeartPlaque = answer{1};
    end
end

% #=================#
% #   heart motion  #
% #=================#
if strcmpi(get(hObject, 'Label'), 'setup heart motion')
    set(conHandles.tConstructWindow, 'Units', 'pixels');
    pos    = get (conHandles.tConstructWindow, 'CurrentPoint');
    pos(3) = get(conHandles.tImageSlider, 'Value');
    set(conHandles.tConstructWindow, 'Units', 'normalized');
    
    answer = [];
    try
        answer = superdlg({...
            'beating heart cycle (s):'        ;...
            'heart beating start phase [0-1]:';...
            'heart base file:'                ;...
            'heart beating curve file:'       }, ...
            {...
            'edit';...
            'edit';...
            'openfile';...
            'openfile';...
            },...
            'Heart Motion Parameters', {...
            xcat.findParameterValue('hrt_period'         );...
            xcat.findParameterValue('hrt_start_ph_index' );...
            xcat.findParameterValue('heart_base'         );...
            xcat.findParameterValue('heart_curve_file'   )});
    catch err
        answer = {};
    end
    
    if ~isempty(answer)
        xcat.setParameterValue('hrt_period'        , answer{1});
        xcat.setParameterValue('hrt_start_ph_index', answer{2});
        xcat.setParameterValue('heart_base'        , answer{3});
        xcat.setParameterValue('heart_curve_file'  , answer{4});
    end
    
    %keyboard
end

guidata(hObject, conHandles);


function displayImage(hObject, xcat, sliceIndx)

conHandles = guidata(hObject);

img  = xcat.getImage();
xcat.setSliceIndex(sliceIndx);


himg = imagesc(img(:,:,sliceIndx), 'Parent', conHandles.tImageAxes, 'Tag', 'tActivityImage');
axis normal; axis tight;
xlim(conHandles.tImageAxes, [1 size(img(:,:,sliceIndx),2)]);
ylim(conHandles.tImageAxes, [1 size(img(:,:,sliceIndx),1)]);

% Set Colormap max and min
set(conHandles.tImageAxes, 'CLim', [0 47]);

setupSlider(hObject, img, sliceIndx);

setupSliceSelector(hObject);

set(himg,'ButtonDownFcn',{@tImage_Callback, xcat});

hText = findobj('Parent', conHandles.tImageAxes, 'Tag', 'tPosText');
if isempty(hText)
    % Crate text for position
    hText = text(10, 10, sprintf('x: %.0f y: %0.f z: %0.f', 1, sliceIndx, 1), 'Parent', conHandles.tImageAxes, 'Color', [1 1 1], 'Tag', 'tPosText');
    % , 'Tag', 'tPosText'
end

guidata(hObject, conHandles);


function setupSlider(hObject, img, sliceIndx)
conHandles = guidata(hObject);

midIndex = floor(0.5*size(img,3));

if sliceIndx == midIndex
    set(conHandles.tImageSlider, 'Max'  , size(img,3));
    set(conHandles.tImageSlider, 'Value', midIndex);
    set(conHandles.tImageSlider, 'Min'  , 1);
    
    set(conHandles.tImageSliceCurrent, 'String', num2str(midIndex));
    set(conHandles.tImageSliceEnd    , 'String', num2str(size(img,3)));
    
    set(conHandles.tImageSlider, 'SliderStep', [1/(size(img,3)-1) 1/(size(img,3)-1)]);
    
else
    set(conHandles.tImageSlider, 'Value', sliceIndx);
    set(conHandles.tImageSliceCurrent, 'String', num2str(sliceIndx));
end

guidata(hObject, conHandles);