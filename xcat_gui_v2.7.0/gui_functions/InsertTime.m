function [newtimedata, newIndx] = InsertTime(indexdata, timedata, dt)

indices = indexdata - 1;
indx    = find(indices>=1);
val = indices(indx);

newIndx = indx;
j = 1;
for k=1:length(val)
    if val(k) ~= 1
        for l=0:val(k)-1
            newIndx = [newIndx(1:sum(val(1:k-1))+l);indx(k) + l; indx(k+1:end)];
        end
    end
end

newIndx(newIndx>numel(timedata)) = [];

newtimedata = timedata(1:newIndx(1));
newtimedata = [newtimedata;newtimedata(end)];
newtimedata(end-1) = newtimedata(end)-dt;

newtimedata = [];
newtimedata = timedata(1:newIndx(1));
for i=2:length(newIndx)
    newtimedata = [newtimedata; timedata(newIndx(i-1):newIndx(i))];
end

d = find(diff(newtimedata)==0);
newtimedata(d) = newtimedata(d+1) - dt;
newtimedata = unique(newtimedata);
df = diff(newtimedata);
i=find(df < (0.5*dt));
newtimedata(i) = [];