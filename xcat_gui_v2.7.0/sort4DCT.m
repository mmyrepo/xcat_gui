[peakInh, peakInhIndx] = findpeaks( ap(:,2), 'MinPeakDistance', floor(4/min(diff(ap(:,1)))), 'MinPeakHeight', 9);
if isempty(peakInhIndx)
    if ap(1,2) >= 9.5
        peakInh     = ap(1,2);
        peakInhIndx = 1;
    end
end

ap_(:,1) = ap(:,1) - ap(peakInhIndx,1);
t_n = find(ap_<0);
%ap_((end+1):(end+numel(t_n)), 1) = ap_(end,1) - ap_(t_n,1);
%ap_(t_n) = [];

% Image sampling times
cineInterval = 0.5;
t(:,1) = ap_(1,1):cineInterval:ap_(end,1);
a(:,1) = interp1(ap_(:,1), ap(:,2), t);


%bins = cineInterval/;
cinePeriod = 5;
t_b(:,1) = linspace(0, cinePeriod, bins);

figure;plot(ap_(:,1), ap(:,2), 'k', 'LineWidth', 1.5); 
hold on;
for k=1:numel(t_b)  
    plot([t_b(k) t_b(k)], ylim, 'LineStyle', '--', 'Color', [0.5 0.5 0.5]);
end
plot(t,a, 'or');
